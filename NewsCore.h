//
//  NewsCore.h
//  NewsCore
//
//  Created by Shashank Patel on 16/03/19.
//  Copyright © 2019 Shashank Patel. All rights reserved.
//


@import NSDate_TimeAgo;
#import <UIColor_Hex/UIColor+Hex.h>
#import <TagListView_ObjC/TagListView.h>
#import <TagListView_ObjC/TagView.h>
#import <SDWebImage/SDWebImage-umbrella.h>
#import <AMScrollingNavbar/AMScrollingNavbar-umbrella.h>
#import <youtube_ios_player_helper/YTPlayerView.h>
#import <TMCache/TMCache.h>
#import <AFNetworking/AFNetworking.h>
#import <DTCoreText/DTCoreText.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import <ImageSlideshow/ImageSlideshow-umbrella.h>
#import <UIAlertController_Blocks/UIAlertController+Blocks.h>
#import <UINavigationController_SLTransitioning/UINavigationController+SLTransitioning.h>
#import <ChameleonFramework/Chameleon.h>
#import <SafariServices/SafariServices.h>

#import "MacroDefine.h"
#import "CJAMacros.h"
#import "NSUserDefaults-macros.h"
#import "Macros.h"

#import "LanguageManager.h"
#import "Author.h"
#import "Tag.h"
#import "Article.h"
#import "NewsConfig.h"
#import "NetworkManager.h"

#import "NSArray+JSON.h"
#import "NSDictionary+JSON.h"
#import "NSString+JSON.h"

#import "UIView+Theme.h"
#import "ViewController.h"
#import "LoadingView.h"
#import "IframeView.h"
#import "IFramelyView.h"
#import "FigCaptionView.h"
#import "BlockQuoteView.h"
#import "FigCaptionAttachment.h"
#import "BlockQuoteAttachment.h"
#import "ArticleDetailsController.h"
#import "ArticlesController.h"
#import "ArticleDetailsCell.h"
#import "CollectionView.h"
#import "ArticleCell.h"
#import "SpecialCoverageCell.h"
#import "NCImageSource.h"
#import "FeaturedArticleCell.h"
#import "NewsListController.h"
#import "Label.h"
#import "Button.h"
#import "UIView+FixLayout.h"
#import "NavigationTitleView.h"
#import "NSLocale+TTTOverrideLocale.h"
#import "NSBundle+TTTOverrideLanguage.h"
#import "UIImage+Scale.h"
#import "AuthorController.h"
#import "LaunchController.h"

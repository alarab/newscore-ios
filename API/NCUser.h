//
//  NCUser.h
//  NewsCore
//
//  Created by Shashank Patel on 24/02/18.
//

#import <Foundation/Foundation.h>
#import "NCObject.h"

@interface NCUser : NCObject

@property (nonatomic, strong, nonnull)  NSString    *token;
@property (nonatomic, strong, nonnull)  UIImage     *image;


typedef void (^NCUserResultBlock)(NCUser *_Nullable user, NSError *_Nullable error);

+ (nullable NCUser*)currentUser;
+ (void)logOut;
+ (void)logInWithEmail:(NSString*_Nonnull)email
              password:(NSString*_Nonnull)password
  inBackgrounWithBlock:(nullable NCUserResultBlock)block;

- (void)signUpInBackgroundWithBlock:(nullable CMBooleanResultBlock)block;
- (void)setCurrent;

@end

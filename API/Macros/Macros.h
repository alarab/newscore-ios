//
//  Macros.h
//  pro
//
//  Created by Shashank Patel on 16/09/16.
//  Copyright © 2018 iOS. All rights reserved.
//

#ifndef Macros_h
#define Macros_h

#ifndef DEBUG
#define DEBUG 0
#endif

#define TemplateImage(x) [x imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]

#define kSharedNewsConfig [NewsConfig sharedNewsConfig]
#define kSharedLanguageManager [LanguageManager sharedLanguageManager]

#define VIEW_HEIGHT self.view.frame.size.height
#define VIEW_WIDTH self.view.frame.size.width

#define ControllerFromStoryBoard(storyboard, identifier) [[UIStoryboard storyboardWithName:storyboard bundle:nil] instantiateViewControllerWithIdentifier:identifier]
#define ControllerFromMainStoryBoard(identifier) ControllerFromStoryBoard(@"Main", identifier)
#define NavigationControllerWithController(controller) [[UINavigationController alloc] initWithRootViewController:controller]

#define SERVER_DATE_FORMAT @"yyyy-MM-dd'T'HH:mm:ss"

#endif /* Macros_h */

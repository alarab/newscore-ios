//
//  Article.h
//  NewsCore
//
//  Created by Shashank Patel on 16/03/19.
//  Copyright © 2019 Shashank Patel. All rights reserved.
//

#import "NCObject.h"

extern NSString *kArticleTitleKey;
extern NSString *kArticleSubTitleKey;
extern NSString *kArticleSummaryKey;
extern NSString *kArticleFeaturedImageCaption;
extern NSString *kArticleTypeKey;
extern NSString *kArticleTypeLabelKey;
extern NSString *kArticleParagraphKey;
extern NSString *kArticleBGColorKey;
extern NSString *kArticleCardColorKey;

extern NSString *kArticleURLKey;
extern NSString *kArticleFeaturedImageKey;
extern NSString *kArticleFocusedImageKey;
extern NSString *kArticleBookImageKey;

extern NSString *kArticleTagsKey;
extern NSString *kArticleTagsIDsKey;

extern NSString *kArticleAuthorKey;
extern NSString *kArticleAuthorIDKey;
extern NSString *kArticleAuthorWebKey;
extern NSString *kArticleAuthorFacebookKey;
extern NSString *kArticleAuthorTwitterKey;
extern NSString *kArticleAuthorNameKey;
extern NSString *kArticleAuthorImageKey;

extern NSString *kArticleCreatedKey;
extern NSString *kArticleUpdatedKey;

extern NSString *kDateFormat;

@interface DummyClass: NSObject
@end


@interface Article : NCObject

@property (nonatomic, strong)   NSString            *title, *subTitle, *summary, *dateText, *compoundText;
@property (nonatomic, strong)   NSString            *featuredImageCaption;
@property (nonatomic, strong)   NSString            *type, *typeLabel;
@property (nonatomic, strong)   NSDictionary        *titleAttributes, *tagAttributes;
@property (nonatomic, strong)   NSDictionary        *bodyAttributes, *dateAttributes;
@property (nonatomic, strong)   NSURL               *URL, *featuredImageURL, *focusedImageURL, *bookImageURL;
@property (nonatomic, strong)   NSArray<Tag*>       *tags;
@property (nonatomic, strong)   NSArray             *paragraphs, *relatedArticles;
@property (nonatomic, strong)   NSArray<Article*>   *articles;
@property (nonatomic, strong)   NSDate              *createdDate, *updatedDate;
@property (nonatomic, readonly) BOOL                isBookmarked;
@property (nonatomic)           BOOL                customSearched;

@property (nonatomic, strong)   Author              *author;
@property (nonatomic, strong)   NSArray<Author*>    *authors;

@property (nonatomic, strong)   NSAttributedString  *attributedTitle, *attributedSubTitle, *attributedSummary;
@property (nonatomic, strong)   NSAttributedString  *attributedTitleForDetail, *attributedSubTitleForDetail;
@property (nonatomic, strong)   NSAttributedString  *attributedFeaturedImageCaption;
@property (nonatomic, strong)   NSAttributedString  *attributedDateText;
@property (nonatomic, strong)   NSAttributedString  *attributedTag;
@property (nonatomic, strong)   NSAttributedString  *attributedCompoundText;
@property (nonatomic, strong)   NSAttributedString  *attributedAuthorNamesForDetail, *attributedDateTextForDetails;
@property (nonatomic, strong)   NSAttributedString  *textOnlyTitle, *tagTitleWriter;
@property (nonatomic, strong)   NSAttributedString  *attributedIssueTitle;

+ (NSArray<Article*>*)bookmarkedArticles;
+ (void)loadSlug:(NSString*)slug completionHandler:(CMObjectResultBlock)block;
+ (void)fetchForSlug:(NSString*)slug completionHandler:(CMObjectResultBlock)block;
+ (void)fetchForArticleID:(NSString*)articleID completionHandler:(CMObjectResultBlock)block;

- (CGFloat)widthOfAttributedTagWithHeight:(CGFloat)height;
- (CGFloat)heightOfAttributedTitleWithWidth:(CGFloat)width;
- (CGFloat)heightOfAttributedTitleWithWidth:(CGFloat)width ignoreRect:(CGRect)ignoreRect;
- (CGFloat)heightOfTextOnlyTitleWithWidth:(CGFloat)width;
- (CGFloat)heightOfTagTitleWriterWithWidth:(CGFloat)width;

- (void)requestArticleDetailsForPrefetching;
- (void)requestArticleDetailsWithBlock:(CMBooleanResultBlock)block;
- (NSString*)htmlString;
- (void)bookmark;
- (NSURL*)shareURL;

- (NSAttributedString*)attributedImageCaptionForText:(NSString*)captionText;

- (UIColor*)backgroundColor;
- (UIColor*)cardColor;

@end

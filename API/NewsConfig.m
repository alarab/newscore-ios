//
//  NewsConfig.m
//  NewsCore
//
//  Created by Shashank Patel on 18/03/19.
//  Copyright © 2019 Shashank Patel. All rights reserved.
//

#import "NewsConfig.h"

@interface NewsConfig ()

@property (nonatomic, strong)   NSMutableArray<NewsConfigDelegate>     *delegates;

@end

@implementation NewsConfig

+ (NewsConfig*)sharedNewsConfig{
    static dispatch_once_t onceToken;
    static NewsConfig *sharedNewsConfig;
    dispatch_once(&onceToken, ^{
        sharedNewsConfig = [NewsConfig new];
    });
    return sharedNewsConfig;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(applicationActivated) name:UIApplicationDidBecomeActiveNotification
                                                   object:nil];
    }
    return self;
}

- (void)addDelegate:(id<NewsConfigDelegate>)aDelegate{
    if (!self.delegates) self.delegates = [NSMutableArray<NewsConfigDelegate> array];
    [self.delegates addObject:aDelegate];
}

- (void)setDataSource:(NSObject<NewsConfigDataSource>*)ds{
    _dataSource = ds;
    [self performSelectorInBackground:@selector(loadHomeAndMenu) withObject:nil];
//    [self loadHomeAndMenu];
}

- (void)applicationActivated{
    static BOOL ignoredOnce = NO;
    if (!ignoredOnce) {
        ignoredOnce = YES;
        return;
    }
//    [self loadHomeAndMenu];
}

- (void)loadHomeAndMenu{
    [self loadHomeAndMenu:YES];
}

- (void)loadHomeAndMenu:(BOOL)shouldUseCache{
    [NetworkManager cancelAllRequests];
    self.articles = @[];
    [NetworkManager getEndPoint:[self.dataSource homeURL]
                          cache:shouldUseCache
                     parameters:nil
                        success:^(id responseObject) {
                            //                            NSLog(@"Got Home: %@", [responseObject description]);
                            self.articles = [self.dataSource articlesFromResponse:responseObject];
                            dispatch_async(dispatch_get_main_queue(), ^{
                                for (id<NewsConfigDelegate>aDelegate in self.delegates) {
                                    if ([aDelegate respondsToSelector:@selector(newsDataLoadCompletedSuccessfully)]) {
                                        [aDelegate newsDataLoadCompletedSuccessfully];
                                    }
                                }
                            });
                        } failure:^(id responseObject, NSError * _Nonnull error) {
                            NSLog(@"Failed Home: %@", [error description]);
                            dispatch_async(dispatch_get_main_queue(), ^{
                                for (id<NewsConfigDelegate>aDelegate in self.delegates) {
                                    if ([aDelegate respondsToSelector:@selector(newsDataLoadComnpletedWithError:)]) {
                                        [aDelegate newsDataLoadComnpletedWithError:error];
                                    }
                                }
                            });
                        }];
    
    
    [NetworkManager callURL:[self.dataSource menuURL]
                      cache:YES
                   withDict:nil
                     method:@"GET"
                       JSON:YES
                    success:^(id  _Nonnull responseObject) {
                        self.menu = [kSharedNewsConfig.dataSource menuFromResponse:responseObject];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            for (id<NewsConfigDelegate>aDelegate in self.delegates) {
                                if ([aDelegate respondsToSelector:@selector(menuLoadComnpletedSuccessfully)]) {
                                    [aDelegate menuLoadComnpletedSuccessfully];
                                }
                            }
                        });
                    } failure:^(id  _Nonnull responseObject, NSError * _Nonnull error) {
                        NSLog(@"Failed Menu: %@", [error description]);
                    }];
}

- (NSString*)baseURL{
    return [self.dataSource baseURL];
}

- (UIColor*)navigationBarColor{
    return [self.dataSource navigationBarColor];
}

- (UIColor*)borderColor{
    return [self.dataSource borderColor];
}

- (UIFont*)fontForLanguage:(LanguageName)languageName{
    return [self.dataSource fontForLanguage:languageName];
}

- (UIFont *)articleBodyFont{
    if (kSharedLanguageManager.language == LanguageNameArabic) {
        return [UIFont fontWithName:@"HelveticaNeueW23forSKY-Reg" size:[self articleBodyFontSize]];
    }else{
        return [UIFont fontWithName:@"ZillaSlab-Light" size:[self articleBodyFontSize]];
    }
    
}

- (UIFont *)articleTitleFont{
    if (kSharedLanguageManager.language == LanguageNameArabic) {
        return [UIFont fontWithName:@"HelveticaNeueW23forSKY-Bd" size:[self.dataSource articleTitleFontSize]];
    }else{
        return [UIFont fontWithName:@"ZillaSlab-Bold" size:[self.dataSource articleTitleFontSize]];
    }
}

- (UIFont *)articleSubTitleFont{
    if (kSharedLanguageManager.language == LanguageNameArabic) {
        return [UIFont fontWithName:@"HelveticaNeueW23forSKY-Reg" size:18];
    }else{
        return [UIFont fontWithName:@"ZillaSlab-Light" size:18];
        return [UIFont systemFontOfSize:14];
    }
}

- (UIFont *)articleImageCaptionFont{
    if (kSharedLanguageManager.language == LanguageNameArabic) {
        return [UIFont fontWithName:@"HelveticaNeueW23forSKY-Reg" size:14];
    }else{
        return [UIFont fontWithName:@"ZillaSlab-Light" size:14];
    }
}

- (double)articleBodyFontSize{
    double fontSize = [defaults_object(@"user_pref_font_size") doubleValue];
    if (fontSize == 0) {
        defaults_set_object(@"user_pref_font_size", @20);
        fontSize = [defaults_object(@"user_pref_font_size") doubleValue];
    }
    return fontSize;
}

- (void)setArticleBodyFontSize:(double)articleBodyFontSize{
    defaults_set_object(@"user_pref_font_size", @(articleBodyFontSize));
}

@end

//
//  LanguageManager.h
//  NewsCore
//
//  Created by Shashank Patel on 16/03/19.
//  Copyright © 2019 Shashank Patel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


typedef enum : NSUInteger {
    LanguageNameUnknown,
    LanguageNameEnglish,
    LanguageNameArabic,
    LanguageNameTurkish,
} LanguageName;

@protocol LanguageManagerDelegate <NSObject>

- (void)languageWillChangeTo:(LanguageName)languageName isRTL:(BOOL)isRTL;
- (void)languageDidChangedTo:(LanguageName)languageName isRTL:(BOOL)isRTL;

@end


@interface LanguageManager : NSObject

@property (nonatomic)               LanguageName                language;
@property (nonatomic, readonly)     BOOL                        isRTL;
@property (nonatomic)               NSTextAlignment             defaultTextAlignment, reversedTextAlignment;
@property (nonatomic)               UISemanticContentAttribute  semanticAttribute, reversedSemanticAttribute;

@property (nonatomic, strong)   NSString    *languageSuffix;

+ (LanguageManager*)sharedLanguageManager;

- (void)addDelegate:(id<LanguageManagerDelegate>)aDelegate;
- (void)showLanguageChangeUI;
- (NSString*)languageChangeTitleShort;
- (UIImage*)logoImage;
- (NSString*)relatedArticlesText;

@end

NS_ASSUME_NONNULL_END

//
//  Author.m
//  NewsCore
//
//  Created by Shashank Patel on 16/03/19.
//  Copyright © 2019 Shashank Patel. All rights reserved.
//

#import "Author.h"

@implementation Author

- (NSString *)objectId{
    return self[kArticleAuthorIDKey];
}

- (NSString *)debugDescription{
    return [NSString stringWithFormat:@"<%@: %p> %@", [self class], self, [internalObject description]];
}

- (NSURL*)imageURL{
    NSString *imageURLString = self[kArticleAuthorImageKey];
    if ([imageURLString isKindOfClass:[NSString class]]) {
        return [NSURL URLWithString:imageURLString];
    }
    return nil;
}


- (NSString *)name{
    return self[kArticleAuthorNameKey]?:@"";
}

- (NSAttributedString *)attributedName{
    return [[NSAttributedString alloc] initWithString:self.name
                                           attributes:self.nameAttributes];
}

- (NSAttributedString *)attributedNameForDetails{
    NSMutableDictionary *reversed = [self.nameAttributesForDetails mutableCopy];
    return [[NSAttributedString alloc] initWithString:self.name
                                           attributes:reversed];
}

- (NSDictionary*)nameAttributesForDetails{
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = kSharedLanguageManager.defaultTextAlignment;
    
    UIFont *font = [[kSharedNewsConfig.dataSource boldFontForLanguage:kSharedLanguageManager.language] fontWithSize:14];
    return @{NSFontAttributeName : font,
             NSForegroundColorAttributeName: UIColor.blackColor,
             NSParagraphStyleAttributeName : paragraphStyle
             };
    
}


- (CGFloat)heightOfattributedNameWithWidth:(CGFloat)width{
    return [self heightOfattributedNameWithWidth:width
                                      ignoreRect:CGRectZero];
}

- (CGFloat)heightOfattributedNameWithWidth:(CGFloat)width ignoreRect:(CGRect)ignoreRect{
    CGRect textRect = [self.attributedName boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX)
                                                        options:NSStringDrawingUsesLineFragmentOrigin
                                                        context:nil];
    return ceil(textRect.size.height);
}

@end

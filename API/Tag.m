//
//  Tag.m
//  MEO
//
//  Created by Shashank Patel on 29/03/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import "Tag.h"

@implementation Tag

- (NSString *)title{
    return self[kArticleTagNameKey];
}

@end

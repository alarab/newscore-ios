//
//  NetworkManager.m
//  NewsCore
//
//  Created by Shashank Patel on 21/12/17.
//

#import "NetworkManager.h"
#import "NCUser.h"
#import "NSString+JSON.h"
#import "AppDelegate.h"

@implementation NetworkManager

+ (AFURLSessionManager*)requestManager{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    static dispatch_once_t onceToken;
    static AFURLSessionManager *manager;
    dispatch_once(&onceToken, ^{
        manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
    });
    
    return manager;
}

+ (void)cancelAllRequests{
    [[self requestManager].session getTasksWithCompletionHandler:^(NSArray<NSURLSessionDataTask *> * _Nonnull dataTasks, NSArray<NSURLSessionUploadTask *> * _Nonnull uploadTasks, NSArray<NSURLSessionDownloadTask *> * _Nonnull downloadTasks) {
        for (NSURLSessionDataTask *aDataTask in dataTasks) {
            [aDataTask cancel];
        }
    }];
}

+ (AFHTTPRequestSerializer*)httpSerializer{
    AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
    if ([NCUser currentUser]) {
        [serializer setValue:[NCUser currentUser].token forHTTPHeaderField:@"Authorization"];
    }
    return serializer;
}

+ (AFJSONRequestSerializer*)jsonSerializer{
    AFJSONRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
    if ([NCUser currentUser]) {
        [serializer setValue:[NCUser currentUser].token forHTTPHeaderField:@"Authorization"];
    }
    return serializer;
}

+ (void)downloadFileFromURLString:(NSString*)URLString
                           toPath:(NSString*)localPath
                         progress:(void (^)(NSProgress *downloadProgress)) downloadProgressBlock
                          success:(void (^)(id  responseObject))success
                          failure:(void (^)(id  responseObject, NSError * error))failure{
    if ([[NSFileManager defaultManager] fileExistsAtPath:localPath]) {
        success(nil);
        return;
    }
    AFURLSessionManager *manager = [self requestManager];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:URLString]];
    NSURLSessionDownloadTask *dataTask = [manager downloadTaskWithRequest:request
                                                                 progress:^(NSProgress * _Nonnull downloadProgress) {
                                                                     NSLog(@"Downloaded: %f %%", downloadProgress.fractionCompleted * 100);
                                                                     if(downloadProgressBlock)downloadProgressBlock(downloadProgress);
                                                                 }
                                                              destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
                                                                  targetPath = [NSURL fileURLWithPath:localPath];
                                                                  return targetPath;
                                                              } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
                                                                  if (error)failure(nil, error);
                                                                  else success(nil);
                                                              }];
    [dataTask resume];
}


+ (void)uploadToEndPoint:(NSString*)endPoint
              parameters:(NSDictionary*)parameters
                    data:(NSData*)fileData
                fileName:(NSString*)fileName
                  isJSON:(BOOL)isJSON
             uploadBlock:(nullable void (^)(NSProgress *uploadProgress)) uploadProgressBlock
           downloadBlock:(nullable void (^)(NSProgress *downloadProgress)) downloadProgressBlock
                 success:(void (^)(id  responseObject))success
                 failure:(void (^)(id  responseObject, NSError * error))failure{
    NSString *URLString = [API_BASE_URL stringByAppendingPathComponent:endPoint];
    NSError *error;
    AFURLSessionManager *manager = [self requestManager];
    AFHTTPRequestSerializer *serializer = [self httpSerializer];
    if ([NCUser currentUser]) {
        [serializer setValue:[NCUser currentUser].token forHTTPHeaderField:@"Authorization"];
    }
    
    NSMutableURLRequest *request = [serializer multipartFormRequestWithMethod:@"POST"
                                                                    URLString:URLString
                                                                   parameters:parameters
                                                    constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
                                                        [formData appendPartWithFormData:fileData name:fileName];
                                                    } error:&error];
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request
                                                   uploadProgress:uploadProgressBlock
                                                 downloadProgress:downloadProgressBlock
                                                completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                                                    if (error) {
                                                        failure(responseObject, error);
                                                        NSLog(@"Error: %@ : %@", URLString, error.description);
                                                    }else{
                                                        if ([responseObject isKindOfClass:[NSData class]]) {
                                                            responseObject = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                                                            id JSONObject = [responseObject JSONObject];
                                                            if (JSONObject) {
                                                                responseObject = JSONObject;
                                                            }
                                                        }
                                                        success(responseObject);
                                                    }
                                                }];
    [dataTask resume];
}

+ (void)uploadToEndPoint:(NSString*)endPoint
              parameters:(NSDictionary*)parameters
                 fileURL:(NSURL*)fileURL
                fileName:(NSString*)fileName
                  isJSON:(BOOL)isJSON
             uploadBlock:(nullable void (^)(NSProgress *uploadProgress)) uploadProgressBlock
           downloadBlock:(nullable void (^)(NSProgress *downloadProgress)) downloadProgressBlock
                 success:(void (^)(id  responseObject))success
                 failure:(void (^)(id  responseObject, NSError * error))failure{
    NSString *URLString = [API_BASE_URL stringByAppendingPathComponent:endPoint];
    AFURLSessionManager *manager = [self requestManager];
    AFHTTPRequestSerializer *serializer = [self httpSerializer];
    
    NSError *error;
    NSMutableURLRequest *request = [serializer multipartFormRequestWithMethod:@"POST"
                                                                    URLString:URLString
                                                                   parameters:parameters
                                                    constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
                                                        [formData appendPartWithFileURL:fileURL name:fileName error:nil];
                                                    } error:&error];
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request
                                                   uploadProgress:uploadProgressBlock
                                                 downloadProgress:downloadProgressBlock
                                                completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                                                    if (error) {
                                                        failure(responseObject, error);
                                                        NSLog(@"Error: %@ : %@", URLString, error.description);
                                                    }else{
                                                        if ([responseObject isKindOfClass:[NSData class]]) {
                                                            responseObject = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                                                            id JSONObject = [responseObject JSONObject];
                                                            if (JSONObject) {
                                                                responseObject = JSONObject;
                                                            }
                                                        }
                                                        success(responseObject);
                                                    }
                                                }];
    [dataTask resume];
}

+ (void)callURL:(NSString*)URLString
          cache:(BOOL)cache
   loadOnCached:(BOOL)loadOnCached
       priority:(float)priority
       withDict:(NSDictionary*)dict
         method:(NSString*)method
           JSON:(BOOL)isJSON
        success:(void (^)(id  _Nonnull responseObject))success
        failure:(void (^)(id  _Nonnull responseObject, NSError * _Nonnull error))failure{
    if (cache && [method isEqualToString:@"GET"]) {
        id responseObject = [[TMCache sharedCache] objectForKey:URLString];
        if(responseObject){
            if(success)success(responseObject);
            if (!loadOnCached) {
                return;
            }
        }
    }
    
    AFURLSessionManager *manager = [self requestManager];
    AFHTTPRequestSerializer *serializer = isJSON ? [self jsonSerializer] : [self httpSerializer];
    NSURL *URL = [NSURL URLWithString:URLString];
    if (!URL) {
        URLString = [URLString stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet];
    }
    NSMutableURLRequest *request = [serializer requestWithMethod:method
                                                       URLString:URLString
                                                      parameters:dict
                                                           error:nil];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request
                                                   uploadProgress:nil
                                                 downloadProgress:nil
                                                completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                                                    if (error) {
                                                        if(failure)failure(responseObject, error);
                                                        NSLog(@"Error: %@ : %@", URLString, error.description);
                                                    }else{
                                                        if ([responseObject isKindOfClass:[NSData class]]) {
                                                            responseObject = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                                                            id JSONObject = [responseObject JSONObject];
                                                            if (JSONObject) {
                                                                responseObject = JSONObject;
                                                            }
                                                        }
                                                        if([method isEqualToString:@"GET"]){
                                                            [[TMCache sharedCache] setObject:responseObject forKey:URLString];
                                                        }
                                                        if(success)success(responseObject);
                                                    }
                                                }];
    if(priority != NSNotFound) dataTask.priority = priority;
    [dataTask resume];
}

+ (void)callURL:(NSString*)URLString
          cache:(BOOL)cache
       withDict:(NSDictionary*)dict
         method:(NSString*)method
           JSON:(BOOL)isJSON
        success:(void (^)(id  _Nonnull responseObject))success
        failure:(void (^)(id  _Nonnull responseObject, NSError * _Nonnull error))failure{
    [self callURL:URLString
            cache:cache
     loadOnCached:YES
         priority:NSNotFound
         withDict:dict
           method:method
             JSON:isJSON
          success:success
          failure:failure];
}

+ (void)callEndPoint:(NSString*)endPoint
               cache:(BOOL)cache
            withDict:(NSDictionary*)dict
              method:(NSString*)method
                JSON:(BOOL)isJSON
             success:(void (^)(id  _Nonnull responseObject))success
             failure:(void (^)(id  _Nonnull responseObject, NSError * _Nonnull error))failure{
    NSString *URLString = [API_BASE_URL stringByAppendingPathComponent:endPoint];
    [self callURL:URLString
            cache:cache
         withDict:dict
           method:method
             JSON:isJSON
          success:success
          failure:failure];
}

+ (void)getEndPoint:(NSString*)endPoint
            success:(void (^)(id  responseObject))success
            failure:(void (^)(id  responseObject, NSError * _Nonnull error))failure{
    [self getEndPoint:endPoint
                cache:YES
           parameters:nil
              success:success
              failure:failure];
}

+ (void)getEndPoint:(NSString*)endPoint
              cache:(BOOL)cache
         parameters:(NSDictionary*)parameters
            success:(void (^)(id  responseObject))success
            failure:(void (^)(id  responseObject, NSError * _Nonnull error))failure{
    [self callEndPoint:endPoint
                 cache:cache
              withDict:parameters
                method:@"GET"
                  JSON:YES
               success:success
               failure:failure];
}

+ (void)postEndPoint:(NSString*)endPoint
          parameters:(NSDictionary*)parameters
             success:(void (^)(id  responseObject))success
             failure:(void (^)(id  responseObject, NSError * _Nonnull error))failure{
    [self callEndPoint:endPoint
                 cache:NO
              withDict:parameters
                method:@"POST"
                  JSON:YES
               success:success
               failure:failure];
}

+ (void)patchEndPoint:(NSString*)endPoint
           parameters:(NSDictionary*)parameters
              success:(void (^)(id  responseObject))success
              failure:(void (^)(id  responseObject, NSError * _Nonnull error))failure{
    [self callEndPoint:endPoint
                 cache:NO
              withDict:parameters
                method:@"PATCH"
                  JSON:YES
               success:success
               failure:failure];
}

+ (void)deleteEndPoint:(NSString*)endPoint
            parameters:(NSDictionary*)parameters
               success:(void (^)(id  responseObject))success
               failure:(void (^)(id  responseObject, NSError * _Nonnull error))failure{
    [self callEndPoint:endPoint
                 cache:NO
              withDict:parameters
                method:@"DELETE"
                  JSON:YES
               success:success
               failure:failure];
}

@end


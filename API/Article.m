//
//  Article.m
//  NewsCore
//
//  Created by Shashank Patel on 16/03/19.
//  Copyright © 2019 Shashank Patel. All rights reserved.
//

#import "Article.h"

@implementation Article

+ (void)fetchForSlug:(NSString*)slug completionHandler:(CMObjectResultBlock)block{
    [self loadSlug:slug completionHandler:^(id responseObject, NSError * _Nullable error) {
        if (!error) {
            if ([responseObject[@"code"] integerValue] == 1) {
                NSDictionary *articleDict = responseObject[@"data"][0];
                Article *article = [[Article alloc] initWithDictionary:articleDict];
                block(article, nil);
            }else{
                block(nil, [NSError errorWithDomain:NSURLErrorDomain
                                               code:[responseObject[@"code"] integerValue]
                                           userInfo:responseObject]);
            }
        }else{
            block(nil, error);
        }
    }];
}

+ (void)loadSlug:(NSString*)slug completionHandler:(CMObjectResultBlock)block{
    NSString *endPoint = [kSharedNewsConfig.dataSource articleDetailsURLForSlug:slug];
    NSString *URLString = [API_BASE_URL stringByAppendingPathComponent:endPoint];
    URLString = [URLString stringByReplacingOccurrencesOfString:@"v2" withString:@"v1"];
    
    [NetworkManager callURL:URLString
                      cache:YES
               loadOnCached:NO
                   priority:0
                   withDict:nil
                     method:@"GET"
                       JSON:YES
                    success:^(id responseObject) {
                        block(responseObject, nil);
                    }
                    failure:^(id responseObject, NSError * _Nonnull error) {
                        block(nil, error);
                    }];
}


+ (void)fetchForArticleID:(NSString*)articleID completionHandler:(CMObjectResultBlock)block{
    NSString *endPoint = [kSharedNewsConfig.dataSource articleDetailsURLForID:articleID];
    NSString *URLString = [API_BASE_URL stringByAppendingPathComponent:endPoint];
    [NetworkManager callURL:URLString
                      cache:YES
               loadOnCached:NO
                   priority:0
                   withDict:nil
                     method:@"GET"
                       JSON:YES
                    success:^(id responseObject) {
                        if ([responseObject[@"code"] integerValue] == 1) {
                            NSDictionary *articleDict = responseObject[@"data"];
                            Article *article = [[Article alloc] initWithDictionary:articleDict];
                            block(article, nil);
                        }else{
                            block(nil, [NSError errorWithDomain:NSURLErrorDomain
                                                           code:[responseObject[@"code"] integerValue]
                                                       userInfo:responseObject]);
                        }
                    }
                    failure:^(id responseObject, NSError * _Nonnull error) {
                        block(nil, error);
                    }];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict{
    if (self = [super initWithDictionary:dict]) {
        [self performSelectorInBackground:@selector(requestArticleDetailsForPrefetching) withObject:nil];
    }
    return self;
}

- (NSString *)typeLabel{
    NSString *label = self[kArticleTypeLabelKey];
    if ([label isEqualToString:@"Coverage Info"]) {
        label = nil;
    }
    return label?: @"";
}

- (NSString*)title{
    NSString *title = self[kArticleTitleKey]?:@"";
    return [title stringByTrimmingCharactersInSet:
            NSCharacterSet.whitespaceAndNewlineCharacterSet];
}

- (NSString*)subTitle{
    NSString *subtitle = self[kArticleSubTitleKey]?:@"";
    if (![subtitle isKindOfClass:[NSString class]]) {
        subtitle = @"";
    }
    return [subtitle stringByTrimmingCharactersInSet:
            NSCharacterSet.whitespaceAndNewlineCharacterSet];
}

- (NSString*)summary{
    if([self[kArticleSummaryKey] isKindOfClass:[NSNull class]]){
        return @"";
    }
    return self[kArticleSummaryKey]?:@"";
}

- (NSString *)featuredImageCaption{
    return self[kArticleFeaturedImageCaption];
}

- (NSString*)dateText{
    NSString *agoTxt = [[self createdDate] timeAgo];
    agoTxt = agoTxt == nil ? @"" : agoTxt;
    
    NSDateFormatter *df = [NSDateFormatter new];
    if(kSharedLanguageManager.language == LanguageNameArabic) {
        df.locale = [NSLocale localeWithLocaleIdentifier:@"ar_DZ"];
    }else if(kSharedLanguageManager.language == LanguageNameTurkish) {
        df.locale = [NSLocale localeWithLocaleIdentifier:@"tr_TR"];
    }

    [df setDateFormat:@"d/M/y"];
    NSString *dateText = [df stringFromDate:[self createdDate]]?:@"";
    return [NSString stringWithFormat:@"%@-%@", agoTxt, dateText];
    return [NSString stringWithFormat:@" %@ ", dateText];
}

- (NSString*)type{
    return self[kArticleTypeKey]?: @"";
}

- (NSURL*)URL{
    return [NSURL URLWithString:self[kArticleURLKey]];
}

- (NSURL*)featuredImageURL{
    if ([self.type isEqualToString:@"book_cover"]) {
        return self.bookImageURL;
    }
    NSString *imageURLString = self[kArticleFeaturedImageKey]?:self[@"image"];
    return [NSURL URLWithString:imageURLString];
}

- (NSURL*)focusedImageURL{
    return [NSURL URLWithString:self[kArticleFocusedImageKey]];
}

- (NSURL*)bookImageURL{
    return [NSURL URLWithString:self[kArticleBookImageKey]];
}

- (NSArray*)tags{
    if (!_tags) {
        NSMutableArray *tags = [NSMutableArray array];
        NSArray *tagArray = self[kArticleTagsKey];
        if ([tagArray isKindOfClass:[NSArray class]]) {
            NSDictionary *aTagDict = tagArray.firstObject;
            if (aTagDict && ![aTagDict isKindOfClass:[NSDictionary class]]) {
                tagArray = [self tagArray];
            }
            for (NSDictionary *aTagDict in tagArray) {
                [tags addObject:[[Tag alloc] initWithDictionary:aTagDict]];
            }
        }
        _tags = tags;
    }
    return _tags;
}

- (NSArray*)tagArray{
    NSArray *tagArray = self[kArticleTagsIDsKey];
    return tagArray;
}

- (Author*)author{
    if (!_author) {
        _author = [kSharedNewsConfig.dataSource authorFromArticle:self];
    }
    return _author;
}

- (NSArray<Author*>*)authors{
    if (!_authors) {
        _authors = [kSharedNewsConfig.dataSource authorsFromArticle:self];
    }
    return _authors;
}

- (NSAttributedString*)attributedAuthorNamesForDetail{
    NSMutableAttributedString *authorNames = [NSMutableAttributedString new];
    NSInteger index = 0;
    for (Author *anAuthor in self.authors) {
        if (index != 0) {
            [authorNames appendString:@","];
        }
        
        [authorNames appendAttributedString:anAuthor.attributedNameForDetails];
        index++;
    }
    return authorNames;
}

- (NSDate*)createdDate{
    NSTimeInterval interval = [self[kArticleCreatedKey] doubleValue];
    if (interval < 12*60*60) {
        NSString *format = @"dd MMM yyyy";
        static NSDateFormatter *df;
        if(!df) df = [NSDateFormatter new];
        [df setDateFormat:format];
        NSString *createdString = self[kArticleCreatedKey];
        NSDate *date = [df dateFromString:createdString];
        if (!date) {
            [df setDateFormat:@"MMM dd yyyy"];
            date = [df dateFromString:createdString];
        }
        return date;
    }
    return [NSDate dateWithTimeIntervalSince1970:interval];
}

- (NSDate*)updatedDate{
    NSTimeInterval interval = [self[kArticleUpdatedKey] doubleValue];
    return [NSDate dateWithTimeIntervalSince1970:interval];
}

- (NSString*)description{
    NSMutableString *str = [NSMutableString new];
    [str appendFormat:@"%@ | ", [self objectId]];
    [str appendFormat:@"%@ | ", self.type];
    [str appendFormat:@"%@ ", [self title]];
    
    return str;
}

- (NSString *)debugDescription{
    return [NSString stringWithFormat:@"<%@: %p> %@", [self class], self, [self description]];
}

- (NSDictionary *)bodyAttributes{
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = kSharedLanguageManager.defaultTextAlignment;
    
    UIFont *font = [kSharedNewsConfig articleBodyFont];
    NSDictionary *attributes = @{NSFontAttributeName : font,
                                 NSForegroundColorAttributeName: [kSharedNewsConfig.dataSource articleBodyTextColor],
                                 NSParagraphStyleAttributeName : paragraphStyle
                                 };
    return attributes;

}

- (NSAttributedString *)attributedTitle{
    return [[NSAttributedString alloc] initWithString:self.title
                                           attributes:self.titleAttributes];
}

- (NSAttributedString*)attributedIssueTitle{
    NSMutableAttributedString *title = [NSMutableAttributedString new];
    [title appendAttributedString:self.attributedTitle];
    [title appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n" attributes:self.titleAttributes]];
    [title appendAttributedString:[[NSAttributedString alloc] initWithString:@"العدد" attributes:self.titleAttributes]];
    [title appendAttributedString:[[NSAttributedString alloc] initWithString:@" : " attributes:self.titleAttributes]];
    [title appendAttributedString:[[NSAttributedString alloc] initWithString:self.objectId attributes:self.titleAttributes]];
    return title;
}
    
- (NSAttributedString *)attributedSubTitle{
    NSMutableDictionary *attributes = [[self bodyAttributes] mutableCopy];
    attributes[NSFontAttributeName] = [kSharedNewsConfig articleSubTitleFont];
    return [[NSAttributedString alloc] initWithString:self.subTitle
                                           attributes:attributes];
}

- (NSAttributedString *)attributedTitleForDetail{
    NSMutableDictionary *attributes = [[self bodyAttributes] mutableCopy];
    attributes[NSForegroundColorAttributeName] = [kSharedNewsConfig.dataSource articleTitleTextColor];
    attributes[NSFontAttributeName] = [kSharedNewsConfig articleTitleFont];
    return [[NSAttributedString alloc] initWithString:self.title
                                           attributes:attributes];
}

- (NSAttributedString *)attributedSubTitleForDetail{
    NSMutableDictionary *attributes = [[self bodyAttributes] mutableCopy];
    attributes[NSFontAttributeName] = [kSharedNewsConfig articleSubTitleFont];
    return [[NSAttributedString alloc] initWithString:self.subTitle
                                           attributes:attributes];
}

- (NSAttributedString *)attributedSummary{
    NSMutableDictionary *attributes = [[self bodyAttributes] mutableCopy];
    attributes[NSFontAttributeName] = [kSharedNewsConfig articleSubTitleFont];
//    return [[NSAttributedString alloc] initWithString:self.summary
//                                           attributes:attributes];
    
    NSMutableAttributedString *summaryAuthor = [[[NSAttributedString alloc] initWithString:self.summary
                                                                                attributes:attributes] mutableCopy];
    if(self.author){
        NSString *authorName = [NSString stringWithFormat:@"%@  ", self.author.name];
        NSAttributedString *attributedAuthorName = [[NSAttributedString alloc] initWithString:authorName
                                                                                   attributes:[self authorNameAttribues]];
        [summaryAuthor appendString:@"\n"];
        [summaryAuthor appendAttributedString:attributedAuthorName];
    };
    return summaryAuthor;
}

- (NSDictionary*)authorNameAttribues{
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = kSharedLanguageManager.reversedTextAlignment;
    
    UIFont *font = [[kSharedNewsConfig.dataSource fontForLanguage:kSharedLanguageManager.language] fontWithSize:14];
    return @{NSFontAttributeName : font,
             NSForegroundColorAttributeName: [kSharedNewsConfig.dataSource titleColor],
             NSParagraphStyleAttributeName : paragraphStyle
             };
    
}

- (NSAttributedString*)attributedFeaturedImageCaption{
    NSMutableDictionary *attributes = [[self bodyAttributes] mutableCopy];
    attributes[NSFontAttributeName] = kSharedNewsConfig.articleImageCaptionFont;
    return [[NSAttributedString alloc] initWithString:self.featuredImageCaption
                                           attributes:attributes];
}

- (NSAttributedString*)attributedImageCaptionForText:(NSString*)captionText{
    NSMutableDictionary *attributes = [[self bodyAttributes] mutableCopy];
    attributes[NSFontAttributeName] = kSharedNewsConfig.articleSubTitleFont;
    return [[NSAttributedString alloc] initWithString:captionText
                                           attributes:attributes];
}


- (NSAttributedString *)attributedTag{
    Tag *firstTag = [self.tags firstObject];
    if (firstTag) {
        NSString *tagString = [NSString stringWithFormat:@" %@ ", firstTag.title];
        return [[NSAttributedString alloc] initWithString:tagString
                                               attributes:self.tagAttributes];
    }
    return nil;
}

//- (NSDictionary *)dateAttributes{
//    return [kSharedNewsConfig.dataSource articleDateAttributes];
////    if (!_dateAttributes) {
////        NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
////        paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
////        paragraphStyle.alignment = kSharedLanguageManager.defaultTextAlignment;
////
////        UIFont *font = [[kSharedNewsConfig fontForLanguage:kSharedLanguageManager.language] fontWithSize:14];
////        _dateAttributes = @{NSFontAttributeName : font,
////                            NSForegroundColorAttributeName: [UIColor colorWithHex:0x707070],
////                            NSParagraphStyleAttributeName : paragraphStyle
////                            };
////    }
////
////    return _dateAttributes;
//}

- (NSAttributedString *)attributedDateTextForDetails{
    return [[NSAttributedString alloc] initWithString:self.dateText
                                           attributes:[kSharedNewsConfig.dataSource articleDateAttributes]];
}

- (NSAttributedString *)attributedDateText{
    return [[NSAttributedString alloc] initWithString:self.dateText
                                           attributes:[kSharedNewsConfig.dataSource articleDateAttributes]];
}

- (CGFloat)widthOfAttributedTagWithHeight:(CGFloat)height{
    CGRect textRect = [self.attributedTag boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, height)
                                                       options:NSStringDrawingUsesLineFragmentOrigin
                                                       context:nil];
    return ceil(textRect.size.width);
}

- (CGFloat)heightOfTextOnlyTitleWithWidth:(CGFloat)width{
    CGRect textRect = [self.textOnlyTitle boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX)
                                                       options:NSStringDrawingUsesLineFragmentOrigin
                                                       context:nil];
    return ceil(textRect.size.height);
}

- (CGFloat)heightOfTagTitleWriterWithWidth:(CGFloat)width{
    CGRect textRect = [self.tagTitleWriter boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX)
                                                            options:NSStringDrawingUsesLineFragmentOrigin
                                                            context:nil];
    return ceil(textRect.size.height);
}


- (CGFloat)heightOfAttributedTitleWithWidth:(CGFloat)width{
    return [self heightOfAttributedTitleWithWidth:width
                                  ignoreRect:CGRectZero];
}

- (CGFloat)heightOfAttributedTitleWithWidth:(CGFloat)width ignoreRect:(CGRect)ignoreRect{
    CGRect textRect = [self.attributedTitle boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX)
                                                         options:NSStringDrawingUsesLineFragmentOrigin
                                                         context:nil];
    return ceil(textRect.size.height);
}

- (void)requestArticleDetailsWithBlock:(CMBooleanResultBlock)block{
    if (self.customSearched) {
        NSString *slug = [self.URL.absoluteString stringByReplacingOccurrencesOfString:@"https://alarab.co.uk/" withString:@""];
        [Article loadSlug:slug completionHandler:^(id object, NSError * _Nullable error) {
            if (!error) {
                NSDictionary *articleDict = object[@"data"][0];
                for (NSString *aKey in articleDict.allKeys) {
                    self[aKey] = articleDict[aKey];
                }
                if(block) block(YES, nil);
            }else{
                if(block) block(NO, error);
            }
            
        }];
    }else{
        NSString *urlString = [kSharedNewsConfig.dataSource articleDetailsURLForID:self.objectId];
        [NetworkManager getEndPoint:urlString
                            success:^(id responseObject) {
                                NSDictionary *data = responseObject[@"data"];
                                for (NSString *aKey in [data allKeys]) {
                                    self[aKey] = data[aKey];
                                }
                                if(block) block(YES, nil);
                            } failure:^(id responseObject, NSError * _Nonnull error) {
                                if(block) block(NO, error);
                            }];
    }
}

- (UIColor*)backgroundColor{
    NSString *colorString = self[kArticleBGColorKey][@"color"];
    CGFloat opacity = [self[kArticleBGColorKey][@"opacity"] floatValue];
    return [[UIColor colorWithHexString:colorString] colorWithAlphaComponent:opacity];
}

- (UIColor*)cardColor{
    NSString *colorString = self[kArticleCardColorKey][@"color"];
    CGFloat opacity = [self[kArticleCardColorKey][@"opacity"] floatValue];
    return [[UIColor colorWithHexString:colorString] colorWithAlphaComponent:opacity];
}


- (void)requestArticleDetailsForPrefetching{
    if (self.objectId == nil) return;
    if ([NSThread isMainThread]) {
        [self performSelectorInBackground:@selector(requestArticleDetailsForPrefetching)
                               withObject:nil];
        return;
    }
    while ([ArticleCell imagesBeingLoaded]) {
        sleep(1);
    }
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSLog(@"Starting prefetching now");
    });
    
    NSString *endPoint = [kSharedNewsConfig.dataSource articleDetailsURLForID:self.objectId];
    NSString *URLString = [API_BASE_URL stringByAppendingPathComponent:endPoint];
    
    [NetworkManager callURL:URLString
                      cache:YES
               loadOnCached:NO
                   priority:0
                   withDict:nil
                     method:@"GET"
                       JSON:YES
                    success:nil
                    failure:nil];
}

- (NSArray *)paragraphs{
    return self[kArticleParagraphKey];
}

- (NSString*)htmlString{
    return @"";
}

- (NSComparisonResult)compare:(Article*)other{
    if([[self createdDate] compare:[other createdDate]] == NSOrderedSame){
        return [other.objectId compare:self.objectId];
    }
    return [[self createdDate] compare:[other createdDate]];
}

- (BOOL)isEqual:(Article*)object{
    return [self.objectId isEqualToString:object.objectId];
}

- (NSUInteger)hash{
    return self.objectId.integerValue;
}

- (NSArray *)relatedArticles{
    if (!_relatedArticles) {
        NSMutableArray *relatedArticles = [NSMutableArray array];
        NSArray *relatedArticlesArray = self[@"related"];
        for (NSDictionary *anArticleDict in relatedArticlesArray) {
            Article *anArticle = [[Article alloc] initWithDictionary:anArticleDict];
            anArticle[kArticleTypeLabelKey] = kSharedLanguageManager.relatedArticlesText;
            anArticle[kArticleTypeKey] = @"Related";
            [relatedArticles addObject:anArticle];
        }
        _relatedArticles = relatedArticles;
    }
    return _relatedArticles;
}

+ (NSArray<Article*>*)bookmarkedArticles{
    NSMutableArray *articles = [NSMutableArray array];
    NSString *savedArticlesJSON = [[TMCache sharedCache] objectForKey:@"bookmarked_articles"];
    NSArray *savedArticlesArray = [savedArticlesJSON JSONObject];
    for (NSDictionary *anArticleDict in savedArticlesArray) {
        Article *article =[[Article alloc] initWithDictionary:anArticleDict];
        article[kArticleTypeLabelKey] = kSharedLanguageManager.language == LanguageNameEnglish ? @"Saved Articles" : @"المقالات المحفوظة";
        [articles addObject:article];
    }
    return articles;
}

+ (void)saveBookmarkedArticles:(NSArray<Article*>*)articles{
    NSMutableArray *saveArticlesArray = [NSMutableArray array];
    for (Article *anArticle in articles) {
        NSDictionary *bookmarkDict = [anArticle bookmarkDictionary];
        [saveArticlesArray addObject:bookmarkDict];
    }
    NSString *saveArticlesJSON = [saveArticlesArray JSONString];
    [[TMCache sharedCache] setObject:saveArticlesJSON forKey:@"bookmarked_articles"];
}

- (NSDictionary*)bookmarkDictionary{
    NSMutableDictionary *objToSave = [internalObject mutableCopy];
    for (NSString *aKey in updateObject.allKeys) {
        objToSave[aKey] = updateObject[aKey];
    }
    return objToSave;
}

- (void)bookmark{
    NSMutableArray<Article*> *bookmarkedArticles = [[Article bookmarkedArticles] mutableCopy];
    if ([bookmarkedArticles containsObject:self]) {
        [bookmarkedArticles removeObject:self];
    }else{
        [bookmarkedArticles addObject:self];
    }
    [Article saveBookmarkedArticles:bookmarkedArticles];
}

- (NSURL *)shareURL{
    NSString *articleURLString = self[kArticleURLKey];
    articleURLString = [articleURLString stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet];
    return [NSURL URLWithString:articleURLString];
}

- (BOOL)isBookmarked{
    return [[Article bookmarkedArticles] containsObject:self];
}

- (NSAttributedString *)attributedCompoundText{
    NSMutableAttributedString *text = [NSMutableAttributedString new];
    [text appendAttributedString:self.attributedTitle];
    [text appendString:@"\n"];
    
    if (self.dateText.length) {
        [text appendAttributedString:self.attributedDateText];
        [text appendString:@"\n"];
    }
    
    
    if (self.subTitle.length > 0) {
        [text appendAttributedString:self.attributedSubTitle];
    }else{
        [text appendAttributedString:self.attributedSummary];
    }
    
    return text;
}

- (NSString *)compoundText{
    NSMutableString *text = [NSMutableString new];
    [text appendString:self.title];
    [text appendString:@"\n"];
    
    if (self.dateText.length) {
        [text appendString:self.dateText];
        [text appendString:@"\n"];
    }
    
    [text appendString:self.summary];
    return text;
}

- (NSAttributedString *)textOnlyTitle{
    Tag *firstTag = self.tags.firstObject;
    
    NSMutableAttributedString *text = [NSMutableAttributedString new];
    if (firstTag) {
        NSAttributedString *tagTitle = [[NSAttributedString alloc] initWithString:firstTag.title attributes:self.tagAttributes];
        [text appendAttributedString:tagTitle];
        
        [text appendAttributedString:[[NSAttributedString alloc] initWithString:@" / " attributes:self.tagAttributes]];
    }
    
    [text appendAttributedString:self.attributedTitle];
    if (self.author) {
        [text appendString:@"\n"];
        [text appendAttributedString:self.author.attributedName];
    }
    
    return text;
}

- (NSAttributedString *)tagTitleWriter{
    Tag *firstTag = self.tags.firstObject;
    
    NSMutableAttributedString *text = [NSMutableAttributedString new];
    if (firstTag) {
        NSAttributedString *tagTitle = [[NSAttributedString alloc] initWithString:firstTag.title attributes:self.tagAttributes];
        [text appendAttributedString:tagTitle];
        [text appendAttributedString:[[NSAttributedString alloc] initWithString:@" / " attributes:self.tagAttributes]];
    }
    
    [text appendAttributedString:self.attributedTitle];
    
    if (self.author) {
        [text appendString:@"\n"];
        [text appendAttributedString:self.author.attributedName];
    }
    
    return text;
}

- (id)copyWithZone:(nullable NSZone *)zone{
    Article *copyArticle = [[Article alloc] initWithDictionary:internalObject];
    for (id aKey in updateObject.allKeys) {
        copyArticle[aKey] = updateObject[aKey];
    }
    return copyArticle;
}

@end

//
//  API_Defines.h
//  NewsCore
//
//  Created by Shashank Patel on 14/04/16.
//
#define CURRENT_USER_KEY    @"current_user"

#define BUNDLE_IDENTIFIER   [[NSBundle mainBundle] bundleIdentifier]
#define API_BASE_URL        [[NewsConfig sharedNewsConfig] baseURL]

#define     USER_DETAILS    @""
#define     LOGIN           @""
#define     REGISTER        @""

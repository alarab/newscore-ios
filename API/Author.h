//
//  Author.h
//  NewsCore
//
//  Created by Shashank Patel on 16/03/19.
//  Copyright © 2019 Shashank Patel. All rights reserved.
//

#import "NCObject.h"

@interface Author : NCObject

@property (nonatomic, strong)   NSString            *name;
@property (nonatomic, strong)   NSAttributedString  *attributedName, *attributedNameForDetails;
@property (nonatomic, strong)   NSDictionary        *nameAttributes;
@property (nonatomic, strong)   NSURL               *imageURL;

- (CGFloat)heightOfattributedNameWithWidth:(CGFloat)width;
- (CGFloat)heightOfattributedNameWithWidth:(CGFloat)width ignoreRect:(CGRect)ignoreRect;

@end

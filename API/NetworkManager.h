//
//  NetworkManager.h
//  NewsCore
//
//  Created by Shashank Patel on 21/12/17.
//

#import <Foundation/Foundation.h>
#import "Definitions.h"

@interface NetworkManager : NSObject

+ (void)cancelAllRequests;

+ (void)downloadFileFromURLString:(NSString*)URLString
                           toPath:(NSString*)localPath
                         progress:(void (^)(NSProgress *downloadProgress)) downloadProgressBlock
                          success:(void (^)(id  responseObject))success
                          failure:(void (^)(id  responseObject, NSError * error))failure;

+ (void)uploadToEndPoint:(NSString*)endPoint
              parameters:(NSDictionary*)parameters
                    data:(NSData*)fileData
                fileName:(NSString*)fileName
                  isJSON:(BOOL)isJSON
             uploadBlock:(nullable void (^)(NSProgress *uploadProgress)) uploadProgressBlock
           downloadBlock:(nullable void (^)(NSProgress *downloadProgress)) downloadProgressBlock
                 success:(void (^)(id  responseObject))success
                 failure:(void (^)(id  responseObject, NSError * error))failure;
    
+ (void)uploadToEndPoint:(NSString*)endPoint
              parameters:(NSDictionary*)parameters
                 fileURL:(NSURL*)fileURL
                fileName:(NSString*)fileName
                  isJSON:(BOOL)isJSON
             uploadBlock:(nullable void (^)(NSProgress *uploadProgress)) uploadProgressBlock
           downloadBlock:(nullable void (^)(NSProgress *downloadProgress)) downloadProgressBlock
                 success:(void (^)(id  responseObject))success
                 failure:(void (^)(id  responseObject, NSError * error))failure;

+ (void)callURL:(NSString*)URLString
          cache:(BOOL)cache
   loadOnCached:(BOOL)loadOnCached
       priority:(float)priority
       withDict:(NSDictionary*)dict
         method:(NSString*)method
           JSON:(BOOL)isJSON
        success:(void (^)(id  _Nonnull responseObject))success
        failure:(void (^)(id  _Nonnull responseObject, NSError * _Nonnull error))failure;

+ (void)callURL:(NSString*)URLString
          cache:(BOOL)cache
       withDict:(NSDictionary*)dict
         method:(NSString*)method
           JSON:(BOOL)isJSON
        success:(void (^)(id  _Nonnull responseObject))success
        failure:(void (^)(id  _Nonnull responseObject, NSError * _Nonnull error))failure;

+ (void)callEndPoint:(NSString*)URLString
                cache:(BOOL)cache
            withDict:(NSDictionary*)dict
              method:(NSString*)method
                JSON:(BOOL)isJSON
             success:(void (^)(id  responseObject))success
             failure:(void (^)(id  responseObject, NSError * error))failure;

+ (void)getEndPoint:(NSString*)endPoint
            success:(void (^)(id  responseObject))success
            failure:(void (^)(id  responseObject, NSError * _Nonnull error))failure;

+ (void)getEndPoint:(NSString*)URLString
            cache:(BOOL)cache
         parameters:(NSDictionary*)parameters
            success:(void (^)(id  responseObject))success
            failure:(void (^)(id  responseObject, NSError * error))failure;

+ (void)postEndPoint:(NSString*)endPoint
          parameters:(NSDictionary*)parameters
             success:(void (^)(id  responseObject))success
             failure:(void (^)(id  responseObject, NSError * error))failure;

+ (void)patchEndPoint:(NSString*)endPoint
           parameters:(NSDictionary*)parameters
              success:(void (^)(id  responseObject))success
              failure:(void (^)(id  responseObject, NSError * _Nonnull error))failure;

+ (void)deleteEndPoint:(NSString*)endPoint
            parameters:(NSDictionary*)parameters
               success:(void (^)(id  responseObject))success
               failure:(void (^)(id  responseObject, NSError * _Nonnull error))failure;

@end

//
//  NCObject.h
//  BeLive
//
//  Created by Shashank Patel on 14/04/16.
//  Copyright © 2018 BeLive. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NetworkManager.h"

#define kUnImplementedError [NSError errorWithDomain:@"Local" code:-101 userInfo:@{@"Error" : @"Not implemented yet"}]

@interface NCObject : NSObject{
    NSMutableDictionary *internalObject, *updateObject;
}

@property (nullable, nonatomic, strong) NSString *objectId;

typedef void (^CMObjectResultBlock)(id object, NSError *_Nullable error);
typedef void (^CMBooleanResultBlock)(BOOL succeeded, NSError *_Nullable error);
typedef void (^CMProgressBlock)(CGFloat progress);
typedef void (^CMBooleanMessageResultBlock)(BOOL succeeded, NSString *_Nullable message, NSError *_Nullable error);
typedef void (^CMArrayResultBlock)(NSArray *_Nullable objects, NSError *_Nullable error);
typedef void (^CMDictionaryResultBlock)(NSDictionary *_Nullable response, NSError *_Nullable error);
typedef void (^CMStringResultBlock)(NSString *_Nullable response, NSError *_Nullable error);

- (nullable instancetype)initWithDictionary:(nonnull NSDictionary*)dict;
- (void)resetDetails:(nonnull NSDictionary*)dict;

- (nullable id)objectForKeyedSubscript:(nonnull NSString *)key;
- (void)setObject:(nullable id)object forKeyedSubscript:(nonnull NSString *)key;

- (void)fetchInBackgroundWithBlock:(nullable CMBooleanResultBlock)block;
- (void)fetchInBackground;

- (void)updateInBackgroundWithBlock:(nullable CMBooleanResultBlock)block;
- (void)updateInBackground;

- (void)saveInBackgroundWithBlock:(nullable CMBooleanResultBlock)block;
- (void)saveInBackground;

- (void)deleteInBackgroundWithBlock:(nullable CMBooleanResultBlock)block;
- (void)deleteInBackground;

- (void)discardChanges;

@end

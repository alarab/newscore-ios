//
//  NCObject.m
//  BeLive
//
//  Created by Shashank Patel on 14/04/16.
//  Copyright © 2018 BeLive. All rights reserved.
//

#import "NCObject.h"

@implementation NCObject

- (instancetype)init{
    if (self = [super init]) {
        internalObject = [[NSMutableDictionary alloc] init];
        updateObject = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (instancetype)initWithDictionary:(NSDictionary*)dict{
    if (self = [self init]) {
        for (NSString *key in dict.allKeys) {
            internalObject[key] = dict[key];
        }
    }
    return self;
}

- (NSString *)objectId{
    return self[@"id"];
}

- (void)resetDetails:(NSDictionary*)dict{
    for (NSString *key in dict.allKeys) {
        internalObject[key] = dict[key];
    }
}

- (void)discardChanges{
    updateObject = [[NSMutableDictionary alloc] init];
}

- (nullable id)objectForKey:(NSString *)key{
    return updateObject[key] ? updateObject[key] : internalObject[key];
}

- (void)setObject:(id)object forKey:(NSString *)key{
    updateObject[key] = object;
}

- (nullable id)objectForKeyedSubscript:(NSString *)key{
    return updateObject[key] ? updateObject[key] : internalObject[key];
}

- (void)setObject:(id)object forKeyedSubscript:(NSString *)key{
    updateObject[key] = object;
}

- (void)deleteInBackgroundWithBlock:(nullable CMBooleanResultBlock)block{
    block(NO, kUnImplementedError);
}

- (void)deleteInBackground{
    [self deleteInBackgroundWithBlock:nil];
}

- (void)updateInBackgroundWithBlock:(nullable CMBooleanResultBlock)block{
    block(NO, kUnImplementedError);
}

- (void)updateInBackground{
    [self updateInBackgroundWithBlock:nil];
}

- (void)fetchInBackgroundWithBlock:(nullable CMBooleanResultBlock)block{
    block(NO, kUnImplementedError);
}

- (void)fetchInBackground{
    [self fetchInBackgroundWithBlock:nil];
}

- (void)saveInBackgroundWithBlock:(nullable CMBooleanResultBlock)block{
    block(NO, kUnImplementedError);
}

- (void)saveInBackground{
    [self saveInBackgroundWithBlock:nil];
}



@end

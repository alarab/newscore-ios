
//
//  NewsConfig.h
//  NewsCore
//
//  Created by Shashank Patel on 18/03/19.
//  Copyright © 2019 Shashank Patel. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol NewsConfigDelegate <NSObject>

@optional

- (void)newsDataLoadCompletedSuccessfully;
- (void)newsDataLoadComnpletedWithError:(NSError*)error;

- (void)menuLoadComnpletedSuccessfully;

@end

@protocol NewsConfigDataSource <NSObject>

@required

- (NSString*)baseURL;
- (NSString*)baseURLV1;
- (NSString*)homeURL;
- (NSString*)menuURL;
- (NSString*)articleDetailsURLForID:(NSString*)articleID;
- (NSString*)taxonomyURLForID:(NSString*)taxonomyID index:(NSInteger)index;
- (NSString*)authorArticlesURLForID:(NSString*)taxonomyID index:(NSInteger)index;
- (NSString*)searchURLForSearchText:(NSString*)searchText index:(NSInteger)index;
- (NSString*)articleDetailsURLForSlug:(NSString*)slug;
- (NSString*)issueURLForID:(NSString*)ID;
- (NSString*)archiveURLForIndex:(NSInteger)index;

- (NSArray*)menuFromResponse:(id)responseDict;

- (Author*)authorFromArticle:(Article*)article;
- (NSArray<Author*>*)authorsFromArticle:(Article*)article;

- (NSArray<Article*>*)articlesFromResponse:(id)response;
- (NSArray<Article*>*)articlesFromLinearResponse:(id)response;
- (NSArray<Article*>*)articlesFromAuthorResponse:(id)response;
- (NSArray<Article*>*)articlesFromSearchResponse:(id)response;
- (NSArray<Article*>*)articlesFromIssueResponse:(id)response;
- (NSArray<Article*>*)articlesFromArchiveResponse:(id)response;

- (UIColor*)navigationBarColor;
- (UIColor*)borderColor;
- (UIColor*)tagBackgroundColor;
- (UIColor*)titleColor;
- (UIColor*)articleTitleTextColor;
- (UIColor*)articleBodyTextColor;
- (UIColor*)navigationTextColor;
- (UIColor*)themeImageTintColor;
- (UIImage*)imageForLoader;
- (UIFont*)fontForLanguage:(LanguageName)languageName;
- (UIFont*)boldFontForLanguage:(LanguageName)languageName;
- (CGFloat)articleTitleFontSize;
- (NSDictionary *)articleDateAttributes;
- (LanguageName)preferredLanguage;
- (BOOL)isSupportedLanguage:(LanguageName)languageName;

@end

@interface NewsConfig : NSObject

@property (nonatomic, strong)   NSObject<NewsConfigDataSource>  *dataSource;
@property (nonatomic, strong)   NSArray<Article*>               *articles;
@property (nonatomic, strong)   NSArray<NSDictionary*>          *menu;
@property (nonatomic)           double                          articleBodyFontSize;

+ (NewsConfig*)sharedNewsConfig;

- (void)addDelegate:(id<NewsConfigDelegate>)aDelegate;

- (NSString*)baseURL;
- (UIColor*)navigationBarColor;
- (UIColor*)borderColor;
- (UIFont*)fontForLanguage:(LanguageName)languageName;
- (UIFont*)articleBodyFont;
- (UIFont *)articleTitleFont;
- (UIFont *)articleSubTitleFont;
- (UIFont *)articleImageCaptionFont;

- (void)loadHomeAndMenu;
- (void)loadHomeAndMenu:(BOOL)shouldUseCache;

@end

NS_ASSUME_NONNULL_END

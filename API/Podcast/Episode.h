//
//  Episode.h
//  Ahval English
//
//  Created by Shashank Patel on 04/05/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Episode : NCObject

@property(nonatomic, readonly)  NSString        *title, *summary, *publishedDateString;
@property(nonatomic, readonly)  NSURL           *imageURL, *imageOriginalURL, *waveformURL, *playbackURL, *nextEpisodesURL;
@property(nonatomic, readonly)  NSDate          *publishedDate;
@property(nonatomic, readonly)  NSTimeInterval  duration;

+ (void)fetchEpisodesWithCompletionBlock:(CMArrayResultBlock)block;

@end

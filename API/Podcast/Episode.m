
//
//  Episode.m
//  Ahval English
//
//  Created by Shashank Patel on 04/05/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import "Episode.h"
#import <DTFoundation/NSURL+DTComparing.h>
#import <DTFoundation/NSString+DTUtilities.h>

@implementation Episode

+ (void)fetchEpisodesWithCompletionBlock:(CMArrayResultBlock)block{
    [NetworkManager callURL:@"https://api.spreaker.com/v2/users/ahvalpod/episodes"
                      cache:YES
                   withDict:nil
                     method:@"GET"
                       JSON:YES
                    success:^(id  _Nonnull responseObject) {
                        NSArray *items = responseObject[@"response"][@"items"];
                        NSMutableArray *episodes = [NSMutableArray array];
                        for (NSDictionary *episodeDict in items) {
                            Episode *anEpisode = [[Episode alloc] initWithDictionary:episodeDict];
                            anEpisode[@"next_url"] = responseObject[@"response"][@"next_url"];
                            [episodes addObject:anEpisode];
                        }
                        block(episodes, nil);
                    } failure:^(id  _Nonnull responseObject, NSError * _Nonnull error) {
                        block(nil, error);
                    }];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict{
    if(self = [super initWithDictionary:dict]){
        [self loadMoreDetails];
    }
    return self;
}

- (void)loadMoreDetails{
    NSString *episodeURL = [NSString stringWithFormat:@"https://api.spreaker.com/v2/episodes/%@", self.objectId];
    [NetworkManager callURL:episodeURL
                      cache:YES
               loadOnCached:NO
                   priority:0.1
                   withDict:nil
                     method:@"GET"
                       JSON:YES
                    success:^(id  _Nonnull responseObject) {
                        NSDictionary *episodeDetails = responseObject[@"response"][@"episode"];
                        [self resetDetails:episodeDetails];
                    } failure:^(id  _Nonnull responseObject, NSError * _Nonnull error) {
                        
                    }];
}

/*
 
 "episode_id": 17828789,
 "type": "RECORDED",
 "title": "Ağrıyla mücadele yolları: Akapunktur iyileştirir",
 "duration": 2366790,
 "explicit": false,
 "show_id": 3361493,
 "author_id": 10753342,
 "site_url": "https://www.spreaker.com/episode/17828789",
 "image_url": "https://d1bm3dmew779uf.cloudfront.net/large/cff2d54e4089aa0e5c84159c5eaf9c64.jpg",
 "image_original_url": "https://d3wo5wojvuv7l.cloudfront.net/images.spreaker.com/original/cff2d54e4089aa0e5c84159c5eaf9c64.jpg",
 "published_at": "2019-05-03 20:11:06",
 "download_enabled": true,
 "waveform_url": "https://d3770qakewhkht.cloudfront.net/episode_17828789.gz.json?v=cYuXqn",
 "download_url": "https://api.spreaker.com/v2/episodes/17828789/download.mp3",
 "playback_url": "https://api.spreaker.com/v2/episodes/17828789/play.mp3"
 
 */

- (NSString *)objectId{
    return [self[@"episode_id"] stringValue];
}

- (NSURL*)nextEpisodesURL{
    return [NSURL URLWithString:self[@"next_url"]];
}

- (NSString*)title{
    NSString *title = self[@"title"]?:@"";
    return [title isKindOfClass:[NSString class]] ? title : @"";
}

- (NSString*)summary{
    NSString *summary = self[@"description"]?:@"";
    return [summary isKindOfClass:NSString.class] ? summary : @"";
}

- (NSTimeInterval)duration{
    return [self[@"duration"] integerValue] / 1000;
}

- (NSURL*)imageURL{
    return [NSURL URLWithString:self[@"image_url"]];
}

- (NSURL*)imageOriginalURL{
    return [NSURL URLWithString:self[@"image_original_url"]];
}

- (NSURL*)waveformURL{
    return [NSURL URLWithString:self[@"waveform_url"]];
}

- (NSURL*)playbackURL{
    return [NSURL URLWithString:self[@"playback_url"]];
}

- (NSDate*)publishedDate{
    static NSDateFormatter *df;
    if (!df) {
        df = [NSDateFormatter new];
        [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    }
    return [df dateFromString:self[@"published_at"]];
}

- (NSString*)publishedDateString{
    static NSDateFormatter *publishedDF;
    if (!publishedDF) {
        publishedDF = [NSDateFormatter new];
        [publishedDF setDateStyle:NSDateFormatterMediumStyle];
    }
    if(kSharedLanguageManager.language == LanguageNameArabic) {
        publishedDF.locale = [NSLocale localeWithLocaleIdentifier:@"ar_DZ"];
    }else if(kSharedLanguageManager.language == LanguageNameTurkish) {
        publishedDF.locale = [NSLocale localeWithLocaleIdentifier:@"tr_TR"];
    }
    return [publishedDF stringFromDate:self.publishedDate];
}

- (NSComparisonResult)compare:(Article*)other{
    return [other.objectId compare:self.objectId];
}

- (BOOL)isEqual:(Episode*)otherObject{
    return [otherObject.playbackURL isEqualToURL:self.playbackURL];
}

- (NSUInteger)hash{
    return [self.playbackURL.absoluteString hash];
}

@end

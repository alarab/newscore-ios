//
//  NCUser.m
//  NewsCore
//
//  Created by Shashank Patel on 24/02/18.
//

#import "NCUser.h"
#import "AFNetworking.h"
#import "Macros.h"
#import "NSUserDefaults-macros.h"
#import <TMCache/TMCache.h>

#define CURRENT_USER_KEY @"current_user"

@implementation NCUser

+ (void)load{
    [super load];
    //    defaults_remove(CURRENT_USER_KEY);
}

- (NSString*)objectId{
    return self[@"user_id"];
}

- (void)setToken:(NSString *)token{
    self[@"token"] = token;
}

- (NSString*)token{
    NSString *tokenString = self[@"token"];
    NSString *authorizationHeader = [NSString stringWithFormat:@"Bearer %@", tokenString];
    return authorizationHeader;
}

static NCUser *currentUser;

+ (NCUser*)currentUser{
    if (currentUser) {
        return currentUser;
    }
    
    NSData *currentUserData = defaults_object(CURRENT_USER_KEY);
    NSError *error;
    NSDictionary *currentUserDict =  [NSKeyedUnarchiver unarchivedObjectOfClass:[NSDictionary class]
                                                                       fromData:currentUserData
                                                                          error:&error];
    if(currentUserDict){
        currentUser = [[NCUser alloc] initWithDictionary:currentUserDict];
        return currentUser;
    }
    
    return nil;
}

- (instancetype)initWithDictionary:(NSDictionary *)dict{
    if (self = [super initWithDictionary:dict]) {
        [self performSelector:@selector(fetchInBackground)
                   withObject:nil afterDelay:1];
    }
    return self;
}

+ (void)logOut{
    defaults_remove(CURRENT_USER_KEY);
    [[TMCache sharedCache] removeAllObjects];
    currentUser = nil;
}

+ (void)logInWithEmail:(NSString*)email
              password:(NSString*)password
  inBackgrounWithBlock:(nullable NCUserResultBlock)block{
    UIDevice *device = [UIDevice currentDevice];
    NSString *identifier = [[device identifierForVendor] UUIDString];
//    NSString *deviceInfo = [@[[device model], [device systemName], [device systemVersion]] componentsJoinedByString:@","];
    
    NSDictionary *loginDict = @{@"email_address": email,
                                @"password" : password,
                                @"os_type" : @1,
                                @"token": identifier
                                };
    [NetworkManager callEndPoint:LOGIN
                           cache:NO
                        withDict:loginDict
                          method:@"POST"
                            JSON:YES
                         success:^(id responseObject) {
                             NCUser *user = [NCUser new];
                             NSDictionary *userDict = responseObject[@"data"][@"user"];
                             NSString *token = responseObject[@"data"][@"authorization_token"];
                             user.token = token;
                             
                             for (NSString *aKey in userDict.allKeys) {
                                 if(![userDict[aKey] isKindOfClass:[NSNull class]]){
                                     user[aKey] = userDict[aKey];
                                 }
                             }
                             
                             [user setCurrent];
                             [user fetchInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                                 if (succeeded) {
                                     block(user, nil);
                                 }
                             }];
                             NSLog(@"Token: %@", user.token);
                         } failure:^(id responseObject, NSError *error) {
                             NSLog(@"Message: %@", responseObject[@"message"]);
                             NSError *usableError = [NSError errorWithDomain:error.domain
                                                                        code:error.code
                                                                    userInfo:responseObject];
                             if (block) block(nil, usableError);
                         }];
}

- (void)fetchInBackgroundWithBlock:(CMBooleanResultBlock)block{
    if (block) block(self, nil);
    return;
    [NetworkManager getEndPoint:USER_DETAILS
                        success:^(id responseObject) {
                            NSDictionary *userDict = responseObject[@"data"];
                            for (NSString *key in [userDict allKeys]){
                                if ([userDict[key] isKindOfClass:[NSNull class]] ||
                                    !userDict[key]){
                                    continue;
                                }
                                self[key] = userDict[key];
                            }
                            [self setCurrent];
                            if (block) block(self, nil);
                        } failure:^(id responseObject, NSError *error) {
                            if (block) block(nil, error);
                        }];
}

- (void)fetchInBackground{
    [self fetchInBackgroundWithBlock:nil];
}

- (void)setCurrent{
    for (NSString *key in updateObject.allKeys) {
        internalObject[key] = updateObject[key];
    }
    NSError *error;
    NSData *saveData = [NSKeyedArchiver archivedDataWithRootObject:internalObject
                                             requiringSecureCoding:NO error:&error];
    defaults_set_object(CURRENT_USER_KEY, saveData);
    currentUser = self;
    updateObject = [NSMutableDictionary dictionary];
}

- (void)setCurrentTags{
}

- (void)signUpInBackgroundWithBlock:(nullable CMBooleanResultBlock)block{
    NSString *identifier =  [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSDictionary *signUpDict = @{@"first_name" : self[@"first_name"],
                                 @"last_name" : self[@"last_name"],
                                 @"email_address" : self[@"email_address"],
                                 @"contact" : self[@"contact"],
                                 @"dob" : self[@"dob"],
                                 @"password" : self[@"password"],
                                 @"password_confirmation" : self[@"password_confirmation"],
                                 @"time_zone" : @"Asia/Calcutta",
                                 @"token" : identifier,
                                 @"os_type" : @1,
                                 @"country_id" : self[@"country_id"],
                                 @"provience_id" : self[@"provience_id"],
                                 @"city_id" : self[@"city_id"],
                                 };
    
    NSURL *profilePictureURL = [[NSBundle mainBundle] URLForResource:@"profile_default" withExtension:@"png"];
    [NetworkManager uploadToEndPoint:REGISTER
                          parameters:signUpDict
                             fileURL:profilePictureURL
                            fileName:@"image"
                              isJSON:YES uploadBlock:^(NSProgress *uploadProgress) {
                                  
                              } downloadBlock:^(NSProgress *downloadProgress) {
                                  
                              } success:^(id responseObject) {
                                  NSLog(@"Message: %@", responseObject[@"message"]);
                                  if (block) block(YES, nil);
                              } failure:^(id responseObject, NSError *error) {
                                  NSInteger statusCode = [responseObject[@"status"] integerValue];
                                  if (block) block(NO, [NSError errorWithDomain:@"remote"
                                                                           code:statusCode
                                                                       userInfo:responseObject]);
                              }];
}

- (void)saveInBackgroundWithBlock:(nullable CMBooleanResultBlock)block{
    
}

- (void)saveInBackground{
    
}

- (NSString*)description{
    return [NSString stringWithFormat:@"Internal:%@ \n Update: %@", [internalObject description], [updateObject description]];
}

@end


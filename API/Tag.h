//
//  Tag.h
//  MEO
//
//  Created by Shashank Patel on 29/03/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import "NCObject.h"

NS_ASSUME_NONNULL_BEGIN

extern NSString *kArticleTagNameKey;

@interface Tag : NCObject

@property (nonatomic, strong)   NSString    *title;

@end

NS_ASSUME_NONNULL_END

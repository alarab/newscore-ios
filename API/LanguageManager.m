//
//  LanguageManager.m
//  NewsCore
//
//  Created by Shashank Patel on 16/03/19.
//  Copyright © 2019 Shashank Patel. All rights reserved.
//

#import "LanguageManager.h"
#import "AppDelegate.h"

@interface LanguageManager ()

@property (nonatomic, strong)   NSMutableArray<LanguageManagerDelegate>     *delegates;

@end

@implementation LanguageManager

@synthesize language = _language;

+ (LanguageManager*)sharedLanguageManager{
    static dispatch_once_t onceToken;
    static LanguageManager *sharedLanguageManager;
    dispatch_once(&onceToken, ^{
        sharedLanguageManager = [LanguageManager new];
    });
    return sharedLanguageManager;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        if (self.language == LanguageNameArabic) {
            [NSLocale ttt_overrideRuntimeLocale:[NSLocale localeWithLocaleIdentifier:@"ar"]];
            [NSBundle ttt_overrideLanguage:@"ar"];
        }else if (self.language == LanguageNameTurkish) {
            [NSLocale ttt_overrideRuntimeLocale:[NSLocale localeWithLocaleIdentifier:@"tr"]];
            [NSBundle ttt_overrideLanguage:@"at"];
        }else{
            [NSLocale ttt_overrideRuntimeLocale:[NSLocale localeWithLocaleIdentifier:@"en"]];
            [NSBundle ttt_overrideLanguage:@"en"];
        }
    }
    return self;
}

- (void)addDelegate:(id<LanguageManagerDelegate>)aDelegate{
    if (!self.delegates) self.delegates = [NSMutableArray<LanguageManagerDelegate> array];
    [self.delegates addObject:aDelegate];
}

- (LanguageName)language{
    if (_language == LanguageNameUnknown) {
        NSString *prefLanguage = defaults_object(@"lang");
        if ([prefLanguage isEqualToString:@"en"]) {
            _language = LanguageNameEnglish;
        }else if ([prefLanguage isEqualToString:@"ar"]) {
            _language = LanguageNameArabic;
        }else if ([prefLanguage isEqualToString:@"tr"]) {
            _language = LanguageNameTurkish;
        }else{
            _language = [kSharedNewsConfig.dataSource preferredLanguage];
            [self setLanguage:_language];
            
        }
    }
    return _language;
}

- (void)setLanguage:(LanguageName)language{
    switch (language) {
        case LanguageNameArabic:
            defaults_set_object(@"lang", @"ar");
            break;
        case LanguageNameEnglish:
            defaults_set_object(@"lang", @"en");
            break;
        case LanguageNameTurkish:
            defaults_set_object(@"lang", @"tr");
            break;
        default:
            break;
    }
    _language = language;
    if (_language == LanguageNameArabic) {
        [NSLocale ttt_overrideRuntimeLocale:[NSLocale localeWithLocaleIdentifier:@"ar"]];
        [NSBundle ttt_overrideLanguage:@"ar"];
    }else if (_language == LanguageNameTurkish) {
        [NSLocale ttt_overrideRuntimeLocale:[NSLocale localeWithLocaleIdentifier:@"tr"]];
        [NSBundle ttt_overrideLanguage:@"tr"];
    }else{
        [NSLocale ttt_overrideRuntimeLocale:[NSLocale localeWithLocaleIdentifier:@"en"]];
        [NSBundle ttt_overrideLanguage:@"en"];
    }
    [NetworkManager cancelAllRequests];
}

- (NSTextAlignment)defaultTextAlignment{
    return [self isRTL] ? NSTextAlignmentRight : NSTextAlignmentLeft;
}

- (NSTextAlignment)reversedTextAlignment{
    switch (self.defaultTextAlignment) {
        case NSTextAlignmentRight:
            return NSTextAlignmentLeft;
            break;
        case NSTextAlignmentLeft:
            return NSTextAlignmentRight;
            break;
        default:
            return self.defaultTextAlignment;
            break;
    }
}

- (NSString *)languageSuffix{
    switch (self.language) {
        case LanguageNameArabic:
            return @"ar";
            break;
        case LanguageNameTurkish:
            return @"tr";
            break;
        case LanguageNameEnglish:
            return @"en";
            break;
        default:
            return @"";
            break;
    }
}

- (UISemanticContentAttribute)semanticAttribute{
    //Does the opposite. Apps are designed in RTL and reveresed in LTR Languages.
    switch (self.language) {
        case LanguageNameArabic:
            return UISemanticContentAttributeForceLeftToRight;
            break;
        default:
            return UISemanticContentAttributeForceRightToLeft;
            break;
    }
}

- (UISemanticContentAttribute)reversedSemanticAttribute{
    //Does the opposite. Apps are designed in RTL and reveresed in LTR Languages.
    switch (self.language) {
        case LanguageNameArabic:
            return UISemanticContentAttributeForceRightToLeft;
            break;
        default:
            return UISemanticContentAttributeForceLeftToRight;
            break;
    }
}

- (NSString*)languageChangeTitle{
    NSString *changeLangTitle;
    switch (self.language) {
        case LanguageNameArabic:
            changeLangTitle = @"العربية";
            break;
        case LanguageNameEnglish:
            changeLangTitle = @"Change Language";
            break;
        case LanguageNameTurkish:
            changeLangTitle = @"Dili değiştir";
            break;
        case LanguageNameUnknown:
            changeLangTitle = @"Change Language";
            break;
    }
    return changeLangTitle;
}

- (NSString*)languageChangeTitleShort{
    NSString *changeLangTitle;
    switch (self.language) {
        case LanguageNameArabic:
            changeLangTitle = @"ع";
            break;
        case LanguageNameEnglish:
            changeLangTitle = @"En";
            break;
        case LanguageNameTurkish:
            changeLangTitle = @"Dili değiştir";
            break;
        default:
            changeLangTitle = @"En";
            break;
    }
    return changeLangTitle;
}

- (BOOL)isRTL{
    switch (self.language) {
        case LanguageNameArabic:
            return YES;
            break;
        default:
            return NO;
            break;
    }
}

- (void)showLanguageChangeUI{
    AppDelegate *appDelegate = (AppDelegate*)kAppDelegate;
    NSMutableArray *languageSelectionTitles = [NSMutableArray array];
    if ([kSharedNewsConfig.dataSource isSupportedLanguage:LanguageNameEnglish]) {
        [languageSelectionTitles addObject:@"English"];
    }
    if ([kSharedNewsConfig.dataSource isSupportedLanguage:LanguageNameTurkish]) {
        [languageSelectionTitles addObject:@"Turkish"];
    }
    if ([kSharedNewsConfig.dataSource isSupportedLanguage:LanguageNameEnglish]) {
        [languageSelectionTitles addObject:@"عربي"];
    }
    
    [UIAlertController showAlertInViewController:appDelegate.drawerController
                                       withTitle:[self languageChangeTitle]
                                         message:nil
                               cancelButtonTitle:@"Cancel"
                          destructiveButtonTitle:nil
                               otherButtonTitles:languageSelectionTitles
                                        tapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
                                            if (buttonIndex != controller.cancelButtonIndex) {
                                                LanguageName previousLanguage = self.language;
                                                NSInteger actualIndex = buttonIndex - 2;
                                                NSString *selectedLanguage = [languageSelectionTitles objectAtIndex:actualIndex];
                                                if ([selectedLanguage isEqualToString:@"English"]) {
                                                    self.language = LanguageNameEnglish;
                                                }else if ([selectedLanguage isEqualToString:@"Turkish"]) {
                                                    self.language = LanguageNameTurkish;
                                                }else{
                                                    self.language = LanguageNameArabic;
                                                }
                                                
                                                if (previousLanguage != self.language){
                                                    [self finishChangingLanguage];
                                                }
                                            }
                                        }];
}

- (void)finishChangingLanguage{
    for (id<LanguageManagerDelegate> aDelegate in self.delegates) {
        if ([aDelegate respondsToSelector:@selector(languageWillChangeTo:isRTL:)]) {
            [aDelegate languageWillChangeTo:self.language isRTL:self.isRTL];
        }
    }
    kSharedNewsConfig.dataSource = kSharedNewsConfig.dataSource;
    for (id<LanguageManagerDelegate> aDelegate in self.delegates) {
        if ([aDelegate respondsToSelector:@selector(languageDidChangedTo:isRTL:)]) {
            [aDelegate languageDidChangedTo:self.language isRTL:self.isRTL];
        }
    }
}

- (UIImage*)logoImage{
    switch (self.language) {
        case LanguageNameArabic:
            return [UIImage imageNamed:@"logo-part-ar"];
            break;
        default:
            return [UIImage imageNamed:@"logo-part-en"];
            break;
    }
    
}

- (NSString *)relatedArticlesText{
    switch (self.language) {
        case LanguageNameEnglish:
            return @"Related Articles";
            break;
        case LanguageNameArabic:
            return @"مقالات ذات صلة";
            break;
        case LanguageNameTurkish:
            return @"İlgili yazılar";
            break;
        default:
            return @"Related Articles";
            break;
    }
}

@end

//
//  UIImage+Scale.h
//  BeLive
//
//  Created by Shashank Patel on 02/11/16.
//  Copyright © 2018 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Scale)

- (UIImage *)scaledToSize:(CGSize)newSize;
- (UIImage*)imageScaledToWidth:(float)i_width;

@end

//
//  LoadingView.m
//  MEO
//
//  Created by Shashank Patel on 04/04/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import "LoadingView.h"

@interface LoadingView () <CAAnimationDelegate>

@property (nonatomic, strong)   SpringImageView     *imageView;
@property (nonatomic, strong)   SpringLabel         *loadingLabel;
@property (nonatomic, strong)   CABasicAnimation    *rotationAnimationX, *rotationAnimationY;
@property (nonatomic, strong)   UIActivityIndicatorView *ac;

@end

@implementation LoadingView

+ (LoadingView*)loadingView{
    LoadingView *lv = [[LoadingView alloc] initWithFrame:UIScreen.mainScreen.bounds];
    return lv;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.imageView = [[SpringImageView alloc] initWithFrame:CGRectMake(0, 0, 250, 250)];
        self.imageView.center = self.center;
//        self.imageView.image = [UIImage imageNamed:@"meo-logo"];
        self.imageView.image = [kSharedNewsConfig.dataSource imageForLoader];
        self.imageView.tintColor = [kSharedNewsConfig.dataSource themeImageTintColor];
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        self.backgroundColor = [kSharedNewsConfig.dataSource navigationBarColor];
        [self addSubview:self.imageView];
        self.imageView.animation = @"flash";
        self.imageView.curve = @"easeIn";
        self.imageView.duration = 1;
        self.imageView.repeatCount = CGFLOAT_MAX;
        [self.imageView animate];
        
        self.loadingLabel = [[SpringLabel alloc] initWithFrame:CGRectMake(0, 0, 250, 50)];
//        [self addSubview:self.loadingLabel];
        
        UIFont *font = [[kSharedNewsConfig.dataSource boldFontForLanguage:LanguageNameEnglish] fontWithSize:40];
        self.loadingLabel.font = font;
        self.loadingLabel.text = @"Loading";
        self.loadingLabel.textColor = UIColor.whiteColor;
        self.loadingLabel.textAlignment = NSTextAlignmentCenter;
        
        self.loadingLabel.animation = @"flash";
        self.loadingLabel.curve = @"easeIn";
        self.loadingLabel.duration = 1;
        self.loadingLabel.repeatCount = CGFLOAT_MAX;
        [self.loadingLabel animate];
        
        self.ac = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [self.ac startAnimating];
        [self addSubview:self.ac];
        
    }
    return self;
}

- (void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    
    self.imageView.center = CGPointMake(self.bounds.size.width / 2, self.bounds.size.height / 2 - 62.5);
    self.loadingLabel.frame = CGRectMake((self.bounds.size.width - 250) / 2, CGRectGetMaxY(self.imageView.frame) + 10, 250, 50);
    self.ac.center = CGPointMake(self.imageView.center.x, CGRectGetMaxY(self.imageView.frame));
}

@end

//
//  BlockQuoteView.h
//  AlArab
//
//  Created by Shashank Patel on 18/07/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


@interface BlockQuoteView : UIView

@property (nonatomic, DT_WEAK_PROPERTY) DTAttributedTextContentView *contentView;
@property (nonatomic, DT_WEAK_PROPERTY) DTTextAttachment            *attachment;

+ (NSDictionary*)captionAttributes;

@end

NS_ASSUME_NONNULL_END

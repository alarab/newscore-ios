//
//  NCImageSource.h
//  MEO
//
//  Created by Shashank Patel on 02/05/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import <Foundation/Foundation.h>

@import ImageSlideshow;

NS_ASSUME_NONNULL_BEGIN

@interface NCImageSource : NSObject <InputSource>

@property (nonatomic, strong)   NSURL               *imageURL;
@property(nonatomic)            UIViewContentMode   contentMode;

- (instancetype)initWithURL:(NSURL*)imageURL;

@end

NS_ASSUME_NONNULL_END

//
//  ViewController.m
//  NewsCore
//
//  Created by Shashank Patel on 24/08/18.
//  Copyright © 2018 Shashank Patel. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

+ (instancetype)controller{
    return ControllerFromStoryBoard([self storyboardID], [[self class] description]);
}

+ (UINavigationController*)navigationController{
    UIViewController *vc = [self controller];
    UINavigationController *navVC = NavigationControllerWithController(vc);
    return navVC;
}

+ (NSString*)storyboardID{
    return @"Main";
}

- (void)awakeFromNib{
    [super awakeFromNib];
    self.isChild = YES;
    self.navigationBarHidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    [self configureNavigationBar];
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:self.navigationBarHidden
                                             animated:YES];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:nil];

}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)configureNavigationBar{
    self.navigationController.navigationBar.tintColor = [kSharedNewsConfig.dataSource navigationTextColor];
    self.navigationController.navigationBar.barTintColor = [kSharedNewsConfig navigationBarColor];
    self.navigationController.navigationBar.translucent = NO;
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[kSharedNewsConfig.dataSource navigationTextColor]}];
    
//    self.navigationController.view.semanticContentAttribute = kSharedLanguageManager.semanticAttribute;
}

- (void)updateUI{
    
}

@end

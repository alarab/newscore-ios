//
//  NavigationTitleView.m
//  MEO
//
//  Created by Shashank Patel on 05/04/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import "NavigationTitleView.h"

@interface NavigationTitleView (){
    UIImageView *titleImageView;
    UIImageView *taglineImageView;
    BOOL shrunk;
}

@end

@implementation NavigationTitleView

+ (NavigationTitleView*)navigationTitleView{
    NavigationTitleView *titleView = [self new];
    return titleView;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        shrunk = NO;
        self.tag = kNavigationTitleViewTag;
        [self loadUIElements];
    }
    return self;
}

- (void)loadUIElements{
    
    titleImageView = [UIImageView new];
    taglineImageView = [UIImageView new];
    
    CGFloat w = 102.4;
    CGFloat h = 32.4;
//    CGFloat containerH = 44 * 2;
    CGFloat containerH = 66;
    CGFloat padding = 10;
    
    titleImageView.frame = CGRectMake(padding, containerH - h - padding, w, h);
    titleImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    taglineImageView.frame = CGRectMake(padding, padding, w, h);
    taglineImageView.image = kSharedLanguageManager.logoImage;
    taglineImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    
    CGFloat titleViewWidth = w + (padding * 2);
    CGFloat x = (SCREEN_WIDTH - titleViewWidth) / 2;
    if(kSharedLanguageManager.isRTL){
        x = 0;
    }else{
        x = SCREEN_WIDTH - titleViewWidth;
    }
    self.frame = CGRectMake(x, -22, titleViewWidth, containerH);
    
//    self.backgroundColor = [kSharedNewsConfig navigationBarColor];
//    [self addBorder:[kSharedNewsConfig borderColor] width:6];
    
    [self addSubview:titleImageView];
    [self addSubview:taglineImageView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToHome)];
    [self addGestureRecognizer:tap];
}

- (void)shrink{
    return;
    if (shrunk) {
        return;
    }
    CGFloat w = 102.4;
    CGFloat h = 32.4;
    CGFloat containerH = 66;
    CGFloat padding = 12;
    
    CGFloat titleViewWidth = w + (padding * 2);
    CGFloat x = (SCREEN_WIDTH - titleViewWidth) / 2;
    
    if(kSharedLanguageManager.isRTL){
        x = 0;
    }else{
        x = SCREEN_WIDTH - titleViewWidth;
    }
    
    
    shrunk = YES;
    
    [UIView animateWithDuration:0.15 animations:^{
        self->taglineImageView.frame = CGRectMake(padding, padding, w, h);
        self->taglineImageView.alpha = 0;
    }];
    
    [UIView animateWithDuration:0.5 animations:^{
        self->titleImageView.frame = CGRectMake(padding, containerH - h - padding, w, h);
        self.frame = CGRectMake(x, -22, titleViewWidth, containerH);
    }];
}

- (void)grow{
    return;
    if (!shrunk) {
        return;
    }
    CGFloat w = 102.4;
    CGFloat h = 32.4;
    CGFloat containerH = 44 * 2;
    CGFloat padding = 12;
    
    CGFloat titleViewWidth = w + (padding * 2);
    CGFloat x = (SCREEN_WIDTH - titleViewWidth) / 2;
    
    if(kSharedLanguageManager.isRTL){
        x = 0;
    }else{
        x = SCREEN_WIDTH - titleViewWidth;
    }
    
    
    shrunk = NO;
    
    [UIView animateWithDuration:0.15
                          delay:0.15
                        options:UIViewAnimationOptionTransitionNone
                     animations:^{
        self->taglineImageView.frame = CGRectMake(padding, padding, w, h);
        self->taglineImageView.alpha = 1;
    } completion:^(BOOL finished) {
        
    }];
    
    [UIView animateWithDuration:0.5 animations:^{
        self->titleImageView.frame = CGRectMake(padding, containerH - h - padding, w, h);
        self.frame = CGRectMake(x, -22, titleViewWidth, containerH);
    }];
}

- (void)setLanguage:(LanguageName)language{
    taglineImageView.image = kSharedLanguageManager.logoImage;
    
    CGFloat w = 102.4;
    CGFloat containerH = 32.4;
    CGFloat padding = 12;
    
    CGFloat titleViewWidth = w + (padding * 2);
    CGFloat x = (SCREEN_WIDTH - titleViewWidth) / 2;
    
    if(kSharedLanguageManager.isRTL){
        x = 0;
    }else{
        x = SCREEN_WIDTH - titleViewWidth;
    }
    self.frame = CGRectMake(x, -22, titleViewWidth, containerH);
}

- (void)goToHome{
    UINavigationController *navVC = (UINavigationController*) ((AppDelegate*)kAppDelegate).drawerController.rootViewController;
    [navVC setViewControllers:@[[HomeViewController controller]] animated:YES];
}

@end

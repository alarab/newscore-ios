//
//  ArticlesController.m
//  MEO
//
//  Created by Shashank Patel on 02/04/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import "ArticlesController.h"

@interface ArticlesController () <UIPageViewControllerDataSource, UIPageViewControllerDelegate>

@property (nonatomic, strong)   UIPageViewController    *pageViewController;
@property (nonatomic, strong)   UIView                  *topBar, *textResizeBar;
@property (nonatomic, strong)   UIButton                *bookmarkButton, *shareButton, *textResizeButton;
@property (nonatomic, strong)   UISlider                *textSizeSlider;

@end

@implementation ArticlesController

- (UIView*)topBar{
    if (!_topBar) {
        CGFloat width = 34;
        CGFloat height = 34;
        CGFloat paddingX = 20;
        CGFloat paddingY = 5;
        NSInteger count = 3;
        
        _topBar = [[UIView alloc] initWithFrame:CGRectMake(0, paddingY, (width * count) + (paddingX * (count + 1)), height)];
        
        NSInteger addedButtonsCount = 0;
        
        CGFloat x = (addedButtonsCount * width) + (paddingX * (addedButtonsCount + 1));
        self.bookmarkButton = [[UIButton alloc] initWithFrame:CGRectMake(x, 0, width, height)];
        [self.bookmarkButton setImage:TemplateImage([UIImage imageNamed:@"bookmark"]) forState:UIControlStateNormal];
        [self.bookmarkButton setImage:TemplateImage([UIImage imageNamed:@"bookmarked"]) forState:UIControlStateSelected];
        self.bookmarkButton.tintColor = [kSharedNewsConfig.dataSource navigationTextColor];
        [self.bookmarkButton addTarget:self action:@selector(bookmarkTapped) forControlEvents:UIControlEventTouchUpInside];
        self.bookmarkButton.layer.cornerRadius = 17;
        self.bookmarkButton.clipsToBounds = YES;
        [self.bookmarkButton addBorder:[kSharedNewsConfig.dataSource navigationTextColor] width:1];
        self.bookmarkButton.selected = self.articles[self.index].isBookmarked;
        [_topBar addSubview:self.bookmarkButton];
        addedButtonsCount++;
        
        x = (addedButtonsCount * width) + (paddingX * (addedButtonsCount + 1));
        self.shareButton = [[UIButton alloc] initWithFrame:CGRectMake(x, 0, width, 34)];
        [self.shareButton setImage:TemplateImage([UIImage imageNamed:@"share"]) forState:UIControlStateNormal];
        self.shareButton.tintColor = [kSharedNewsConfig.dataSource navigationTextColor];
        [self.shareButton addTarget:self action:@selector(shareTapped) forControlEvents:UIControlEventTouchUpInside];
        self.shareButton.layer.cornerRadius = 17;
        self.shareButton.clipsToBounds = YES;
        [self.shareButton addBorder:[kSharedNewsConfig.dataSource navigationTextColor] width:1];
        [_topBar addSubview:self.shareButton];
        addedButtonsCount++;
        
        x = (addedButtonsCount * width) + (paddingX * (addedButtonsCount + 1));
        self.textResizeButton = [[UIButton alloc] initWithFrame:CGRectMake(x, 0, width, height)];
        [self.textResizeButton setImage:TemplateImage([UIImage imageNamed:@"aaIcon"]) forState:UIControlStateNormal];
        self.textResizeButton.tintColor = [kSharedNewsConfig.dataSource navigationTextColor];
        [self.textResizeButton addTarget:self action:@selector(textResizeTapped) forControlEvents:UIControlEventTouchUpInside];
        self.textResizeButton.layer.cornerRadius = 17;
        self.textResizeButton.clipsToBounds = YES;
        [self.textResizeButton addBorder:[kSharedNewsConfig.dataSource navigationTextColor] width:1];
        self.textResizeButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [_topBar addSubview:self.textResizeButton];
        
        _topBar.center = CGPointMake(UIScreen.mainScreen.bounds.size.width / 2, 22);
    }
         
     return _topBar;
}

- (UIView *)textResizeBar{
    if (!_textResizeBar) {
        CGFloat barWidth = VIEW_WIDTH * 0.75;
        CGFloat barX = (VIEW_WIDTH - barWidth) / 2;
        CGFloat width = 50;
        CGFloat height = 50;
        
        CGFloat paddingX = 10;
        CGFloat paddingY = 10;
        
        CGFloat buttonWidth = width - (paddingX * 2);
        CGFloat buttonheight = height - (paddingY * 2);
        
        
        
        CGFloat sliderX = paddingX + width + paddingX;
        CGFloat sliderWidth = barWidth - sliderX - (width + paddingX + paddingX);
        
        _textResizeBar = [[UIView alloc] initWithFrame:CGRectMake(barX, 40, barWidth, height)];
        UIButton *decreaseButton = [[UIButton alloc] initWithFrame:CGRectMake(paddingX, paddingY, buttonWidth, buttonheight)];
        UIImage *minusImage = [[UIImage imageNamed:@"minus"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [decreaseButton setImage:minusImage forState:UIControlStateNormal];
        decreaseButton.imageView.tintColor = [kSharedNewsConfig.dataSource navigationBarColor];
        [decreaseButton addTarget:self
                           action:@selector(decreaseTextTapped)
                 forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *increaseButton = [[UIButton alloc] initWithFrame:CGRectMake(barWidth - buttonWidth - paddingX, paddingY, buttonWidth, buttonheight)];
        UIImage *plusImage = [[UIImage imageNamed:@"plus"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [increaseButton setImage:plusImage forState:UIControlStateNormal];
        increaseButton.imageView.tintColor = [kSharedNewsConfig.dataSource navigationBarColor];
        [increaseButton addTarget:self
                           action:@selector(increaseTextTapped)
                 forControlEvents:UIControlEventTouchUpInside];
        
        self.textSizeSlider = [UISlider new];
        self.textSizeSlider.minimumValue = 12;
        self.textSizeSlider.maximumValue = 100;
        self.textSizeSlider.value = kSharedNewsConfig.articleBodyFontSize;
        self.textSizeSlider.frame = CGRectMake(sliderX, 0, sliderWidth, height);
        [self.textSizeSlider addTarget:self
                                action:@selector(sliderMoved:)
                      forControlEvents:UIControlEventValueChanged];
        [_textResizeBar addSubview:decreaseButton];
        [_textResizeBar addSubview:self.textSizeSlider];
        [_textResizeBar addSubview:increaseButton];
        [_textResizeBar addBorder:[kSharedNewsConfig.dataSource navigationBarColor]
                            width:1];
        [_textResizeBar applyDarkShadow];
        _textResizeBar.backgroundColor = UIColor.whiteColor;
    }
    return _textResizeBar;
}

- (void)increaseTextTapped{
    self.textSizeSlider.value += 5;
    [self sliderMoved:self.textSizeSlider];
}

- (void)decreaseTextTapped{
    self.textSizeSlider.value -= 5;
    [self sliderMoved:self.textSizeSlider];
}

- (void)sliderMoved:(UISlider*)slider{
    kSharedNewsConfig.articleBodyFontSize = slider.value;
    ArticleDetailsController *vc = [self.pageViewController.viewControllers firstObject];
    [vc resizeParagraphs];
}

- (void)bookmarkTapped{
    [self.articles[self.index] bookmark];
    self.bookmarkButton.selected = self.articles[self.index].isBookmarked;
}

- (void)shareTapped{
    NSURL *shareURL = [self.articles[self.index] shareURL];
    
    NSArray *activities = @[shareURL];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activities applicationActivities:nil];
    [self.navigationController presentViewController:activityVC animated:YES completion:nil];
}

- (void)textResizeTapped{
    if (self.textResizeBar.superview) {
        [UIView animateWithDuration:0.25
                         animations:^{
                             self.textResizeBar.alpha = 0;
                         } completion:^(BOOL finished) {
                             [self.textResizeBar removeFromSuperview];
                         }];
    }else{
        [self.view addSubview:self.textResizeBar];
        [UIView animateWithDuration:0.25
                         animations:^{
                             self.textResizeBar.alpha = 1;
                         }];
    }
}


- (void)awakeFromNib{
    [super awakeFromNib];
    self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll
                                                              navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal
                                                                            options:nil];
}

- (id)init{
    if (self = [super init]) {
        self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll
                                                                  navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal
                                                                                options:nil];
    }
    return self;
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.pageViewController.view.frame = self.view.bounds;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar addSubview:self.topBar];
    self.navigationItem.rightBarButtonItem = [self logoBarButtonItem];
    
    self.navigationController.navigationBar.tintColor = [kSharedNewsConfig.dataSource navigationTextColor];
    self.navigationController.navigationBar.barTintColor = [kSharedNewsConfig.dataSource navigationBarColor];
}

- (UIBarButtonItem*)logoBarButtonItem{
    UIButton *logoButton = [[UIButton alloc] initWithFrame:CGRectMake(5, 5, 25, 25)];
    UIImage *logoImage = [NewsConfig.sharedNewsConfig.dataSource imageForLoader];
    [logoButton setImage:TemplateImage(logoImage) forState:UIControlStateNormal];
    logoButton.tintColor = UIColor.whiteColor;
    logoButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    logoButton.clipsToBounds = YES;
    
    UIBarButtonItem *logoBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:logoButton];
    CGFloat ratio = logoImage.size.width / logoImage.size.height;
    NSLayoutConstraint *widthCon = [logoBarButtonItem.customView.widthAnchor constraintEqualToConstant:25 * ratio];
    widthCon.active = YES;
    
    NSLayoutConstraint *heightCon = [logoBarButtonItem.customView.heightAnchor constraintEqualToConstant:25];
    heightCon.active = YES;
        
    return logoBarButtonItem;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.topBar removeFromSuperview];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColor.whiteColor;
    self.pageViewController.dataSource = self;
    self.pageViewController.delegate = self;
    [self.view addSubview:self.pageViewController.view];
    [self addChildViewController:self.pageViewController];
}

- (NSInteger)index{
    ArticleDetailsController *vc = [self.pageViewController.viewControllers firstObject];
    if (!vc) {
        return -1;
    }
    NSInteger index = [self.articles indexOfObject:vc.article];
    return index;
}

- (void)setIndex:(NSInteger)index{
    NSInteger _index = self.index;
    
    if (index < 0 || index >= self.articles.count){
        return;
    }
    
    ArticleDetailsController *vc = [self controllerAt:index];
    [self controllerAt:index - 1];
    [self controllerAt:index + 1];
    
    
    UIPageViewControllerNavigationDirection direction;
    if (_index > index) {
        direction = UIPageViewControllerNavigationDirectionForward;
    }else{
        direction = UIPageViewControllerNavigationDirectionReverse;
    }
    
    self.bookmarkButton.selected = vc.article.isBookmarked;
    
    [self.pageViewController setViewControllers:@[vc]
                                      direction:direction
                                       animated:YES completion:nil];
}

- (ArticleDetailsController*)controllerAt:(NSInteger)index{
    if (index < 0 || index >= self.articles.count){
        return nil;
    }
    
    Article *anArticle = self.articles[index];
    ArticleDetailsController *vc = [ArticleDetailsController controller];
    vc.article = anArticle;
    vc.navigationBarHidden = NO;
    return vc;
}

#pragma mark UIPageViewControllerDataSource
- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController{
    ArticleDetailsController *vc = (ArticleDetailsController*)viewController;
    NSInteger vcIndex = [self.articles indexOfObject:vc.article];
    return [self controllerAt:(vcIndex - 1)];
}

- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController{
    ArticleDetailsController *vc = (ArticleDetailsController*)viewController;
    NSInteger vcIndex = [self.articles indexOfObject:vc.article];
    return [self controllerAt:(vcIndex + 1)];
}

#pragma -

#pragma mark UIPageViewControllerDelegate

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed{
    self.bookmarkButton.selected = self.articles[self.index].isBookmarked;
}

#pragma -

@end

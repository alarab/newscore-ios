//
//  ArticleDetailsController.m
//  MEO
//
//  Created by Shashank Patel on 02/04/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import "ArticleDetailsController.h"
#import <WebKit/WebKit.h>
#import <DTFoundation/DTFoundation-umbrella.h>
#import <SafariServices/SafariServices.h>
#import <Social/Social.h>

@interface ArticleDetailsController () <UIScrollViewDelegate, WKNavigationDelegate, DTAttributedTextContentViewDelegate, DTLazyImageViewDelegate>

@property (nonatomic, strong)   IBOutlet    WKWebView       *webview;
@property (nonatomic, strong)   IBOutlet    UIView          *authorView;
@property (nonatomic, strong)   IBOutlet    UILabel         *authorLabel;
@property (nonatomic, strong)   IBOutlet    UIImageView     *authorImageView;
@property (nonatomic, strong)   IBOutlet    UIImageView     *featuredImageView;
@property (nonatomic, strong)               UIButton        *captionButton;
@property (nonatomic, strong)               UIView          *featuredImageCaptionView;
@property (nonatomic, strong)               UIScrollView    *scrollView;
@property (nonatomic, strong)               NSArray         *textViews;
@property (nonatomic, strong)               NSMutableArray  *captionLabels;
@property (nonatomic)                       BOOL            articleLoaded;

@property (nonatomic, strong)               NewsListController  *relatedController;

@property (nonatomic, strong)               NSMutableDictionary<NSString*, IFrameView*>         *iFrames;
@property (nonatomic, strong)               NSMutableDictionary<NSString*, DTLazyImageView*>    *imageAttachmentViews;

@end

@implementation ArticleDetailsController

+ (void)load{
    [super load];
    [DTAttributedTextContentView setLayerClass:[DTTiledLayerWithoutFade class]];
}

- (void)awakeFromNib{
    [super awakeFromNib];
    self.iFrames = [NSMutableDictionary dictionary];
    self.imageAttachmentViews = [NSMutableDictionary dictionary];
    self.scrollView = [UIScrollView new];
    self.scrollView.frame = [self scrollViewFrame];
    self.scrollView.delegate = self;
    self.scrollView.backgroundColor = UIColor.whiteColor;
    self.scrollView.contentSize = CGSizeZero;
    [DTAttributedTextContentView setLayerClass:[DTTiledLayerWithoutFade class]];
    self.captionButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
    [self.captionButton setImage:[UIImage imageNamed:@"caption_icon"] forState:UIControlStateNormal];
    self.captionButton.tintColor = UIColor.blackColor;
    [self.captionButton addTarget:self
                           action:@selector(toggleCaption)
                 forControlEvents:UIControlEventTouchUpInside];
}

- (void)viewDidLoad{
    [super viewDidLoad];
    [self.view addSubview:self.scrollView];
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.scrollView.frame = [self scrollViewFrame];
    [self resizeParagraphs];
}

- (void)toggleCaption{
    [UIView animateWithDuration:0.15 animations:^{
        self.featuredImageCaptionView.alpha = !self.featuredImageCaptionView.alpha;
    }];
}

- (CGRect)scrollViewFrame{
//    CGRect bounds = kAppDelegate.window.bounds;
    CGRect bounds = self.view.bounds;
    CGFloat paddingX = 0;
    CGFloat paddingY = 0;
    CGFloat width = bounds.size.width - (2 * paddingX);
    CGFloat height = bounds.size.height - (2 * paddingY);
    return CGRectMake(paddingX, paddingY, width, height);
}

- (void)setArticle:(Article *)article{
    _article = article;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (self.article.paragraphs.count) {
        if(!self.articleLoaded){
            [self addParagraphs];
            [self resizeParagraphs];
        }
    }else{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSDate *date = [NSDate date];
        
        [self.article requestArticleDetailsWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
            NSLog(@"Interval: %f", [date timeIntervalSinceNow]);
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (succeeded) {
                if(!self.articleLoaded){
                    [self addParagraphs];
                    [self resizeParagraphs];
                }
            }
        }];
    }
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
//    for (UIView *aView in self.scrollView.subviews) {
//        if (aView.tag == 100) {
//            if ([aView isKindOfClass:[WKWebView class]]) {
//                WKWebView *wv = (WKWebView*)aView;
//                [wv loadHTMLString:@"about:blank" baseURL:nil];
//            }
//            [aView removeFromSuperview];
//        }
//    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if ([keyPath isEqualToString:@"contentSize"]){
        UIScrollView *scrollView = (UIScrollView*)object;
        scrollView.scrollEnabled = NO;
        WKWebView *webView = (WKWebView*)scrollView.superview;
        [webView evaluateJavaScript:@"document.getElementsByTagName(\"body\")[0].clientHeight"
                  completionHandler:^(id scrollHeight, NSError * _Nullable error) {
                      
                      CGFloat width = self.scrollView.frame.size.width - kTotalHorizontalPadding;
                      CGFloat height = [scrollHeight floatValue];
                      if (webView.frame.size.height != height) {
                          webView.frame = CGRectMake(kHorizontalPadding, webView.frame.origin.y, width, height);
                          if ([webView isKindOfClass:[IFrameView class]]) {
                              IFrameView *iWebView = (IFrameView *)webView;
                              if (iWebView.attachment.displaySize.height != height) {
                                  iWebView.attachment.originalSize = webView.frame.size;
                                  iWebView.attachment.displaySize = webView.frame.size;
                                  
                                  iWebView.contentView.layouter = nil;
                                  [iWebView.contentView relayoutText];
                                  
                                  [self scheduleResizeParagraphs];
                              }
                          }else{
                            [self resizeParagraphs];
                          }
                          
                      }
                  }];
    }
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{

}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler{
    if (navigationAction.navigationType == WKNavigationTypeLinkActivated) {
        decisionHandler(WKNavigationActionPolicyCancel);
        NSURL *URL = navigationAction.request.URL;
        SFSafariViewController *vc = [[SFSafariViewController alloc] initWithURL:URL];
        [self.navigationController presentViewController:vc animated:YES completion:nil];
    }else{
        decisionHandler(WKNavigationActionPolicyAllow);
    }
}

- (void)scheduleResizeParagraphs{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(resizeParagraphs) object:nil];
    [self performSelector:@selector(resizeParagraphs) withObject:nil afterDelay:1];
}

- (void)addRelatedArticles{
    CGFloat width = _scrollView.frame.size.width - kTotalHorizontalPadding;
    CGFloat y = self.scrollView.contentSize.height + kVerticalPaddingBetweenViews;
    
    self.relatedController = [NewsListController controller];
    self.relatedController.collectionView.scrollEnabled = NO;
    self.relatedController.articlesSource = _article.relatedArticles;
    [self addChildViewController:self.relatedController];
    
    self.relatedController.view.frame = CGRectMake(0, y, width, 666);
    self.relatedController.view.tag = 175;
    [self.scrollView addSubview:self.relatedController.view];
    self.scrollView.contentSize = CGSizeMake(width, CGRectGetMaxY(self.relatedController.view.frame));
}

- (void)addTags{
    CGFloat width = _scrollView.frame.size.width - kTotalHorizontalPadding;
    CGFloat y = self.scrollView.contentSize.height + kVerticalPaddingBetweenViews;
    UIFont *font = [kSharedNewsConfig.dataSource boldFontForLanguage:kSharedLanguageManager.language];
    TagListView *tagListView = [[TagListView alloc] initWithFrame:CGRectMake(kTotalHorizontalPadding, y, width, 100)];
    [tagListView fixLayout];
    tagListView.tag = 100;
    tagListView.tagBackgroundColor = [kSharedNewsConfig.dataSource tagBackgroundColor];
    tagListView.cornerRadius = 10;
    tagListView.paddingX = 10;
    tagListView.paddingY = 10;
    tagListView.marginY = 5;
    tagListView.marginX = 5;
    tagListView.textFont = font;
    [tagListView makeCircular];
    for (Tag *aTag in self.article.tags) {
        [tagListView addTag:aTag.title];
        
        if(tagListView.tagViews.count >= 3){
            break;
        }
    }
    
    for (TagView *tagView in tagListView.tagViews) {
        if(kSharedLanguageManager.isRTL){
            tagView.transform = CGAffineTransformMakeScale(-1, 1);
        }
        [tagView setOnTap:^(TagView *tv) {
            for (Tag *aTag in self.article.tags) {
                if ([tv.titleLabel.text isEqualToString:aTag.title]) {
                    NewsListController *vc = [NewsListController controller];
                    vc.isTag = YES;
                    vc.taxonomyTitle = aTag.title;
                    vc.taxonomyID = aTag[@"id"];
                    self.navigationController.transitioningStyle = SLNavigationTransitioningStyleNone;
                    [self.navigationController pushViewController:vc animated:YES];
                    break;
                }
            }
            
        }];
    }
    
    if(kSharedLanguageManager.isRTL){
        tagListView.transform = CGAffineTransformMakeScale(-1, 1);
    }
    
    [self.scrollView addSubview:tagListView];
    self.scrollView.contentSize = CGSizeMake(width, CGRectGetMaxY(tagListView.frame));
}

- (void)resizeParagraphs{
    UIFont *bodyFont = kSharedNewsConfig.articleBodyFont;
    UIFont *titleFont = kSharedNewsConfig.articleTitleFont;
    CGFloat width = _scrollView.frame.size.width - kTotalHorizontalPadding;
    
    CGPoint originalOffset = self.scrollView.contentOffset;
    
    self.scrollView.contentSize = CGSizeZero;
    for (UIView *aView in _scrollView.subviews) {
        CGFloat y = self.scrollView.contentSize.height + kVerticalPaddingBetweenViews;
        CGFloat height = aView.frame.size.height;
        
        if ([aView isKindOfClass:[TagListView class]]) {
            TagListView *tlv = (TagListView*)aView;
            height = tlv.tagViewHeight * tlv.rows + (tlv.marginY * (tlv.rows + 1));
        }
        
        if (aView.tag >= 100 && [aView isKindOfClass:[DTAttributedTextContentView class]]) {
            DTAttributedTextContentView *label = (DTAttributedTextContentView*)aView;
            NSMutableAttributedString *attributedString = [label.attributedString mutableCopy];
            
            if (aView.tag == 150) {
                [attributedString addAttribute:NSFontAttributeName value:titleFont range:NSMakeRange(0, attributedString.length)];
            }else if(aView.tag == 155 || aView.tag == 160){
                [attributedString addAttribute:NSFontAttributeName value:kSharedNewsConfig.articleSubTitleFont range:NSMakeRange(0, attributedString.length)];
            }else if(aView.tag == 250){
                [attributedString addAttribute:NSFontAttributeName value:kSharedNewsConfig.articleImageCaptionFont range:NSMakeRange(0, attributedString.length)];
            }else{
                [attributedString enumerateAttribute:NSFontAttributeName inRange:NSMakeRange(0, attributedString.length) options:0 usingBlock:^(id  _Nullable value, NSRange range, BOOL * _Nonnull stop) {
                    UIFont *font = (UIFont*)value;
                    UIFont *newFont = [font fontWithSize:bodyFont.pointSize];
//                    font = [font fontWithSize:bodyFont.pointSize];
                    if (newFont) {
                        [attributedString addAttribute:NSFontAttributeName
                                                 value:newFont
                                                 range:range];
                    }
                }];
            }
            
            label.attributedString = attributedString;
            
            CGFloat maxWidth = _scrollView.frame.size.width - (label.edgeInsets.left + label.edgeInsets.right);
            
            CGRect maxRect = CGRectMake(0, 0, maxWidth, CGFLOAT_HEIGHT_UNKNOWN);
            NSRange entireString = NSMakeRange(0, [label.attributedString length]);
            DTCoreTextLayoutFrame *layoutFrame = [label.layouter layoutFrameWithRect:maxRect range:entireString];
            
            CGSize sizeNeeded = [layoutFrame frame].size;
            height = sizeNeeded.height;
            
        }
        if (aView.tag >= 100) {
            if (aView.tag == 125) {
                if ([aView isKindOfClass:[UIImageView class]]) {
                    UIImageView *iv = (UIImageView*)aView;
                    if (iv.image) {
//                        height = (aView.frame.size.width*iv.image.size.height)/iv.image.size.width;
                        height = (width*iv.image.size.height)/iv.image.size.width;
                    }
                }
//                aView.frame = CGRectMake(aView.frame.origin.x, 0, aView.frame.size.width, height);
                aView.frame = CGRectMake(kHorizontalPadding, y, width, height);
            }else if(aView.tag == 175){
                height = [self.relatedController totalHeight];
                self.relatedController.collectionView.scrollEnabled = false;

//                height = 667;
                aView.frame = CGRectMake(kHorizontalPadding, y, width, height);
            }else if(aView.tag == 160){
                y = self.scrollView.contentSize.height;
                aView.frame = CGRectMake(0, y, _scrollView.frame.size.width, height);
            }else if(aView.tag == 250){
                y = self.scrollView.contentSize.height;
                aView.frame = CGRectMake(aView.frame.origin.x, y, aView.frame.size.width, height);
            }else if([aView isKindOfClass:[WKWebView class]]){
                WKWebView *webView = (WKWebView *)aView;
                aView.frame = CGRectMake(kHorizontalPadding, y, width, webView.frame.size.height);
            }else if([aView isKindOfClass:[ImageSlideshow class]]){
                ImageSlideshow *slideShow = (ImageSlideshow *)aView;
                UILabel *captionLabel = [slideShow viewWithTag:500];
                [captionLabel sizeToFit];
                CGFloat captionHeight = captionLabel.frame.size.height;
                if (captionHeight < 30) {
                    captionHeight = 30;
                }
                CGFloat slideShowWidth = slideShow.frame.size.width;
                CGFloat slideShowHeight = slideShow.frame.size.height;
                captionLabel.frame = CGRectMake(0, slideShowHeight - captionHeight, slideShowWidth, captionHeight);
                captionLabel.alpha = 1;
                aView.frame = CGRectMake(kHorizontalPadding, y, width, height);
            }else if(aView.tag == 130){
                aView.frame = CGRectMake(0, y, width + kTotalHorizontalPadding, height);
            }else if([aView isKindOfClass:[TagListView class]]){
                y += kVerticalPaddingBetweenViews;
                aView.frame = CGRectMake(kHorizontalPadding, y, width, height);
            }else if (aView.tag == 137) {
                aView.frame = CGRectMake(kHorizontalPadding, y, width, height);
            }else if (aView.tag == 187) {
                aView.frame = CGRectMake(0, y, width + kTotalHorizontalPadding, height);
                UIView *quoteStartView = [aView viewWithTag:401];
                UIView *quoteEndView = [aView viewWithTag:402];
                
                if (kSharedLanguageManager.isRTL) {
                    quoteStartView.frame = CGRectMake(width, 0, 25, 25);
                }else{
                    quoteStartView.frame = CGRectMake(kHorizontalPadding, 0, 25, 25);
                    quoteStartView.transform = CGAffineTransformMakeRotation(M_PI);
                }
                
                if (kSharedLanguageManager.isRTL) {
                    quoteEndView.frame = CGRectMake(kHorizontalPadding, height - 25, 25, 25);
                }else{
                    quoteEndView.frame = CGRectMake(width, height - 25, 25, 25);
                    quoteEndView.transform = CGAffineTransformMakeRotation(M_PI);
                }
                
            }else if([aView isKindOfClass:[IFramelyView class]]){
                y +=kVerticalPaddingBetweenViews;
                aView.frame = CGRectMake(kHorizontalPadding, y, width, height);
                [(IFramelyView*) aView calculateFrame];
            }else{
                aView.frame = CGRectMake(0, y, width + kTotalHorizontalPadding, height);
            }
            
            self.scrollView.contentSize = CGSizeMake(width, CGRectGetMaxY(aView.frame));
        }
    }
    
    if (originalOffset.y > 1) {
        CGPoint newOffset = originalOffset;
//        CGFloat newHeighst = self.scrollView.contentSize.height;
//        newOffset.y = newOffset.y + (newHeight - originalHeight);
        self.scrollView.contentOffset = newOffset;
    }
    [self resizeCaptionLabels];
}

- (void)addWriterDetails{
    BOOL isLTR = kSharedLanguageManager.defaultTextAlignment == NSTextAlignmentLeft;
    
    CGFloat width = _scrollView.frame.size.width;
    CGFloat height = self.article.author ? 70 : 50;
    CGFloat y = self.scrollView.contentSize.height + kVerticalPaddingBetweenViews;
    
    UIView *writerView = [[UIView alloc] initWithFrame:CGRectMake(0, y, width, height)];
    writerView.backgroundColor = UIColor.whiteColor;
    
    CGFloat writerWidth = self.article.author ? 50 : 0;
    CGFloat writerHeight = writerWidth;
    CGFloat writerX = width - writerWidth - kHorizontalPadding;
    
    CGFloat writerY = self.article.author ? kVerticalPaddingBetweenViews : kVerticalPaddingBetweenViews;
    if (isLTR) {
        writerWidth = self.article.author ? 50 : 0;
        writerHeight = writerWidth;
        writerX = kHorizontalPadding;
    }
    
    UIImageView *authorImageView = [[UIImageView alloc] initWithFrame:CGRectMake(writerX, writerY, writerWidth, writerHeight)];
    [writerView addSubview:authorImageView];
    
    CGFloat writerLabelMaxX = CGRectGetMinX(authorImageView.frame) - kHorizontalPadding;
    CGFloat writerLabelX = kHorizontalPadding;
    CGFloat writerLabelWidth = writerLabelMaxX - kHorizontalPadding;
    CGFloat writerLabelHeight = writerHeight / 2;
    
    if (isLTR) {
        writerLabelX = CGRectGetMaxX(authorImageView.frame) + kHorizontalPadding;
        writerLabelMaxX = width - kHorizontalPadding;
        writerLabelWidth = writerLabelMaxX - writerLabelX;
        writerLabelHeight = writerHeight / 2;
    }
    
//    writerLabelHeight = writerHeight;
    
    Label *authorLabel = [[Label alloc] initWithFrame:CGRectMake(writerLabelX, kVerticalPaddingBetweenViews + 5, writerLabelWidth, writerLabelHeight - 5)];
    authorLabel.attributedText = self.article.attributedAuthorNamesForDetail;
    [writerView addSubview:authorLabel];
    
    CGFloat dateLabelMaxX = CGRectGetMinX(authorImageView.frame) - kHorizontalPadding;
    CGFloat dateLabelY = CGRectGetMidY(authorImageView.frame);
    CGFloat dateLabelX = kHorizontalPadding;
    CGFloat dateLabelWidth = dateLabelMaxX;
    CGFloat dateLabelHeight =  height - dateLabelY - 5;
    
    if (self.article.author) {
        dateLabelWidth -= kHorizontalPadding;
    }
    
    if (isLTR) {
        dateLabelMaxX = width - kHorizontalPadding;
        dateLabelX = CGRectGetMaxX(authorImageView.frame) + kHorizontalPadding;
        if (!self.article.author) dateLabelX = kHorizontalPadding;
        dateLabelWidth = dateLabelMaxX - dateLabelX;
        dateLabelHeight = self.article.author ? ((writerHeight / 2) - 5) : height;
    }
    
    Label *dateLabel = [[Label alloc] initWithFrame:CGRectMake(dateLabelX, dateLabelY, dateLabelWidth, dateLabelHeight)];
    dateLabel.attributedText = self.article.attributedDateTextForDetails;
    [writerView addSubview:dateLabel];
    
    writerView.tag = 130;
    [self.scrollView addSubview:writerView];
    
    
    [authorImageView sd_setImageWithURL:self.article.author.imageURL
                       placeholderImage:[UIImage imageNamed:@"genericuser"]
                                options:SDWebImageHighPriority | SDWebImageProgressiveLoad];
    authorImageView.layer.cornerRadius = writerWidth / 2;
    authorImageView.clipsToBounds = YES;
    [authorImageView addBorder:UIColor.lightGrayColor width:1];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(writerTapped)];
    [writerView addGestureRecognizer:tap];
    authorImageView.userInteractionEnabled = YES;
    
//    CGFloat socialWidth = 30;
//    CGFloat twitterX = width - socialWidth - kTotalHorizontalPadding;
//
//    if (!isLTR) {
//        twitterX = 0;
//    }
//
//    CGFloat socialY = (height - socialWidth) / 2;
//    UIButton *twitter = [[UIButton alloc]initWithFrame:CGRectMake(twitterX, socialY, socialWidth, socialWidth)];
//    UIImage *icon = [[UIImage imageNamed:@"twitter_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//    [twitter setImage:icon forState:UIControlStateNormal];
//    twitter.tintColor = UIColor.blackColor;
//    twitter.imageView.contentMode = UIViewContentModeScaleAspectFit;
//    twitter.layer.cornerRadius = socialWidth / 2;
//    [twitter addTarget:self action:@selector(twitterTapped) forControlEvents:UIControlEventTouchUpInside];
//    [twitter addBorder];
//    [writerView addSubview:twitter];
//
//    CGFloat facebookX = twitterX -  socialWidth - kHorizontalPadding;
//
//    if (!isLTR) {
//        facebookX = twitterX + socialWidth + kHorizontalPadding;
//    }
//
//    UIButton *facebook = [[UIButton alloc]initWithFrame:CGRectMake(facebookX, socialY, socialWidth, socialWidth)];
//    icon = [[UIImage imageNamed:@"facebook_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//    [facebook setImage:icon forState:UIControlStateNormal];
//    facebook.tintColor = UIColor.blackColor;
//    facebook.imageView.contentMode = UIViewContentModeScaleAspectFit;
//    facebook.layer.cornerRadius = socialWidth / 2;
//    [facebook addTarget:self action:@selector(facebookTapped) forControlEvents:UIControlEventTouchUpInside];
//    [facebook addBorder];
//    [writerView addSubview:facebook];
    
    self.scrollView.contentSize = CGSizeMake(width, CGRectGetMaxY(writerView.frame));
}

- (void)twitterTapped{
//    SLComposeViewController *vc = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
}

- (void)facebookTapped{
    
}


- (void)addFeaturedImage{
    CGFloat width = _scrollView.frame.size.width;
    UIView *featuredImageView = [self viewForFeaturedImage];
    
    
    if ([self.article.type hasPrefix:@"coverage_"]) {
        featuredImageView = [self viewForImageSlides:self.article[@"slideshow"]];
        self.featuredImageView = featuredImageView;
    }
    featuredImageView.tag = 125;
    featuredImageView.frame = CGRectMake(0, 0, width, width * 9 / 16);
    
    [self.scrollView addSubview:featuredImageView];
    self.scrollView.contentSize = CGSizeMake(width, CGRectGetMaxY(featuredImageView.frame));
    
    if (self.article.featuredImageCaption.length) {
        self.featuredImageCaptionView = [self addImageCaptionWithString:self.article.featuredImageCaption
                                                                 toView:featuredImageView
                                                                padding:10 + 32];
        self.featuredImageCaptionView.alpha = 0;
        [featuredImageView addSubview:self.captionButton];
    }
}

- (void)resizeCaptionLabels{
    for (UIView *aCaptionView in self.captionLabels) {
        CGFloat height = aCaptionView.superview.frame.size.height;
        CGFloat width = aCaptionView.superview.frame.size.width;
        
        UILabel *aCaptionLabel = [aCaptionView viewWithTag:500];
        [aCaptionLabel sizeToFit];
        CGFloat captionHeight = aCaptionLabel.frame.size.height;
        if (captionHeight < 30) {
            captionHeight = 30;
        }
        CGFloat padding = aCaptionLabel.frame.origin.x;
        aCaptionLabel.frame = CGRectMake(padding, 5, width - (padding * 2), captionHeight);
        aCaptionView.frame = CGRectMake(0, height - (captionHeight + 10), width, captionHeight + 10);
    }
    
    UIView *captionContainer = [self.captionButton superview];
    CGFloat width = captionContainer.frame.size.width;
    CGFloat height = captionContainer.frame.size.height;
    self.captionButton.frame = CGRectMake(width - 32 - 5, height - 32 - 5, 32, 32);
}

- (UIView*)addImageCaptionWithString:(NSString*)string
                              toView:(UIView*)toView
                             padding:(CGFloat)padding{
    CGFloat width = toView.frame.size.width;
    CGFloat height = toView.frame.size.height;
    UILabel *captionLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,
                                                                      width , height)];
    captionLabel.font = [[kSharedNewsConfig fontForLanguage:kSharedLanguageManager.language] fontWithSize:14];
    captionLabel.textColor = UIColor.whiteColor;
    captionLabel.textAlignment = kSharedLanguageManager.defaultTextAlignment;
    captionLabel.numberOfLines = 0;
    captionLabel.backgroundColor = [UIColor colorWithWhite:0 alpha:0.0];
    captionLabel.tag = 500;
    
    captionLabel.text = string;
    [captionLabel sizeToFit];
    CGFloat captionHeight = captionLabel.frame.size.height;
    if (captionHeight < 30) {
        captionHeight = 30;
    }
    captionLabel.frame = CGRectMake(padding, 5, width - (padding * 2), captionHeight + 10);
    UIView *captionView = [UIView new];
    captionView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    [captionView addSubview:captionLabel];
    
    if (!self.captionLabels) {
        self.captionLabels = [NSMutableArray array];
    }
    
    [self.captionLabels addObject:captionView];
    
    [toView addSubview:captionView];
    
    return captionView;
}

- (void)addImageCaptionWithString:(NSString*)string
                           toView:(UIView*)toView{
    
    [self addImageCaptionWithString:string toView:toView padding:10];
    
//    CGFloat width = _scrollView.frame.size.width - (padding * 2);
//
//    DTAttributedTextContentView *label = [DTAttributedTextContentView new];
//    label.edgeInsets = UIEdgeInsetsMake(0, kHorizontalPadding, 0, kHorizontalPadding);
//    label.shouldDrawLinks = YES;
//    label.shouldDrawImages = YES;
//    label.backgroundColor = UIColor.whiteColor;
//    label.attributedString = attributedString;
//    label.edgeInsets = UIEdgeInsetsMake(0, kHorizontalPadding, 0, kHorizontalPadding);
//    label.tag = 250;
//    label.backgroundColor = [UIColor colorWithHex:0xefefef];
//    label.frame = CGRectMake(padding, 0, width, 100);
//
//    [self.scrollView addSubview:label];
//    self.scrollView.contentSize = CGSizeMake(width, CGRectGetMaxY(label.frame));
}

- (void)addArticleTitle{
    CGFloat width = _scrollView.frame.size.width - kTotalHorizontalPadding;
    
    DTAttributedTextContentView *label = [DTAttributedTextContentView new];
    label.edgeInsets = UIEdgeInsetsMake(0, kHorizontalPadding, 0, kHorizontalPadding);
    label.shouldDrawLinks = YES;
    label.shouldDrawImages = YES;
    label.backgroundColor = UIColor.whiteColor;
    label.attributedString = self.article.attributedTitleForDetail;
//    label.numberOfLines = 0;
    
    label.tag = 150;
    
    label.frame = CGRectMake(0, 0, width, 100);
    
    [self.scrollView addSubview:label];
    self.scrollView.contentSize = CGSizeMake(width, CGRectGetMaxY(label.frame));
}

- (void)addArticleSubTitle{
    CGFloat width = _scrollView.frame.size.width - kTotalHorizontalPadding;
    
    DTAttributedTextContentView *label = [DTAttributedTextContentView new];
    label.edgeInsets = UIEdgeInsetsMake(0, kHorizontalPadding, 0, kHorizontalPadding);
    label.shouldDrawLinks = YES;
    label.shouldDrawImages = YES;
    label.backgroundColor = UIColor.whiteColor;
    label.attributedString = self.article.attributedSubTitleForDetail;
    label.tag = 155;
    label.frame = CGRectMake(0, 0, width, 100);
    [self.scrollView addSubview:label];
    self.scrollView.contentSize = CGSizeMake(width, CGRectGetMaxY(label.frame));
}

- (void)addParagraphs{
    self.articleLoaded = YES;
    self.scrollView.contentSize = CGSizeZero;
    
    
    [self addArticleTitle];
    [self addArticleSubTitle];
    [self addWriterDetails];
    
    [self addFeaturedImage];
    
    CGFloat width = _scrollView.frame.size.width;
    NSArray *paragraphs = self.article.paragraphs;
    if (![paragraphs isKindOfClass:[NSArray class]]) {
        return;
    }
    
    for (NSDictionary *aParagraph in self.article.paragraphs) {
        
        NSString *type = aParagraph[@"type"];
        UIView *aView;
        
        if ([type isEqualToString:@"text"]) {
            aView = [self viewForTextParagraph:aParagraph];
        }else if ([type isEqualToString:@"video"]){
            aView = [self viewForVideoParagraph:aParagraph];
        }else if ([type isEqualToString:@"facebook"] || [type isEqualToString:@"facebook_text"]){
            aView = [self viewForFacebookParagraph:aParagraph];
        }else if ([type isEqualToString:@"instagram"]){
            aView = [self viewForInstagramParagraph:aParagraph];
        }else if ([type isEqualToString:@"quote"]){
            aView = [self viewForQuoteParagraph:aParagraph];
        }else if ([type isEqualToString:@"twitter"]){
            aView = [self viewForTwitterParagraph:aParagraph];
        }else if ([type isEqualToString:@"slideshow"]){
            aView = [self viewForSlideShowParagraph:aParagraph];
        }else if ([type isEqualToString:@"image"]){
            aView = [self viewForImageParagraph:aParagraph];
        }else if ([type isEqualToString:@"iframly"]){
            aView = [self viewForIFramelyParagraph:aParagraph];
        }else{
            NSLog(@"Type: %@", type);
        }
        
        if (aView) {
            if(aView.tag == 0){
                aView.tag = 100;
            }
            [self.scrollView addSubview:aView];
            self.scrollView.contentSize = CGSizeMake(width, CGRectGetMaxY(aView.frame));
        }
    }
    
    [self addOpinionDisclaimer];
    
    [self addTags];
    [self addRelatedArticles];
}

- (void)addOpinionDisclaimer{
    return;
    if ([self.article.type isEqualToString:@"opinion"]) {
        CGFloat width = _scrollView.frame.size.width;
        NSDictionary *aParagraph = @{@"type" : @"text",
                                     @"text" : self.article[@"opinion_message"]};
        
        UIView *aView = [self viewForTextParagraph:aParagraph];
        aView.backgroundColor = UIColor.lightGrayColor;
        [self.scrollView addSubview:aView];
        self.scrollView.contentSize = CGSizeMake(width, CGRectGetMaxY(aView.frame));
    }
}

- (UIView*)viewForFeaturedImage{
    CGFloat ratio = 1.629213483;
    CGFloat width = _scrollView.frame.size.width;
    CGFloat height = width / ratio
    ;
    CGFloat y = self.scrollView.contentSize.height;
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(kHorizontalPadding, y, width, height)];
    [imageView sd_setImageWithURL:self.article.featuredImageURL
                 placeholderImage:nil
                          options:SDWebImageProgressiveLoad
                        completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                            [self resizeParagraphs];
                        }];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.backgroundColor = [kSharedNewsConfig navigationBarColor];
    imageView.frame = CGRectMake(0, y, width, height);
    imageView.userInteractionEnabled = YES;
    UIGestureRecognizer *g = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)];
    [imageView addGestureRecognizer:g];
    
    self.featuredImageView = imageView;
    
    return imageView;
}

- (UIView*)viewForImageParagraph:(NSDictionary*)aParagraph{
    CGFloat width = _scrollView.frame.size.width - kTotalHorizontalPadding;
    CGFloat height = width * 9 / 16;
    CGFloat y = self.scrollView.contentSize.height + kVerticalPaddingBetweenViews;
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(kHorizontalPadding, y, width, height)];
    [imageView sd_setImageWithURL:aParagraph[@"url"]
                 placeholderImage:nil
                          options:SDWebImageProgressiveLoad];
    imageView.tag = 137;
    NSString *caption = aParagraph[@"caption"];
    if (caption && ![caption isKindOfClass:[NSNull class]]) {
        [self addImageCaptionWithString:caption
                                 toView:imageView];
    }
    
    return imageView;
}

- (UIView*)viewForIFramelyParagraph:(NSDictionary*)aParagraph{
    CGFloat width = _scrollView.frame.size.width - kTotalHorizontalPadding;
    CGFloat height = width * 9 / 16;
    CGFloat y = self.scrollView.contentSize.height + kVerticalPaddingBetweenViews;
    
    IFramelyView *iFramelyView = [IFramelyView iFramelyView];
    iFramelyView.frame = CGRectMake(kHorizontalPadding, y, width, height);
    iFramelyView.data = aParagraph;
    [iFramelyView calculateFrame];
    
    return iFramelyView;
}

- (UIView*)viewForSlideShowParagraph:(NSDictionary*)aParagraph{
    __block NSArray<NSDictionary*> *imageSlides = aParagraph[@"slides"];
    
    if (!imageSlides.count) {
        imageSlides = aParagraph[@"slideshow"];
    }
    return [self viewForImageSlides:imageSlides];
}

- (UIView*)viewForImageSlides:(NSArray<NSDictionary*>*)imageSlides{
    CGFloat width = _scrollView.frame.size.width;
    CGFloat height = width * 9 / 16;
    CGFloat y = self.scrollView.contentSize.height + kVerticalPaddingBetweenViews;
    
    NSMutableArray *imageInputs = [NSMutableArray array];
    for (NSDictionary *aSlide in imageSlides) {
        NSString *imageURLString = aSlide[@"url"];
        NCImageSource *aSource = [[NCImageSource alloc] initWithURL:[NSURL URLWithString:imageURLString]];
        [imageInputs addObject:aSource];
    }
    
    ImageSlideshow *slideshow = [[ImageSlideshow alloc] initWithFrame:CGRectMake(kHorizontalPadding, y, width, height)];
    __block UILabel *captionLabel = [[UILabel alloc] initWithFrame:CGRectMake(kHorizontalPadding, kHorizontalPadding,
                                                                      width - kTotalHorizontalPadding, height - kHorizontalPadding)];
    NSString *caption = imageSlides[0][@"caption"];
    captionLabel.font = [[kSharedNewsConfig fontForLanguage:kSharedLanguageManager.language] fontWithSize:14];
    captionLabel.textColor = UIColor.whiteColor;
    captionLabel.textAlignment = kSharedLanguageManager.defaultTextAlignment;
    captionLabel.numberOfLines = 0;
    captionLabel.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    captionLabel.tag = 500;
    
    [self setCaptionFrameFor:captionLabel
                 fromCaption:caption
                   slideshow:slideshow];
    
    [slideshow addSubview:captionLabel];
    slideshow.slideshowInterval = 5;
    __weak typeof(slideshow) weakSlideshow = slideshow;
    slideshow.currentPageChanged = ^(NSInteger page) {
        NSString *caption = imageSlides[page][@"caption"];
        [self setCaptionFrameFor:captionLabel
                     fromCaption:caption
                       slideshow:weakSlideshow];
    };
    slideshow.circular = YES;
    [slideshow applyDarkShadow];
    [slideshow setImageInputs:imageInputs];
    UIGestureRecognizer *g = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideShowTapped:)];
    [slideshow addGestureRecognizer:g];
    return slideshow;
}

- (void)setCaptionFrameFor:(UILabel*)captionLabel
               fromCaption:(NSString*)caption
                 slideshow:(ImageSlideshow*)slideshow{
    if (![caption  isKindOfClass:[NSNull class]]) {
        captionLabel.text = [NSString stringWithFormat:@"  %@",caption];
        [captionLabel sizeToFit];
        CGFloat captionHeight = captionLabel.frame.size.height;
        if (captionHeight < 30) {
            captionHeight = 30;
        }
        CGFloat slideShowWidth = slideshow.frame.size.width;
        CGFloat slideShowHeight = slideshow.frame.size.height;
        captionLabel.frame = CGRectMake(0, slideShowHeight - captionHeight, slideShowWidth, captionHeight);
        captionLabel.alpha = 1;
    }else{
        captionLabel.text = nil;
        captionLabel.alpha = 0;
    }
}

- (UIView*)viewForVideoParagraph:(NSDictionary*)aParagraph{
    YTPlayerView *playerView = [YTPlayerView new];
    [playerView loadWithVideoId:aParagraph[@"video_id"]];
//    [playerView loadWithVideoId:@"fE50xrnJnR8"];
    
    
    CGFloat width = _scrollView.frame.size.width - kTotalHorizontalPadding;
    
    CGFloat height = width * 9 / 16;
    CGFloat y = self.scrollView.contentSize.height + kVerticalPaddingBetweenViews;
    
    playerView.frame = CGRectMake(kHorizontalPadding, y, width, height);
//    playerView.layer.cornerRadius = 10;
    playerView.clipsToBounds = YES;
//    [playerView applyDarkShadow];
    
    return playerView;
}

- (NSString*)htmlContainerForString:(NSString*)string{
    return [self htmlContainerForString:string fixHeight:NO];
}

- (NSString*)htmlContainerForString:(NSString*)string fixHeight:(BOOL)fixHeight{
    CGFloat width = _scrollView.frame.size.width - kTotalHorizontalPadding;
    
    NSString *RTLParameter = @"";
    if (kSharedLanguageManager.isRTL) {
        RTLParameter = @"dir=\"rtl\"";
    }
    
    NSString *viewport = @"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">";
    NSString *htmlBase = @"<!DOCTYPE html><html %@><head>%@<meta http-equiv=\"Content-Security-Policy\" content=\"policy-definition\"></head><body style=\"margin: 0;text-align: center;\">%@</body></html>";
    
//    NSString *htmlBase = @"<!DOCTYPE html><html %@><head>%@<meta http-equiv=\"Content-Security-Policy\" content=\"policy-definition\"></head><body>%@</body></html>";
    
    NSString *html = [NSString stringWithFormat:htmlBase, RTLParameter, viewport, string];
    
    NSError *error = NULL;
    
    NSString *pattern1 = @"width=[0-9]+";
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern1
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    NSArray<NSTextCheckingResult*>* matches = [regex matchesInString:html options:0 range:NSMakeRange(0, html.length)];
    NSString *widthString1 = [NSString stringWithFormat:@"width=%ld", (NSInteger)width];
    for (NSTextCheckingResult *aResult in matches) {
        html = [html stringByReplacingCharactersInRange:aResult.range
                                             withString:widthString1];
    }
    
    NSString *pattern2 = @"width=\"[0-9]+\"";
    regex = [NSRegularExpression regularExpressionWithPattern:pattern2
                                                      options:NSRegularExpressionCaseInsensitive
                                                        error:&error];
    matches = [regex matchesInString:html options:0 range:NSMakeRange(0, html.length)];
    NSString *widthString2 = [NSString stringWithFormat:@"width=\"%ld\"", (NSInteger)width];
    for (NSTextCheckingResult *aResult in matches) {
        html = [html stringByReplacingCharactersInRange:aResult.range
                                             withString:widthString2];
    }
    
    if (fixHeight) {
        NSString *pattern3 = @"height=[0-9]+";
        NSString *heightString1 = @"height=auto";
        regex = [NSRegularExpression regularExpressionWithPattern:pattern3
                                                          options:NSRegularExpressionCaseInsensitive
                                                            error:&error];
        NSArray<NSTextCheckingResult*>* matches = [regex matchesInString:html options:0 range:NSMakeRange(0, html.length)];
        for (NSTextCheckingResult *aResult in matches) {
            html = [html stringByReplacingCharactersInRange:aResult.range
                                                 withString:heightString1];
        }
        
        NSString *pattern4 = @"height=\"[0-9]+\"";
        NSString *heightString2 = @"height=\"auto\"";
        regex = [NSRegularExpression regularExpressionWithPattern:pattern4
                                                          options:NSRegularExpressionCaseInsensitive
                                                            error:&error];
        matches = [regex matchesInString:html options:0 range:NSMakeRange(0, html.length)];
        
        for (NSTextCheckingResult *aResult in matches) {
            html = [html stringByReplacingCharactersInRange:aResult.range
                                                 withString:heightString2];
        }
    }
    
    if (NO) {
        CGFloat height = width *  9/ 16;
        NSString *pattern3 = @"height=[0-9]+";
        regex = [NSRegularExpression regularExpressionWithPattern:pattern3
                                                                               options:NSRegularExpressionCaseInsensitive
                                                                                 error:&error];
        NSArray<NSTextCheckingResult*>* matches = [regex matchesInString:html options:0 range:NSMakeRange(0, html.length)];
        NSString *widthString1 = [NSString stringWithFormat:@"height=%ld", (NSInteger)height];
        for (NSTextCheckingResult *aResult in matches) {
            html = [html stringByReplacingCharactersInRange:aResult.range
                                                 withString:widthString1];
        }
        
        NSString *pattern4 = @"height=\"[0-9]+\"";
        regex = [NSRegularExpression regularExpressionWithPattern:pattern4
                                                          options:NSRegularExpressionCaseInsensitive
                                                            error:&error];
        matches = [regex matchesInString:html options:0 range:NSMakeRange(0, html.length)];
        NSString *widthString2 = [NSString stringWithFormat:@"height=\"%ld\"", (NSInteger)height];
        for (NSTextCheckingResult *aResult in matches) {
            html = [html stringByReplacingCharactersInRange:aResult.range
                                                 withString:widthString2];
        }
    }
    
    return html;
}

- (UIView*)viewForQuoteParagraph:(NSDictionary*)aParagraph{
    NSString *paragraphText = aParagraph[@"url"];
    
    NSDictionary *builderOptions = @{
                                     DTDefaultFontFamily: kSharedNewsConfig.articleBodyFont.familyName,
                                     DTDefaultFontName: kSharedNewsConfig.articleBodyFont.fontName,
                                     DTDefaultFontSize : @(kSharedNewsConfig.articleBodyFontSize),
                                     DTDefaultTextColor: [UIColor colorWithHex:0x555555]
                                     };
    paragraphText = [NSString stringWithFormat:@"<blockquote>%@</blockquote>", paragraphText];
    paragraphText = [self htmlContainerForString:paragraphText];
    
    NSData *htmlData = [paragraphText dataUsingEncoding:NSUTF8StringEncoding];
    DTHTMLAttributedStringBuilder *stringBuilder = [[DTHTMLAttributedStringBuilder alloc] initWithHTML:htmlData
                                                                                               options:builderOptions
                                                                                    documentAttributes:nil];
    DTAttributedTextContentView *label = [DTAttributedTextContentView new];
    label.edgeInsets = UIEdgeInsetsMake(0, kHorizontalPadding * 2, 0, kHorizontalPadding * 2);
    label.shouldDrawLinks = YES;
    label.shouldDrawImages = YES;
    label.tag = 187;
    
    NSMutableAttributedString *quoteString = [[stringBuilder generatedAttributedString] mutableCopy];
    
    
    UIImage *quoteStart = [[UIImage imageNamed:@"quote_start"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIImage *quoteEnd = [[UIImage imageNamed:@"quote_end"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    UIImageView *quoteStartView = [[UIImageView alloc] initWithImage:quoteStart];
    quoteStartView.tintColor = [kSharedNewsConfig borderColor];
    quoteStartView.tag = 401;
    quoteStartView.contentMode = UIViewContentModeScaleAspectFit;
    
    UIImageView *quoteEndView = [[UIImageView alloc] initWithImage:quoteEnd];
    quoteEndView.tintColor = [kSharedNewsConfig borderColor];
    quoteEndView.tag = 402;
    quoteEndView.contentMode = UIViewContentModeScaleAspectFit;
    label.backgroundColor = [UIColor colorWithWhite:0.98 alpha:1];
    
    label.attributedString = quoteString;
    [label addSubview:quoteStartView];
    [label addSubview:quoteEndView];
    return label;
}

- (UIView*)viewForTwitterParagraph:(NSDictionary*)aParagraph{
    CGFloat width = _scrollView.frame.size.width - kTotalHorizontalPadding;

    WKWebView *webView = [WKWebView new];
    webView.navigationDelegate = self;
    webView.tag = 205;
    [webView.scrollView addObserver:self
                         forKeyPath:@"contentSize"
                            options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld | NSKeyValueObservingOptionPrior
                            context:NULL];
    
    NSString *tweetURL = aParagraph[@"url"];
    NSString *twitterHTML = [NSString stringWithFormat:@"<br><p><script src=\"https://platform.twitter.com/widgets.js\"></script>   <blockquote class=\"twitter-tweet\"> <a href=\"%@\"></a></blockquote></p>", tweetURL];
    NSString *html = [self htmlContainerForString:twitterHTML];
    [webView loadHTMLString:html baseURL:[NSURL URLWithString:@"https://www.twitter.com"]];
    
    CGFloat height = width * 1.4;
    CGFloat y = self.scrollView.contentSize.height + kVerticalPaddingBetweenViews;
    
    webView.frame = CGRectMake(kHorizontalPadding, y, width, height);
    return webView;
}

- (UIView*)viewForInstagramParagraph:(NSDictionary*)aParagraph{
    CGFloat width = _scrollView.frame.size.width - kTotalHorizontalPadding;
    
    WKWebView *webView = [WKWebView new];
    webView.navigationDelegate = self;
    webView.tag = 205;
    NSString *instagramDetails = aParagraph[@"data"];
    if (instagramDetails.length) {
        NSString *html = [self htmlContainerForString:instagramDetails];
        [webView loadHTMLString:html baseURL:[NSURL URLWithString:@"https://www.instagram.com"]];
    }else{
        NSString *instagramURL = aParagraph[@"url"];
        if (![instagramURL hasSuffix:@"embed"]) {
            instagramURL = [instagramURL stringByAppendingPathComponent:@"embed"];
        }
        [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:instagramURL]]];
    }
    
    CGFloat height = width * 1.4;
    CGFloat y = self.scrollView.contentSize.height + kVerticalPaddingBetweenViews;
    
    webView.frame = CGRectMake(kHorizontalPadding, y, width, height);
    return webView;
}

- (UIView*)viewForFacebookParagraph:(NSDictionary*)aParagraph{
    CGFloat width = _scrollView.frame.size.width - kTotalHorizontalPadding;
    
    WKWebView *webView = [WKWebView new];
    webView.navigationDelegate = self;
    webView.tag = 205;
    NSString *facebookDetails = aParagraph[@"url"];
    facebookDetails = [self htmlContainerForString:facebookDetails fixHeight:YES];
    
    [webView loadHTMLString:facebookDetails baseURL:[NSURL URLWithString:@"https://www.facebook.com"]];
    [webView.scrollView addObserver:self
                         forKeyPath:@"contentSize"
                            options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld | NSKeyValueObservingOptionPrior
                            context:NULL];
//
//    NSString *patternPath = [[NSBundle mainBundle] pathForResource:@"url_regex" ofType:@"txt"];
//    NSString *pattern = [[NSString alloc]initWithContentsOfFile:patternPath
//                                                       encoding:NSUTF8StringEncoding
//                                                          error:nil];
//    NSArray *patternLines = [pattern componentsSeparatedByCharactersInSet:NSCharacterSet.newlineCharacterSet];
//    NSError *error;
//    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:patternLines[0]
//                                                                           options:NSRegularExpressionCaseInsensitive
//                                                                             error:&error];
//    NSArray<NSTextCheckingResult*>* matches = [regex matchesInString:facebookDetails
//                                                             options:0
//                                                               range:NSMakeRange(0, facebookDetails.length)];
//    NSURL *URL;
//    for (NSTextCheckingResult *aResult in matches) {
//        NSString *URLString = [facebookDetails substringWithRange:aResult.range];
//        URLString = [URLString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//        if ([URLString hasSuffix:@"\""]) {
//            URLString = [URLString stringByReplacingCharactersInRange:NSMakeRange(URLString.length - 1, 1) withString:@""];
//        }
//        NSLog(@"Facebook URL:%@", URLString);
//        URL = [[NSURL alloc] initWithString:URLString];
//        if(URL)break;
//    }
//    [webView loadRequest:[NSURLRequest requestWithURL:URL]];
    
    CGFloat height = width * 9 / 16;
    CGFloat y = self.scrollView.contentSize.height + kVerticalPaddingBetweenViews;
    
    webView.frame = CGRectMake(kHorizontalPadding, y, width, height);
    return webView;
}

- (UIView*)viewForTextParagraph:(NSDictionary*)aParagraph{
    [DTTextAttachment registerClass:[BlockQuoteAttachment class] forTagName:@"quote"];
    [DTTextAttachment registerClass:[FigCaptionAttachment class] forTagName:@"figcaption"];
    
    NSString *paragraphText = aParagraph[@"text"];
    if (kSharedLanguageManager.isRTL) {
//        paragraphText = [NSString stringWithFormat:@"<html dir=\"rtl\">%@", paragraphText];
        paragraphText = [paragraphText stringByReplacingOccurrencesOfString:@"<html>" withString:@"<html dir=\"rtl\">"];
    }else{
        paragraphText = [paragraphText stringByReplacingOccurrencesOfString:@"<html dir=\"rtl\">" withString:@"<html dir=\"ltr\">"];
        paragraphText = [paragraphText stringByReplacingOccurrencesOfString:@"<html>" withString:@"<html dir=\"ltr\">"];
    }
    paragraphText = [paragraphText stringByReplacingOccurrencesOfString:@"<?xml encoding=\"UTF-8\">" withString:@""];
    paragraphText = [self processHTML:paragraphText];
    if (TARGET_OS_SIMULATOR && [paragraphText containsString:@"blockquote"]) {
        paragraphText = [paragraphText stringByReplacingOccurrencesOfString:@"<blockquote>\r\n<p>"
                                                                 withString:@"<figure><quote>"];
        paragraphText = [paragraphText stringByReplacingOccurrencesOfString:@"</p>\r\n</blockquote>"
                                                                 withString:@"</quote></figure>"];
        [paragraphText writeToFile:@"/Users/shashankpatel/Desktop/blockquote.html"
                        atomically:YES
                          encoding:NSUTF8StringEncoding
                             error:nil];
    }
    
    if (TARGET_OS_SIMULATOR && [paragraphText containsString:@"figcaption"]) {
        [paragraphText writeToFile:@"/Users/shashankpatel/Desktop/figcaption.html"
                        atomically:YES
                          encoding:NSUTF8StringEncoding
                             error:nil];
    }
    
    NSDictionary *builderOptions = @{
                                     DTDefaultFontFamily: kSharedNewsConfig.articleTitleFont.familyName,
//                                     DTDefaultFontName: kSharedNewsConfig.articleBodyFont.fontName,
                                     DTDefaultFontSize : @(kSharedNewsConfig.articleBodyFontSize),
                                     DTDefaultTextColor: [UIColor colorWithHex:0x555555],
                                     DTMaxImageSize: [NSValue valueWithCGSize:CGSizeMake(280, 450)]
//                                     DTDefaultTextAlignment : @(kSharedLanguageManager.defaultTextAlignment)
                                     };
//    paragraphText = [paragraphText stringByReplacingOccurrencesOfString:@"dir=\"rtl\"" withString:@""];
    NSData *htmlData = [paragraphText dataUsingEncoding:NSUTF8StringEncoding];
    DTHTMLAttributedStringBuilder *stringBuilder = [[DTHTMLAttributedStringBuilder alloc] initWithHTML:htmlData
                                                                                               options:builderOptions
                                                                                    documentAttributes:nil];
    DTAttributedTextContentView *label = [DTAttributedTextContentView new];
    label.edgeInsets = UIEdgeInsetsMake(0, kHorizontalPadding, 0, kHorizontalPadding);
    label.shouldDrawLinks = YES;
    label.shouldDrawImages = YES;
    label.backgroundColor = UIColor.whiteColor;
    label.delegate = self;
    label.attributedString = [stringBuilder generatedAttributedString];
    
    
    return label;
}

- (NSString*)processHTML:(NSString*)html{
    NSString *linkPatternStart = @"<a href=\"#\" onclick=\"window.open('";
    NSString *linkPatternEnd = @"','_system','location=yes')";
    
    NSString *htmlToParse = html;
    NSRange rangeStart = [htmlToParse rangeOfString:linkPatternStart];
    NSRange rangeEnd = [htmlToParse rangeOfString:linkPatternEnd];
    
    while (rangeStart.location != NSNotFound && rangeEnd.location != NSNotFound) {
        NSInteger startLocation = rangeStart.location + rangeStart.length;
        NSInteger endLocation = rangeEnd.location - startLocation;
        NSString *linkURL = [[htmlToParse substringFromIndex:startLocation] substringToIndex:endLocation];
        NSString *fixedLinkString = [NSString stringWithFormat:@"<a href=\"%@\" onclick=\"window.open('", linkURL];
        htmlToParse = [htmlToParse stringByReplacingOccurrencesOfString:linkPatternStart withString:fixedLinkString];
        rangeStart = [htmlToParse rangeOfString:linkPatternStart];
        rangeEnd = [htmlToParse rangeOfString:linkPatternEnd];
    }
    
    htmlToParse = [self processForTwitter:htmlToParse];
    
    return htmlToParse;
}

- (NSString*)processForTwitter:(NSString*)html{
    NSString *linkPatternStart = @"<blockquote class=\"twitter-tweet\">";
    NSString *linkPatternEnd = @"</blockquote>";
    
    NSString *htmlToParse = html;
    NSRange rangeStart, rangeEnd;
    NSInteger rangeEndLocation, rangeEndLength;
    
    rangeStart = [htmlToParse rangeOfString:linkPatternStart];
    rangeEndLocation = rangeStart.location + rangeStart.length;
    rangeEndLength = htmlToParse.length - rangeEndLocation;
    
    if (rangeStart.location != NSNotFound) {
       rangeEnd = [htmlToParse rangeOfString:linkPatternEnd options:NSCaseInsensitiveSearch range:NSMakeRange(rangeEndLocation, rangeEndLength)];
    }
    
    
    while (rangeStart.location != NSNotFound &&
           rangeEnd.location != NSNotFound &&
           rangeEnd.location > (rangeStart.location + rangeStart.length)) {
        NSInteger startLocation = rangeStart.location;
        NSInteger endLocation = rangeEnd.location - rangeStart.location + rangeEnd.length;
        
        NSString *twitterBlockQuote = [[htmlToParse substringFromIndex:startLocation] substringToIndex:endLocation];
        NSString *updatedTwitterBlockQuote = [twitterBlockQuote stringByReplacingOccurrencesOfString:@"\"" withString:@"'"];
        
        NSString *iFrameTwitterBlockQuote = [NSString stringWithFormat:@"<iframe src=\"<br><p>%@</p> <script async src='https://platform.twitter.com/widgets.js' charset='utf-8'></script>\"></iframe>", updatedTwitterBlockQuote];
        
        htmlToParse = [htmlToParse stringByReplacingOccurrencesOfString:twitterBlockQuote
                                                             withString:iFrameTwitterBlockQuote];
        
        rangeStart = [htmlToParse rangeOfString:linkPatternStart];
        rangeEndLocation = rangeStart.location + rangeStart.length;
        rangeEndLength = htmlToParse.length - rangeEndLocation;
        
        if (rangeStart.location != NSNotFound) {
            rangeEnd = [htmlToParse rangeOfString:linkPatternEnd options:NSCaseInsensitiveSearch range:NSMakeRange(rangeEndLocation, rangeEndLength)];
        }
    }
    return htmlToParse;
}

- (UIView *)attributedTextContentView:(DTAttributedTextContentView *)attributedTextContentView viewForLink:(NSURL *)url identifier:(NSString *)identifier frame:(CGRect)frame{
    DTLinkButton *btn = [[DTLinkButton alloc] initWithFrame:frame];
    btn.URL = url;
    [btn addTarget:self action:@selector(linkTapped:) forControlEvents:UIControlEventTouchUpInside];
    return btn;
}

- (void)linkTapped:(DTLinkButton*)linkButton{
    SFSafariViewController *vc = [[SFSafariViewController alloc] initWithURL:linkButton.URL];
    [self.navigationController presentViewController:vc animated:YES completion:nil];
    return;
    
    NSString *path = linkButton.URL.path;
    if ([path hasPrefix:@"/"]) {
        path = [path stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@""];
    }
    if ([path hasSuffix:@"/"]) {
        path = [path stringByReplacingCharactersInRange:NSMakeRange(path.length - 1, 1) withString:@""];
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [Article fetchForSlug:path completionHandler:^(id object, NSError * _Nullable error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (!error) {
            Article *anArticle = (Article*)object;
            ArticlesController *vc = [ArticlesController new];
            vc.articles = @[anArticle];
            vc.index = 0;
            vc.navigationBarHidden = NO;
            [self.navigationController pushViewController:vc
                                            animated:YES];
        }else{
            SFSafariViewController *vc = [[SFSafariViewController alloc] initWithURL:linkButton.URL];
            [self.navigationController presentViewController:vc animated:YES completion:nil];
        }
    }];
    
}

- (UIView *)attributedTextContentView:(DTAttributedTextContentView *)attributedTextContentView viewForAttachment:(DTTextAttachment *)attachment frame:(CGRect)frame{
    if ([attachment isKindOfClass:[DTImageTextAttachment class]]) {
        NSString *md5 = [attachment.contentURL.absoluteString md5Checksum];
        DTLazyImageView *imageView = self.imageAttachmentViews[md5];
        
        if (!imageView) {
            imageView = [[DTLazyImageView alloc] initWithFrame:frame];
            imageView.contentView = attributedTextContentView;
            imageView.delegate = self;
            
            imageView.url = attachment.contentURL;
            imageView.shouldShowProgressiveDownload = YES;
        }
        
        return imageView;
    }else if ([attachment isKindOfClass:[DTIframeTextAttachment class]]) {
        CGFloat width = _scrollView.frame.size.width - kTotalHorizontalPadding;
        if (frame.size.width == 0 || frame.size.height == 0) {
            frame.size.width = width;
            frame.size.height = width;
            
            attachment.originalSize = frame.size;
            attachment.displaySize = frame.size;
        }
        NSString *md5;
        NSString *html;
        if (attachment.contentURL) {
            md5 = [attachment.contentURL.absoluteString md5Checksum];
        }else{
            NSString *twitterHTML = attachment.attributes[@"src"];
            html = [self htmlContainerForString:twitterHTML];
            md5 = [html md5Checksum];
        }
        
        IFrameView *webView = self.iFrames[md5];
        if (!webView) {
            webView  = [[IFrameView alloc] initWithFrame:frame];
            webView.contentView = attributedTextContentView;
            webView.attachment = attachment;
            webView.navigationDelegate = self;
            webView.tag = 206;
            [webView.scrollView addObserver:self
                                 forKeyPath:@"contentSize"
                                    options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld | NSKeyValueObservingOptionPrior
                                    context:NULL];
            
            if (attachment.contentURL) {
                [webView loadRequest:[NSURLRequest requestWithURL:attachment.contentURL]];
            }else{
                [webView loadHTMLString:html baseURL:nil];
//                [webView loadHTMLString:html baseURL:[NSURL URLWithString:@"https://www.twitter.com"]];
            }
            self.iFrames[md5] = webView;
        }else{
            self.webview.frame = frame;
        }
        return webView;
    }else if ([attachment isKindOfClass:[BlockQuoteAttachment class]]) {
        BlockQuoteAttachment *bAttachment = (BlockQuoteAttachment*)attachment;
        CGFloat width = _scrollView.frame.size.width - kTotalHorizontalPadding;
        DTTextHTMLElement *textElement = bAttachment.element.childNodes[0];
        if ([textElement isKindOfClass:[DTTextHTMLElement class]]) {
            NSAttributedString *captionText = [[NSAttributedString alloc] initWithString:textElement.text attributes:[BlockQuoteView captionAttributes]];
            CGRect labelRect = [captionText boundingRectWithSize:CGSizeMake(width - 20, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
            
            if (frame.size.width == 0 || frame.size.height == 0) {
                frame.size.width = width;
                frame.size.height = labelRect.size.height + 20;
                frame.origin.y -= 40;
                attachment.originalSize = frame.size;
                attachment.displaySize = frame.size;
            }
            BlockQuoteView *blockQuote = [[BlockQuoteView alloc] initWithFrame:frame];
            blockQuote.attachment = bAttachment;
            return blockQuote;
        }
        
    }else if ([attachment isKindOfClass:[FigCaptionAttachment class]]) {
        FigCaptionAttachment *fAttachment = (FigCaptionAttachment*)attachment;
        CGFloat width = _scrollView.frame.size.width - kTotalHorizontalPadding;
        DTTextHTMLElement *textElement = fAttachment.element.childNodes[0];
        if ([textElement isKindOfClass:[DTTextHTMLElement class]]) {
            NSAttributedString *captionText = [[NSAttributedString alloc] initWithString:textElement.text attributes:[FigCaptionView captionAttributes]];
            CGRect labelRect = [captionText boundingRectWithSize:CGSizeMake(width - 20, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
            
            if (frame.size.width == 0 || frame.size.height == 0) {
                frame.size.width = width;
                frame.size.height = labelRect.size.height + 20;
                frame.origin.y -= 40;
                attachment.originalSize = frame.size;
                attachment.displaySize = frame.size;
            }
            FigCaptionView *figCaption = [[FigCaptionView alloc] initWithFrame:frame];
            figCaption.attachment = fAttachment;
            return figCaption;
        }
    }
    return nil;
}


- (void)lazyImageView:(DTLazyImageView *)lazyImageView didChangeImageSize:(CGSize)size{
    DTAttributedTextContentView *contentView = (DTAttributedTextContentView *)lazyImageView.contentView;
    NSArray *textAttachments = [contentView.layoutFrame textAttachments];
    CGFloat width = _scrollView.frame.size.width - kTotalHorizontalPadding;
    
    for (DTTextAttachment *anAttachment in textAttachments) {
        if ([anAttachment.contentURL isEqual:lazyImageView.url] && [anAttachment isKindOfClass:[DTImageTextAttachment class]]) {
            CGSize newSize = CGSizeMake(width, (size.height * width) / size.width);
            
            if (!CGSizeEqualToSize(anAttachment.originalSize, size)) {
                anAttachment.originalSize = size;
                anAttachment.displaySize = newSize;
                
                
                lazyImageView.contentView.layouter = nil;
                [lazyImageView.contentView relayoutText];
                
                [self scheduleResizeParagraphs];
            }
        }
    }
}

- (NSAttributedString*)attributedStringFromHTML:(NSString*)htmlString{
    return [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUTF8StringEncoding]
                                            options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                      NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                 documentAttributes:nil error:nil];
}

- (void)slideShowTapped:(UITapGestureRecognizer*)g{
    ImageSlideshow *slideshow = (ImageSlideshow*)g.view;
    [slideshow presentFullScreenControllerFrom:self completion:nil];
}

- (void)imageTapped:(UITapGestureRecognizer*)g{
    UIImageView *imageView = (UIImageView*)g.view;
    
    NSMutableArray *imageInputs = [NSMutableArray array];
    SDWebImageSource *aSource = [[SDWebImageSource alloc] initWithUrlString:self.article.featuredImageURL.absoluteString
                                                                placeholder:nil];
    [imageInputs addObject:aSource];
    
    ImageSlideshow *slideshow = [[ImageSlideshow alloc] initWithFrame:imageView.frame];
    [slideshow setImageInputs:imageInputs];
    [slideshow presentFullScreenControllerFrom:self completion:nil];
}

- (void)writerTapped{
    AuthorController *vc = [AuthorController controller];
    vc.author = self.article.author;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    [self setFeaturedImageFrame];
}

- (void)setFeaturedImageFrame{
    CGRect imageRect = self.featuredImageView.frame;
    CGRect cellFrameInSuperview = [self.scrollView convertRect:imageRect
                                                        toView:self.scrollView];
    
    CGFloat y = self.scrollView.contentOffset.y - cellFrameInSuperview.origin.y;
    
    UIView *coverView = [self.featuredImageView viewWithTag:1000];
    if (coverView == nil) {
        coverView = [[UIView alloc] initWithFrame:self.featuredImageView.bounds];
        coverView.tag = 1000;
        coverView.backgroundColor = UIColor.blackColor;
        coverView.alpha = 0;
        [self.featuredImageView addSubview:coverView];
    }
    
    if (y >= 0) {
        self.featuredImageView.clipsToBounds = YES;
        imageRect.origin.y = y / 2;
        CGFloat alpha = MIN(0.5, (y / self.featuredImageView.frame.size.height));
        coverView.alpha = alpha;
    }else{
        coverView.alpha = 0;
        self.featuredImageView.clipsToBounds = NO;
        imageRect.origin.y = 0;
    }
    self.featuredImageView.frame = imageRect;
}
     
@end

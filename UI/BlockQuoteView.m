//
//  BlockQuoteView.m
//  AlArab
//
//  Created by Shashank Patel on 18/07/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import "BlockQuoteView.h"

@interface BlockQuoteView ()

@property   (nonatomic, strong)     UILabel     *label;
@property   (nonatomic, strong)     UIImage     *quoteStart, *quoteEnd;

@property   (nonatomic, strong)     UIImageView *quoteStartView, *quoteEndView;

@end

@implementation BlockQuoteView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        _label = [UILabel new];
        _label.numberOfLines = 0;
        [self addSubview:_label];
        
        self.quoteStart = [[UIImage imageNamed:@"quote_start"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.quoteEnd = [[UIImage imageNamed:@"quote_end"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        
        self.quoteStartView = [[UIImageView alloc] initWithImage:self.quoteStart];
        self.quoteStartView.tintColor = [kSharedNewsConfig navigationBarColor];
        self.quoteStartView.tag = 401;
        self.quoteStartView.contentMode = UIViewContentModeScaleAspectFit;
        
        self.quoteEndView = [[UIImageView alloc] initWithImage:self.quoteEnd];
        self.quoteEndView.tintColor = [kSharedNewsConfig navigationBarColor];
        self.quoteEndView.tag = 402;
        self.quoteEndView.contentMode = UIViewContentModeScaleAspectFit;
        
//        _label.backgroundColor = [UIColor colorWithWhite:0.98 alpha:1];
        [self addSubview:self.quoteStartView];
        [self addSubview:self.quoteEndView];
        self.frame = frame;
    }
    return self;
}

- (void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    
    if (kSharedLanguageManager.isRTL) {
        self.quoteStartView.frame = CGRectMake(frame.size.width - 25, 0, 25, 25);
    }else{
        self.quoteStartView.frame = CGRectMake(0, 0, 25, 25);
        self.quoteStartView.transform = CGAffineTransformMakeRotation(M_PI);
    }
    
    if (kSharedLanguageManager.isRTL) {
        self.quoteEndView.frame = CGRectMake(0, frame.size.height - 25, 25, 25);
    }else{
        self.quoteEndView.frame = CGRectMake(frame.size.width - 25, frame.size.height - 25, 25, 25);
        self.quoteEndView.transform = CGAffineTransformMakeRotation(M_PI);
    }
    
    [self bringSubviewToFront:self.quoteStartView];
    [self bringSubviewToFront:self.quoteEndView];
}

- (void)setAttachment:(DTTextAttachment *)attachment{
    FigCaptionAttachment *fAttachment = (FigCaptionAttachment*)attachment;
    DTTextHTMLElement *htmlAttachment = (DTTextHTMLElement *)fAttachment.element.childNodes[0];
    _label.attributedText = [[NSAttributedString alloc] initWithString:htmlAttachment.text attributes:[BlockQuoteView captionAttributes]];
    self.label.frame = CGRectMake(30, 10, self.frame.size.width - 60, self.frame.size.height - 20);
    self.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1];
}

+ (NSDictionary*)captionAttributes{
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = kSharedLanguageManager.defaultTextAlignment;
    
    UIFont *font = [[kSharedNewsConfig articleTitleFont] fontWithSize:17];
    NSDictionary *attributes = @{NSFontAttributeName : font,
                                 NSForegroundColorAttributeName: UIColor.darkGrayColor,
                                 NSParagraphStyleAttributeName : paragraphStyle
                                 };
    return attributes;
}

@end

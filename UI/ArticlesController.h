//
//  ArticlesController.h
//  MEO
//
//  Created by Shashank Patel on 02/04/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import "ViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ArticlesController : ViewController

@property (nonatomic)           NSInteger           index;
@property (nonatomic, strong)   NSArray<Article*>   *articles;

@end

NS_ASSUME_NONNULL_END

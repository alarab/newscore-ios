//
//  IFramelyView.h
//  Ahval English
//
//  Created by Shashank Patel on 24/05/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface IFramelyView : UIView

@property (nonatomic, strong)   NSDictionary    *data;
@property (nonatomic, strong)   NSURL           *URL;

+ (IFramelyView*)iFramelyView;
- (void)calculateFrame;

@end

NS_ASSUME_NONNULL_END

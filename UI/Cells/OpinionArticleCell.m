//
//  OpinionArticleCell.m
//  MEO
//
//  Created by Shashank Patel on 01/04/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import "OpinionArticleCell.h"

@implementation OpinionArticleCell

- (void)awakeFromNib{
    [super awakeFromNib];
    self.titleContainer.backgroundColor = UIColor.clearColor;
}

- (void)setArticle:(Article *)article{
    [super setArticle:article];
    [self.authorImageView sd_setImageWithURL:article.author.imageURL
                            placeholderImage:[UIImage imageNamed:@"genericuser"]
                                     options:SDWebImageHighPriority | SDWebImageProgressiveLoad];
    
}

- (UIColor*)authorNameColor{
    return [kSharedNewsConfig.dataSource titleColor];
}

@end

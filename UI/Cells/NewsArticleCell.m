//
//  NewsArticleCell.m
//  MEO
//
//  Created by Shashank Patel on 01/04/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import "NewsArticleCell.h"

@implementation NewsArticleCell

- (void)awakeFromNib{
    [super awakeFromNib];
    self.articleTitleLabel.numberOfLines = 2;
//    self.articleTitleLabel.minimumFontSize = 8;
    self.articleTitleLabel.adjustsFontSizeToFitWidth = YES;
    self.articleTitleLabel.minimumScaleFactor = 0.2;
    self.articleTitleLabel.lineBreakMode = NSLineBreakByWordWrapping;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    CGFloat tagWidth = [self.article widthOfAttributedTagWithHeight:kTagHeight];
    self.tagContainerWidth.constant = tagWidth + 10;
}

- (void)setTitleContainerHeight{
    self.titleContainer.backgroundColor = UIColor.clearColor;
    self.titleContainerHeight.constant = self.frame.size.height - 40;
}

- (CGFloat)titleFontSize{
    return 15;
}

- (CGFloat)tagFontSize{
    return 12;
}

- (CGFloat)authorNameFontSize{
    return 12;
}

- (UIColor*)authorNameColor{
    return [kSharedNewsConfig navigationBarColor];
}

- (NSDictionary*)titleAttributes{
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = kSharedLanguageManager.defaultTextAlignment;
    
    UIFont *font = [[kSharedNewsConfig articleTitleFont] fontWithSize:[self titleFontSize]];
    NSDictionary *attributes = @{NSFontAttributeName : font,
                                 NSForegroundColorAttributeName: [self titleColor],
                                 NSParagraphStyleAttributeName : paragraphStyle
                                 };
    return attributes;
}

@end

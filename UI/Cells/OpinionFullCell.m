//
//  OpinionFullCell.m
//  MEO
//
//  Created by Shashank Patel on 05/04/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import "OpinionFullCell.h"

@interface OpinionFullCell ()

@property (nonatomic, strong)   IBOutlet    UIView  *authorContainer;

@end

@implementation OpinionFullCell

- (void)layoutSubviews{
    [super layoutSubviews];
    CGFloat tagWidth = [self.article widthOfAttributedTagWithHeight:kTagHeight];
    self.tagContainerWidth.constant = tagWidth + 10;
//    self.authorImageView.layer.cornerRadius = self.authorImageView.frame.size.width / 2;
//    self.authorImageView.clipsToBounds = YES;
}

- (void)setArticle:(Article *)article{
    [super setArticle:article];
    
    NSDictionary *authorNameAttributes = self.authorNameAttributes;
    NSMutableParagraphStyle *paragraphStyle = authorNameAttributes[NSParagraphStyleAttributeName];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    self.article.titleAttributes = self.titleAttributes;
    self.article.tagAttributes = self.tagAttributes;
    self.article.author.nameAttributes = authorNameAttributes;
    self.articleTitleLabel.attributedText = self.article.attributedTitle;
    self.articleTitleLabel.adjustsFontSizeToFitWidth = true;
    self.articleTitleLabel.numberOfLines = 3;
    
    self.articleTagLabel.attributedText = self.article.attributedTag;
    
    self.articleAuthorLabel.attributedText = self.article.author.attributedName;
    
    self.authorImageView.layer.cornerRadius = self.authorImageView.frame.size.width / 2;
    
    [self.authorImageView sd_setImageWithURL:article.author.imageURL
                            placeholderImage:[UIImage imageNamed:@"genericuser"]
                                     options:SDWebImageHighPriority | SDWebImageProgressiveLoad
                                   completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                                       if (!image) {
                                           image = [UIImage imageNamed:@"genericuser"];
                                       }
                                       UIGraphicsBeginImageContextWithOptions(self.authorImageView.bounds.size, NO, 1.0);
                                       [[UIBezierPath bezierPathWithRoundedRect:self.authorImageView.bounds
                                                                   cornerRadius:self.authorImageView.bounds.size.width / 2 ] addClip];
                                       [image drawInRect:self.authorImageView.bounds];
                                       self.authorImageView.image = UIGraphicsGetImageFromCurrentImageContext();
                                       UIGraphicsEndImageContext();
                                   }];
    
}

- (NSDictionary *)titleAttributes{
    NSMutableDictionary *attributes = [[super titleAttributes] mutableCopy];
    
    UIFont *font = [[kSharedNewsConfig articleTitleFont] fontWithSize:[self titleFontSize] - 5];
    attributes[NSFontAttributeName] = font;
    
    return attributes;
}

- (void)setTitleContainerHeight{
    self.titleContainer.backgroundColor = UIColor.clearColor;
    self.titleContainerHeight.constant = self.frame.size.height - 40;
}

- (CGFloat)titleFontSize{
    return 25;
}

- (CGFloat)calculatedHeightForCellWidth:(CGFloat)cellWidth{
    return cellWidth;
}

- (UIColor*)authorNameColor{
    return [kSharedNewsConfig navigationBarColor];
}

@end

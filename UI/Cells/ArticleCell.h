//
//  ArticleCell.h
//  MEO
//
//  Created by Shashank Patel on 29/03/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import "CollectionViewCell.h"

#define kTagHeight              25
#define kTitlePadding           5
#define kTitleContainerPadding  10
#define kMinContainerHeight     50

NS_ASSUME_NONNULL_BEGIN

@interface ArticleCell : CollectionViewCell

@property (nonatomic, strong)   Article     *article;
@property (nonatomic)           CGFloat     titleFontSize;
@property (nonatomic)           BOOL        isFirst;

@property (nonatomic, strong)   IBOutlet    NSLayoutConstraint  *titleContainerHeight, *tagContainerWidth;
@property (nonatomic, strong)   IBOutlet    NSLayoutConstraint  *leftAlignConstraint, *rightAlignConstraint;

@property (nonatomic, strong)   IBOutlet    UIView          *titleContainer;
@property (nonatomic, strong)   IBOutlet    UIImageView     *articleImageView, *authorImageView;
@property (nonatomic, strong)   IBOutlet    UILabel         *articleTitleLabel, *articleSummaryLabel;
@property (nonatomic, strong)   IBOutlet    UILabel         *articleAuthorLabel, *articleTagLabel;
@property (nonatomic, strong)   IBOutlet    UILabel         *articleDateLabel;
@property (nonatomic, strong)   IBOutlet    UIButton        *shareButton;
@property (nonatomic, weak)     IBOutlet    UIView          *containerView;

+ (BOOL)imagesBeingLoaded;

- (NSDictionary*)titleAttributes;
- (NSDictionary*)tagAttributes;
- (NSDictionary*)authorNameAttributes;

- (CGFloat)calculatedHeightForCellWidth:(CGFloat)cellWidth;
- (CGFloat)widthForContainerWidth:(CGFloat)containerWidth;
- (void)cellDidScrollForCollectionView:(UICollectionView*)collectionView onView:(UIView*)view;
- (void)shareTapped;
- (UIColor*)titleColor;
- (void)layoutWithoutExtra;
- (void)loadUI;

@end

NS_ASSUME_NONNULL_END

//
//  FeaturedArticleCell.h
//  MEO
//
//  Created by Shashank Patel on 29/03/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import "ArticleCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface FeaturedArticleCell : ArticleCell

@property (nonatomic)   CGFloat     fixedHeight;

@end

NS_ASSUME_NONNULL_END

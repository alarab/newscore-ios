//
//  SearchCell.h
//  MEO
//
//  Created by Shashank Patel on 18/04/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import "ArticleCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface SearchCell : ArticleCell

@end

NS_ASSUME_NONNULL_END

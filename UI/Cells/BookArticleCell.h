//
//  BookArticleCell.h
//  MEO
//
//  Created by Shashank Patel on 22/04/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import "ArticleCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface BookArticleCell : ArticleCell

@end

NS_ASSUME_NONNULL_END

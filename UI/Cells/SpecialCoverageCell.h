//
//  SpecialCoverageCell.h
//  MEO
//
//  Created by Shashank Patel on 02/05/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import "ArticleCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface SpecialCoverageCell : ArticleCell

@end

NS_ASSUME_NONNULL_END

//
//  FeaturedArticleCell.m
//  MEO
//
//  Created by Shashank Patel on 29/03/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import "FeaturedArticleCell.h"

@interface FeaturedArticleCell ()

@property (nonatomic, strong)   IBOutlet    NSLayoutConstraint  *topContainerPadding;

@end

@implementation FeaturedArticleCell

- (void)awakeFromNib{
    [super awakeFromNib];
    self.fixedHeight = -1;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    CGFloat tagWidth = [self.article widthOfAttributedTagWithHeight:kTagHeight];
    self.tagContainerWidth.constant = tagWidth + 10;
    
    CGFloat cellWidth = self.frame.size.width;
    CGFloat imageHeight = cellWidth * 10 / 19;
    CGFloat titleWidthMax = cellWidth - (kTitleContainerPadding * 2) - (kTitlePadding * 2);
    CGFloat titleHeight = [self.article heightOfAttributedTitleWithWidth:titleWidthMax];
    CGFloat containerHeight = imageHeight + titleHeight + (kTitlePadding * 2) - [self extraYPadding];
    
    if (self.isFirst) {
        self.topContainerPadding.constant = [self extraYPadding];
    }
    
    if (self.fixedHeight != -1) {
        NSLog(@"titleHeight: %f", titleHeight);
    }
    
    self.titleContainerHeight.constant = containerHeight;
}

- (CGFloat)extraYPadding{
    CGFloat extraYPadding = 0;
    
    if (self.isFirst) {
        extraYPadding = 35;
    }
    return extraYPadding;
}

- (void)setTitleContainerHeight{
    self.titleContainer.backgroundColor = UIColor.clearColor;
    self.titleContainerHeight.constant = self.frame.size.height - 40;
}

- (CGFloat)titleFontSize{
    return 25;
}

- (CGFloat)calculatedHeightForCellWidth:(CGFloat)cellWidth{
    CGFloat imageHeight = cellWidth * 10 / 19;
    CGFloat titleContainerY = imageHeight;
    CGFloat titleWidthMax = cellWidth - (kTitleContainerPadding * 2) - (kTitlePadding * 2);
    CGFloat containerHeight = [self.article heightOfAttributedTitleWithWidth:titleWidthMax] + (kTitlePadding * 2);
    
    return titleContainerY + containerHeight + (kTitleContainerPadding * 2) + [self extraYPadding];
}


- (UIColor*)authorNameColor{
    return [kSharedNewsConfig navigationBarColor];
}

@end

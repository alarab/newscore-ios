//
//  ArticleDetailsCell.h
//  MEO
//
//  Created by Shashank Patel on 17/04/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ArticleDetailsCell : UICollectionViewCell

@property (nonatomic, strong)   ArticleDetailsController     *articleController;

@end

NS_ASSUME_NONNULL_END

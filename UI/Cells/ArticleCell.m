//
//  ArticleCell.m
//  MEO
//
//  Created by Shashank Patel on 29/03/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import "ArticleCell.h"
#import "OpinionArticleCell.h"

@interface ArticleCell ()

@end

@implementation ArticleCell

- (void)awakeFromNib{
    [super awakeFromNib];
    
    self.articleImageView.image = nil;
    [self.titleContainer addBorder:UIColor.whiteColor width:2];
    [self.titleContainer applyShadow];
    self.articleTagLabel.backgroundColor = [kSharedNewsConfig.dataSource tagBackgroundColor];
//    [Label appearance].semanticContentAttribute = UISemanticContentAttributeForceRightToLeft;
    [self configureShareButton];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    [self fixLayout];
    [self setTitleContainerHeight];
    CGFloat tagWidth = [self.article widthOfAttributedTagWithHeight:kTagHeight];
    self.articleTagLabel.alpha = tagWidth > 0 ? 1 : 0;
    self.tagContainerWidth.constant = tagWidth + 10;
}

- (void)layoutWithoutExtra{
    [super layoutSubviews];
}

- (void)setTitleContainerHeight{
//    CGFloat titleWidthMax = self.titleContainer.frame.size.width - (kTitlePadding * 2);
//    CGFloat containerHeight = [self.article heightOfAttributedTitleWithWidth:titleWidthMax] + (kTitlePadding * 2);
//    self.titleContainerHeight.constant = containerHeight > kMinContainerHeight ? containerHeight : containerHeight;
}

static NSInteger imageLoadCount = 0;

+ (BOOL)imagesBeingLoaded{
    return imageLoadCount == 0;
}

- (void)setArticle:(Article *)article{
    _article = article;
    
    [self setArticleImage];
    
    self.article.titleAttributes = self.titleAttributes;
    self.article.tagAttributes = self.tagAttributes;
    self.article.author.nameAttributes = self.authorNameAttributes;
    self.articleTitleLabel.attributedText = self.article.attributedTitle;
    self.articleTagLabel.attributedText = self.article.attributedTag;
    self.articleDateLabel.attributedText = self.article.attributedDateText;
    self.articleAuthorLabel.attributedText = self.article.author.attributedName;
    self.articleSummaryLabel.attributedText = self.article.attributedSubTitle;
    
    if (self.article.relatedArticles.count){
        for (Article *anArticle in self.article.relatedArticles) {
            anArticle.titleAttributes = self.titleAttributes;
            anArticle.tagAttributes = self.tagAttributes;
            anArticle.author.nameAttributes = self.authorNameAttributes;
        }
    }
}

- (void)loadUI{
    
}


- (void)setArticleImage{
    imageLoadCount++;
    [self.articleImageView sd_setImageWithURL:self.article.featuredImageURL
                             placeholderImage:nil
                                      options:SDWebImageHighPriority | SDWebImageProgressiveLoad
                                    completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                                        imageLoadCount--;
                                    }];
}

- (CGFloat)titleFontSize{
    return 20;
}

- (CGFloat)tagFontSize{
    return 15;
}

- (CGFloat)authorNameFontSize{
    return 15;
}
    
- (UIColor*)titleColor{
    return [kSharedNewsConfig.dataSource titleColor];
}


- (UIColor*)tagTextColor{
    return UIColor.whiteColor;
}

- (UIColor*)authorNameColor{
    return UIColor.whiteColor;
}

- (NSDictionary*)titleAttributes{
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = kSharedLanguageManager.defaultTextAlignment;
    
    UIFont *font = [[kSharedNewsConfig articleTitleFont] fontWithSize:[self titleFontSize]];
    NSDictionary *attributes = @{NSFontAttributeName : font,
                                 NSForegroundColorAttributeName: [self titleColor],
                                 NSParagraphStyleAttributeName : paragraphStyle
                                 };
    return attributes;
}

- (NSDictionary*)tagAttributes{
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    UIFont *font = [[kSharedNewsConfig fontForLanguage:kSharedLanguageManager.language] fontWithSize:[self tagFontSize]];
    NSDictionary *attributes = @{NSFontAttributeName : font,
                                 NSForegroundColorAttributeName: [self tagTextColor],
                                 NSParagraphStyleAttributeName : paragraphStyle
                                 };
    return attributes;
}

- (NSDictionary*)authorNameAttributes{
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = kSharedLanguageManager.reversedTextAlignment;
    
    UIFont *font = [[kSharedNewsConfig fontForLanguage:kSharedLanguageManager.language] fontWithSize:[self authorNameFontSize]];
    NSDictionary *attributes = @{NSFontAttributeName : font,
                                 NSForegroundColorAttributeName: [self authorNameColor],
                                 NSParagraphStyleAttributeName : paragraphStyle
                                 };
    return attributes;
}

- (CGFloat)calculatedHeightForCellWidth:(CGFloat)cellWidth{
    return 0;
}

- (void)fixPosition{
    UIView *coverView = [self.contentView viewWithTag:1000];
    coverView.alpha = 0;
    CGRect imageRect = self.contentView.frame;
    imageRect.origin.y = 0;
    self.contentView.frame = imageRect;
}

- (void)cellDidScrollForCollectionView:(UICollectionView*)collectionView onView:(UIView*)view{
    CGRect imageRect = self.contentView.frame;
    NSIndexPath *indexPath = [collectionView indexPathForCell:self];
    if (!indexPath) {
        [self fixPosition];
        return;
    }
    UICollectionViewLayoutAttributes *theAttributes = [collectionView layoutAttributesForItemAtIndexPath:indexPath];
    
    CGRect cellFrameInSuperview = [collectionView convertRect:theAttributes.frame toView:collectionView];
    
    CGFloat y = collectionView.contentOffset.y - cellFrameInSuperview.origin.y;
    
    UIView *coverView = [self.contentView viewWithTag:1000];
    if (coverView == nil) {
        coverView = [[UIView alloc] initWithFrame:self.contentView.bounds];
        coverView.tag = 1000;
        coverView.backgroundColor = UIColor.blackColor;
        coverView.alpha = 0;
        [self.contentView addSubview:coverView];
    }
    
    if (y >= 0) {
        self.contentView.clipsToBounds = YES;
        imageRect.origin.y = y / 2;
        CGFloat alpha = MIN(0.5, (y / self.contentView.frame.size.height));
        coverView.alpha = alpha;
    }else{
        coverView.alpha = 0;
        self.contentView.clipsToBounds = NO;
        imageRect.origin.y = 0;
    }
    self.contentView.frame = imageRect;
}

- (CGFloat)widthForContainerWidth:(CGFloat)containerWidth{
    return containerWidth;
}

- (void)shareTapped{
    NSURL *shareURL = [self.article shareURL];
    NSArray *activities = @[shareURL];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activities applicationActivities:nil];
    AppDelegate *delegate = (AppDelegate*)kAppDelegate;
    [delegate.drawerController presentViewController:activityVC animated:YES completion:nil];
}

- (void)configureShareButton{
    [self.shareButton addTarget:self
                         action:@selector(shareTapped)
               forControlEvents:UIControlEventTouchUpInside];
    self.shareButton.layer.cornerRadius = 15;
    self.shareButton.clipsToBounds = YES;
    [self.shareButton addBorder:UIColor.whiteColor width:1];
    self.shareButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    UIImage *shareImage = [[UIImage imageNamed:@"share"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.shareButton setImage:shareImage
                      forState:UIControlStateNormal];
    self.shareButton.tintColor = [kSharedNewsConfig.dataSource navigationBarColor];
    self.shareButton.backgroundColor = UIColor.clearColor;
}

@end

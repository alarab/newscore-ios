//
//  ArticleDetailsCell.m
//  MEO
//
//  Created by Shashank Patel on 17/04/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import "ArticleDetailsCell.h"

@implementation ArticleDetailsCell

- (void)setArticleController:(ArticleDetailsController *)articleController{
    [_articleController removeFromParentViewController];
    [_articleController viewWillDisappear:YES];
    [_articleController.view removeFromSuperview];
    _articleController = articleController;
    [self.contentView addSubview:articleController.view];
    [articleController viewWillAppear:YES];
    
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.articleController.view.frame = CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.height);
    [self.articleController viewDidLayoutSubviews];
}

@end

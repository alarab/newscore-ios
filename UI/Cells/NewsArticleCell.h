//
//  NewsArticleCell.h
//  MEO
//
//  Created by Shashank Patel on 01/04/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import "ArticleCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface NewsArticleCell : ArticleCell

@end

NS_ASSUME_NONNULL_END

//
//  SearchCell.m
//  MEO
//
//  Created by Shashank Patel on 18/04/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import "SearchCell.h"

@interface SearchCell ()

@property (nonatomic, strong)   IBOutlet    UITextView  *articleTextView;
@property (nonatomic, strong)   IBOutlet    UIView      *container;

@end

@implementation SearchCell

- (void)awakeFromNib{
    [super awakeFromNib];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    [self.container addBorder:UIColor.lightGrayColor width:1];
    self.articleTextView.scrollEnabled = NO;
    self.articleTextView.textContainer.exclusionPaths = [self exclusionPathsForCellWidth:self.frame.size.width];
    self.articleTextView.editable = NO;
    self.articleTextView.userInteractionEnabled = NO;
}

- (void)setArticle:(Article *)article{
    [super setArticle:article];
    self.articleTextView.attributedText = article.attributedCompoundText;
}

- (NSArray<UIBezierPath*>*)exclusionPathsForCellWidth:(CGFloat)cellWidth{
    return @[[UIBezierPath bezierPathWithRect:[self ignoreRectForCellWidth:cellWidth]]];
}

- (CGRect)ignoreRectForCellWidth:(CGFloat)cellWidth{
    CGFloat paddingX = 10;
    CGFloat paddingY = 10;
    
    CGFloat width = cellWidth / 3;
    CGFloat height = width * 3 / 4;
//    CGFloat height = [self calculatedHeightForCellWidth:width];
    
    CGFloat x;
    if (kSharedLanguageManager.isRTL) {
        x = cellWidth - width - paddingX;
    }else{
        x = paddingX;
    }
    CGFloat y = paddingY;
    CGRect ignoreRect = CGRectMake(x, y, width, height);
    return ignoreRect;
}

- (CGFloat)calculatedHeightForCellWidth:(CGFloat)cellWidth{
    self.articleTextView.textContainer.exclusionPaths = [self exclusionPathsForCellWidth:cellWidth];
    self.articleTextView.attributedText = self.article.attributedCompoundText;
    
    CGFloat height = self.articleTextView.contentSize.height - 20;
    CGFloat imageWidth = cellWidth / 3;
    CGFloat imageHeight = imageWidth * 3 / 4;
    return height > imageHeight ? height : imageHeight + 20;
}

- (UIColor*)titleColor{
    return UIColor.darkGrayColor;
}

@end

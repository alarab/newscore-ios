//
//  SpecialCoverageCell.m
//  MEO
//
//  Created by Shashank Patel on 02/05/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import "SpecialCoverageCell.h"

@interface SpecialCoverageCell ()

@property (nonatomic, strong)   IBOutlet  ImageSlideshow        *slideShow;
@property (nonatomic, strong)   IBOutlet  UILabel               *topLabel;

@property (nonatomic, strong)   IBOutlet  NSMutableArray<UIButton*>    *relatedButtons;

@end

@implementation SpecialCoverageCell

- (void)awakeFromNib{
    [super awakeFromNib];
    self.slideShow.userInteractionEnabled = NO;
}

- (void)setArticle:(Article *)article{
    [super setArticle:article];

    __block NSArray<NSDictionary*> *imageSlides = article[@"slideshow"];
    
    NSMutableArray *imageInputs = [NSMutableArray array];
    for (NSDictionary *aSlide in imageSlides) {
        NSString *imageURLString = [aSlide isKindOfClass:[NSString class]] ? aSlide : aSlide[@"url"];
        NCImageSource *aSource = [[NCImageSource alloc] initWithURL:[NSURL URLWithString:imageURLString]];
        [imageInputs addObject:aSource];
    }
    
    self.slideShow.slideshowInterval = 5;
    
    self.slideShow.currentPageChanged = ^(NSInteger page) {
        
    };
    
    self.slideShow.circular = YES;
    self.slideShow.contentMode = UIViewContentModeScaleToFill;
    [self.slideShow applyDarkShadow];
    [self.slideShow setImageInputs:imageInputs];
    
    for (NSInteger index = 0; index < [self.relatedButtons count]; index++) {
        UIButton *btn = self.relatedButtons[index];
        [btn removeFromSuperview];
    }
    
    self.relatedButtons = [NSMutableArray array];
    
    for (NSInteger index = 0; index < 3; index++) {
        Article *anArticle = article.relatedArticles[index];
        anArticle.titleAttributes = self.titleAttributes;
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setAttributedTitle:anArticle.attributedTitle
                       forState:UIControlStateNormal];
        btn.frame = CGRectMake(0, 0, self.contentView.frame.size.width, 65);
        [btn addBorder:UIColor.whiteColor width:1];
        [btn applyDarkShadow];
        btn.backgroundColor = [UIColor colorWithWhite:0 alpha:0.25];
        if (kSharedLanguageManager.defaultTextAlignment == NSTextAlignmentLeft) {
            btn.contentHorizontalAlignment =  UIControlContentHorizontalAlignmentLeft;
        }else{
            btn.contentHorizontalAlignment =  UIControlContentHorizontalAlignmentRight;
        }
        btn.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 10);
        btn.titleLabel.numberOfLines = 2;
        btn.tag = index;
        [btn addTarget:self
                action:@selector(articleTapped:)
      forControlEvents:UIControlEventTouchUpInside];
        [self.relatedButtons addObject:btn];
        [self.contentView addSubview:btn];
    }
    
    self.topLabel.attributedText = [self attributedTopLabelText];
    
    [self layoutSubviews];
}

- (NSAttributedString*)attributedTopLabelText{
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = kSharedLanguageManager.defaultTextAlignment;
    
    UIFont *font = [[kSharedNewsConfig articleTitleFont] fontWithSize:15];
    
    NSMutableAttributedString *str = [NSMutableAttributedString new];
    NSDictionary *titleAttributes = @{NSFontAttributeName : font,
                                      NSForegroundColorAttributeName: UIColor.whiteColor,
                                      NSParagraphStyleAttributeName : paragraphStyle,
                                      NSBackgroundColorAttributeName : [kSharedNewsConfig borderColor]
                                      };
    NSDictionary *typeTitleAttributes = @{NSFontAttributeName : font,
                                          NSForegroundColorAttributeName: UIColor.whiteColor,
                                          NSParagraphStyleAttributeName : paragraphStyle,
                                          NSBackgroundColorAttributeName : [kSharedNewsConfig navigationBarColor]
                                          };
    NSString *titleString = [NSString stringWithFormat:@" %@  ", self.article.title];
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:titleString
                                                                attributes:titleAttributes];
    
    NSString *typeString = @" Special Coverage ";
    if([kSharedLanguageManager language] == LanguageNameArabic){
        typeString = @" تغطية خاصة ";
    }
    NSAttributedString *typeTitle = [[NSAttributedString alloc] initWithString:typeString
                                                                   attributes:typeTitleAttributes];
    [str appendAttributedString:typeTitle];
    [str appendAttributedString:title];
    
    return str;
}

- (NSDictionary*)titleAttributes{
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = kSharedLanguageManager.defaultTextAlignment;
    
    UIFont *font = [[kSharedNewsConfig articleTitleFont] fontWithSize:15];
    NSDictionary *attributes = @{NSFontAttributeName : font,
                                 NSForegroundColorAttributeName: UIColor.whiteColor,
                                 NSParagraphStyleAttributeName : paragraphStyle
                                 };
    return attributes;
}

- (CGFloat)paddingX{
    return 10;
}

- (CGFloat)paddingY{
    return 10;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    CGFloat width = self.contentView.frame.size.width - (self.paddingX * 2);
    CGFloat height = 40;
    CGFloat containerHeight = self.contentView.frame.size.height;
    
    for (NSInteger index = 0; index < [self.relatedButtons count]; index++) {
        UIButton *btn = self.relatedButtons[index];
        NSInteger reverseIndex = [self.relatedButtons count] - index;
        CGFloat y = containerHeight - ((reverseIndex * height) + (self.paddingY * reverseIndex));
        
        btn.frame = CGRectMake(self.paddingX, y, width, height);
        [self.contentView addSubview:btn];
    }
}

- (CGFloat)calculatedHeightForCellWidth:(CGFloat)cellWidth{
    CGFloat imageHeight = cellWidth * 3 / 4;
    CGFloat titleHeight = 40;
    return imageHeight + (3 * titleHeight) + (4 * self.paddingX);
}

- (void)articleTapped:(UIButton*)sender{
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UINavigationController *navigationController = (UINavigationController *)delegate.drawerController.rootViewController;
    
    ArticlesController *vc = [ArticlesController new];
    vc.articles = self.article.relatedArticles;
    vc.index = sender.tag;
    vc.navigationBarHidden = NO;
    
    navigationController.transitioningStyle = SLNavigationTransitioningStyleDivide;
    [navigationController pushViewController:vc
                                    animated:YES];
}

- (void)setBackgroundColor:(UIColor *)backgroundColor{
    [super setBackgroundColor:[kSharedNewsConfig navigationBarColor]];
}


@end

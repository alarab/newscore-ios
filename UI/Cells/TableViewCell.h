//
//  TableViewCell.h
//  NewsCore
//
//  Created by Shashank Patel on 16/03/19.
//  Copyright © 2019 Shashank Patel. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TableViewCell : UITableViewCell

@end

NS_ASSUME_NONNULL_END

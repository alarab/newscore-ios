//
//  UIView+FixLayout.h
//  MEO
//
//  Created by Shashank Patel on 04/04/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (FixLayout)

- (void)fixLayout:(UIView*)view;
- (void)fixLayout;

- (void)fixLayoutReversed:(UIView*)view;
- (void)fixLayoutReversed;

@end

NS_ASSUME_NONNULL_END

//
//  BlockQuoteAttachment.m
//  Ahval English
//
//  Created by Shashank Patel on 22/05/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import "BlockQuoteAttachment.h"

@implementation BlockQuoteAttachment

- (id)initWithElement:(DTHTMLElement *)element options:(NSDictionary *)options{
    if (self = [super initWithElement:element options:options]) {
        self.element = element;
        self.element.padding = UIEdgeInsetsMake(0, 50, 0, 50);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            for (id obj in self.element.childNodes) {
                NSLog(@"obj:%@", [obj description]);
            }
        });
    }
    return self;
}

@end

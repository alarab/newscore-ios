//
//  Button.m
//  MEO
//
//  Created by Shashank Patel on 05/04/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import "Button.h"

@implementation Button

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initUIElements];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUIElements];
    }
    return self;
}

- (void)initUIElements{
    CGFloat fontSize = self.titleLabel.font.pointSize;
    self.titleLabel.font = [[kSharedNewsConfig.dataSource fontForLanguage:kSharedLanguageManager.language] fontWithSize:fontSize];
    NSLog(@"self.titleLabel.font: %@", self.titleLabel.font.description);
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
}

@end

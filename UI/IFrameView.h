//
//  IFrameView.h
//  Ahval English
//
//  Created by Shashank Patel on 23/05/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import <WebKit/WebKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface IFrameView : WKWebView

@property (nonatomic, DT_WEAK_PROPERTY) DTAttributedTextContentView *contentView;
@property (nonatomic, DT_WEAK_PROPERTY) DTTextAttachment            *attachment;

@end

NS_ASSUME_NONNULL_END

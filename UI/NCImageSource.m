//
//  NCImageSource.m
//  MEO
//
//  Created by Shashank Patel on 02/05/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import "NCImageSource.h"

@implementation NCImageSource

- (instancetype)initWithURL:(NSURL*)imageURL{
    if(self = [super init]){
        self.imageURL = imageURL;
        self.contentMode = UIViewContentModeScaleAspectFill;
    }
    return self;
}

- (void)loadTo:(UIImageView *)imageView with:(void (^)(UIImage * _Nullable))callback{
    imageView.contentMode = self.contentMode;
    [imageView sd_setImageWithURL:self.imageURL
                 placeholderImage:nil
                          options:SDWebImageHighPriority | SDWebImageProgressiveLoad
                        completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                            imageView.contentMode = self.contentMode;
                        }];
}

- (void)cancelLoadOn:(UIImageView *)imageView{
    [imageView sd_cancelCurrentImageLoad];
}

@end

//
//  CollectionView.h
//  MEO
//
//  Created by Shashank Patel on 29/03/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CollectionView : UICollectionView

@end

NS_ASSUME_NONNULL_END

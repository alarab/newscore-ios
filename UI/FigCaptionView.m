//
//  FigCaptionView.m
//  AlArab
//
//  Created by Shashank Patel on 18/07/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import "FigCaptionView.h"

@interface FigCaptionView ()

@property   (nonatomic, strong)     UILabel *label;

@end

@implementation FigCaptionView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        _label = [UILabel new];
        _label.numberOfLines = 0;
        [self addSubview:_label];
    }
    return self;
}

- (void)setAttachment:(DTTextAttachment *)attachment{
    FigCaptionAttachment *fAttachment = (FigCaptionAttachment*)attachment;
    DTTextHTMLElement *htmlAttachment = (DTTextHTMLElement *)fAttachment.element.childNodes[0];
    _label.attributedText = [[NSAttributedString alloc] initWithString:htmlAttachment.text attributes:[FigCaptionView captionAttributes]];
    self.label.frame = CGRectMake(10, 10, self.frame.size.width - 20, self.frame.size.height - 20);
    self.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1];
    
}

+ (NSDictionary*)captionAttributes{
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = kSharedLanguageManager.defaultTextAlignment;
    
    UIFont *font = [[kSharedNewsConfig articleTitleFont] fontWithSize:17];
    NSDictionary *attributes = @{NSFontAttributeName : font,
                                 NSForegroundColorAttributeName: UIColor.darkGrayColor,
                                 NSParagraphStyleAttributeName : paragraphStyle
                                 };
    return attributes;
}

@end

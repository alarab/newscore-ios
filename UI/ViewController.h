//
//  ViewController.h
//  NewsCore
//
//  Created by Shashank Patel on 24/08/18.
//  Copyright © 2018 Shashank Patel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic)         BOOL    navigationBarHidden;
@property (nonatomic)         BOOL    isChild;

+ (instancetype)controller;
+ (UINavigationController*)navigationController;
+ (NSString*)storyboardID;

- (void)configureNavigationBar;
- (IBAction)backTapped;
- (void)updateUI;

@end

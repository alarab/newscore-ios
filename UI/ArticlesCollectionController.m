//
//  ArticlesCollectionController.m
//  MEO
//
//  Created by Shashank Patel on 17/04/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import "ArticlesCollectionController.h"
#import "NewsCore.h"

@interface ArticlesCollectionController ()<UICollectionViewDataSource, UICollectionViewDelegate> {
    NSInteger   _index;
}

@property (nonatomic, strong)   IBOutlet    UICollectionView        *collectionView;

@property (nonatomic, strong)   UIView                  *topBar, *textResizeBar;
@property (nonatomic, strong)   UIButton                *bookmarkButton, *shareButton, *textResizeButton;
@property (nonatomic, strong)   UISlider                *textSizeSlider;


@end

@implementation ArticlesCollectionController

- (UIView*)topBar{
    if (!_topBar) {
        CGFloat width = 34;
        CGFloat height = 34;
        CGFloat paddingX = 20;
        CGFloat paddingY = 5;
        NSInteger count = 3;
        
        _topBar = [[UIView alloc] initWithFrame:CGRectMake(0, paddingY, (width * count) + (paddingX * (count + 1)), height)];
        
        NSInteger addedButtonsCount = 0;
        
        CGFloat x = (addedButtonsCount * width) + (paddingX * (addedButtonsCount + 1));
        self.bookmarkButton = [[UIButton alloc] initWithFrame:CGRectMake(x, 0, width, height)];
        [self.bookmarkButton setImage:[UIImage imageNamed:@"bookmark"] forState:UIControlStateNormal];
        [self.bookmarkButton setImage:[UIImage imageNamed:@"bookmarked"] forState:UIControlStateSelected];
        [self.bookmarkButton addTarget:self action:@selector(bookmarkTapped) forControlEvents:UIControlEventTouchUpInside];
        self.bookmarkButton.layer.cornerRadius = 17;
        self.bookmarkButton.clipsToBounds = YES;
        [self.bookmarkButton addBorder:UIColor.whiteColor width:1];
        self.bookmarkButton.selected = self.articles[self.index].isBookmarked;
        [_topBar addSubview:self.bookmarkButton];
        addedButtonsCount++;
        
        x = (addedButtonsCount * width) + (paddingX * (addedButtonsCount + 1));
        self.shareButton = [[UIButton alloc] initWithFrame:CGRectMake(x, 0, width, 34)];
        [self.shareButton setImage:[UIImage imageNamed:@"share"] forState:UIControlStateNormal];
        [self.shareButton addTarget:self action:@selector(shareTapped) forControlEvents:UIControlEventTouchUpInside];
        self.shareButton.layer.cornerRadius = 17;
        self.shareButton.clipsToBounds = YES;
        [self.shareButton addBorder:UIColor.whiteColor width:1];
        [_topBar addSubview:self.shareButton];
        addedButtonsCount++;
        
        x = (addedButtonsCount * width) + (paddingX * (addedButtonsCount + 1));
        self.textResizeButton = [[UIButton alloc] initWithFrame:CGRectMake(x, 0, width, height)];
        [self.textResizeButton setImage:[UIImage imageNamed:@"aaIcon"] forState:UIControlStateNormal];
        [self.textResizeButton addTarget:self action:@selector(textResizeTapped) forControlEvents:UIControlEventTouchUpInside];
        self.textResizeButton.layer.cornerRadius = 17;
        self.textResizeButton.clipsToBounds = YES;
        [self.textResizeButton addBorder:UIColor.whiteColor width:1];
        [_topBar addSubview:self.textResizeButton];
        
        _topBar.center = CGPointMake(UIScreen.mainScreen.bounds.size.width / 2, 22);
    }
    
    return _topBar;
}

- (UIView *)textResizeBar{
    if (!_textResizeBar) {
        CGFloat barWidth = VIEW_WIDTH * 0.75;
        CGFloat barX = (VIEW_WIDTH - barWidth) / 2;
        CGFloat width = 50;
        CGFloat height = 50;
        
        CGFloat paddingX = 10;
        CGFloat paddingY = 10;
        
        CGFloat buttonWidth = width - (paddingX * 2);
        CGFloat buttonheight = height - (paddingY * 2);
        
        
        
        CGFloat sliderX = paddingX + width + paddingX;
        CGFloat sliderWidth = barWidth - sliderX - (width + paddingX + paddingX);
        
        _textResizeBar = [[UIView alloc] initWithFrame:CGRectMake(barX, 40, barWidth, height)];
        UIButton *decreaseButton = [[UIButton alloc] initWithFrame:CGRectMake(paddingX, paddingY, buttonWidth, buttonheight)];
        UIImage *minusImage = [[UIImage imageNamed:@"minus"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [decreaseButton setImage:minusImage forState:UIControlStateNormal];
        decreaseButton.imageView.tintColor = [kSharedNewsConfig.dataSource navigationBarColor];
        [decreaseButton addTarget:self
                           action:@selector(decreaseTextTapped)
                 forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *increaseButton = [[UIButton alloc] initWithFrame:CGRectMake(barWidth - buttonWidth - paddingX, paddingY, buttonWidth, buttonheight)];
        UIImage *plusImage = [[UIImage imageNamed:@"plus"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [increaseButton setImage:plusImage forState:UIControlStateNormal];
        increaseButton.imageView.tintColor = [kSharedNewsConfig.dataSource navigationBarColor];
        [increaseButton addTarget:self
                           action:@selector(increaseTextTapped)
                 forControlEvents:UIControlEventTouchUpInside];
        
        self.textSizeSlider = [UISlider new];
        self.textSizeSlider.minimumValue = 12;
        self.textSizeSlider.maximumValue = 100;
        self.textSizeSlider.value = kSharedNewsConfig.articleBodyFontSize;
        self.textSizeSlider.frame = CGRectMake(sliderX, 0, sliderWidth, height);
        [self.textSizeSlider addTarget:self
                                action:@selector(sliderMoved:)
                      forControlEvents:UIControlEventValueChanged];
        [_textResizeBar addSubview:decreaseButton];
        [_textResizeBar addSubview:self.textSizeSlider];
        [_textResizeBar addSubview:increaseButton];
        [_textResizeBar addBorder:[kSharedNewsConfig.dataSource navigationBarColor]
                            width:1];
        [_textResizeBar applyDarkShadow];
        _textResizeBar.backgroundColor = UIColor.whiteColor;
    }
    return _textResizeBar;
}

- (void)increaseTextTapped{
    self.textSizeSlider.value += 5;
    [self sliderMoved:self.textSizeSlider];
}

- (void)decreaseTextTapped{
    self.textSizeSlider.value -= 5;
    [self sliderMoved:self.textSizeSlider];
}

- (void)sliderMoved:(UISlider*)slider{
    kSharedNewsConfig.articleBodyFontSize = slider.value;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.index inSection:0];
    ArticleDetailsCell *cell = (ArticleDetailsCell*)[self.collectionView cellForItemAtIndexPath:indexPath];
    [cell.articleController resizeParagraphs];
}

- (void)bookmarkTapped{
    [self.articles[self.index] bookmark];
    self.bookmarkButton.selected = self.articles[self.index].isBookmarked;
}

- (void)shareTapped{
    NSURL *shareURL = [self.articles[self.index] shareURL];
    
    NSArray *activities = @[shareURL];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activities applicationActivities:nil];
    [self.navigationController presentViewController:activityVC animated:YES completion:nil];
}

- (void)textResizeTapped{
    if (self.textResizeBar.superview) {
        [UIView animateWithDuration:0.25
                         animations:^{
                             self.textResizeBar.alpha = 0;
                         } completion:^(BOOL finished) {
                             [self.textResizeBar removeFromSuperview];
                         }];
    }else{
        [self.view addSubview:self.textResizeBar];
        [UIView animateWithDuration:0.25
                         animations:^{
                             self.textResizeBar.alpha = 1;
                         }];
    }
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar addSubview:self.topBar];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.topBar removeFromSuperview];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.collectionView.collectionViewLayout = [self collectionViewLayout];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColor.whiteColor;
    self.navigationController.navigationBar.translucent = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.collectionView.pagingEnabled = YES;
}

- (UICollectionViewFlowLayout*)collectionViewLayout{
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
//    AnimatedCollectionViewLayout *layout = [ArticlesAnimation layout];
    
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    layout.minimumLineSpacing = 0.0;
    layout.minimumInteritemSpacing = 0.0;
    
    return layout;
}

- (void)setArticles:(NSArray<Article *> *)articles{
    _articles = articles;
    self.index = 0;
    [self.collectionView reloadData];
}

- (NSInteger)index{
    NSInteger index = self.collectionView.contentOffset.x / self.collectionView.frame.size.width;
    return index;
}

- (void)setIndex:(NSInteger)index{
    if (index < 0 || index >= self.articles.count){
        return;
    }
    
    _index = index;
    
    dispatch_async(dispatch_get_main_queue(), ^{
//        [self.collectionView reloadData];
//        self.collectionView.alpha = 0;
        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:self->_index inSection:0]
                                    atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                            animated:NO];
        [UIView animateWithDuration:0.15 animations:^{
            self.collectionView.alpha = 1;
        }];
    });
}

- (ArticleDetailsController*)controllerAt:(NSInteger)index{
    if (index < 0 || index >= self.articles.count){
        return nil;
    }
    
    Article *anArticle = self.articles[index];
    ArticleDetailsController *vc = [ArticleDetailsController controller];
    vc.article = anArticle;
    vc.navigationBarHidden = NO;
    [self addChildViewController:vc];
    return vc;
}

#pragma mark UIPageViewControllerDataSource
- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController{
    ArticleDetailsController *vc = (ArticleDetailsController*)viewController;
    NSInteger vcIndex = [self.articles indexOfObject:vc.article];
    return [self controllerAt:(vcIndex - 1)];
}

- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController{
    ArticleDetailsController *vc = (ArticleDetailsController*)viewController;
    NSInteger vcIndex = [self.articles indexOfObject:vc.article];
    return [self controllerAt:(vcIndex + 1)];
}

#pragma -

#pragma mark UIPageViewControllerDelegate

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed{
    self.bookmarkButton.selected = self.articles[self.index].isBookmarked;
}

#pragma -

#pragma UICollectionViewDataSouce

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.articles.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ArticleDetailsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ArticleDetailsCell"
                                                                  forIndexPath:indexPath];
    cell.articleController = [self controllerAt:indexPath.row];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return collectionView.frame.size;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    self.bookmarkButton.selected = self.articles[self.index].isBookmarked;
}

#pragma -

@end

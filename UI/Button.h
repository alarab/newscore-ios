//
//  Button.h
//  MEO
//
//  Created by Shashank Patel on 05/04/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Button : UIButton

@end

NS_ASSUME_NONNULL_END

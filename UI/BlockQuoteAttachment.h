//
//  BlockQuoteAttachment.h
//  Ahval English
//
//  Created by Shashank Patel on 22/05/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import <DTCoreText/DTCoreText.h>

NS_ASSUME_NONNULL_BEGIN

@interface BlockQuoteAttachment : DTTextAttachment

@property (nonatomic, strong)   DTHTMLElement   *element;

@end

NS_ASSUME_NONNULL_END

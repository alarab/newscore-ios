//
//  UIView+FixLayout.m
//  MEO
//
//  Created by Shashank Patel on 04/04/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import "UIView+FixLayout.h"

#define kDoNotFixViewTag        1000

@implementation UIView (FixLayout)

- (void)fixLayout:(UIView*)view{
    if (view.tag == kDoNotFixViewTag) {
        return;
    }
    view.semanticContentAttribute = kSharedLanguageManager.semanticAttribute;
    for (UIView *subview in view.subviews) {
//        subview.semanticContentAttribute = kSharedLanguageManager.semanticAttribute;
        [self fixLayout:subview];
    }
}

- (void)fixLayout{
    [self fixLayout:self];
}

- (void)fixLayoutReversed:(UIView*)view{
    if (view.tag == kDoNotFixViewTag){
        return;
    }
    view.semanticContentAttribute = kSharedLanguageManager.reversedSemanticAttribute;
    for (UIView *subview in view.subviews) {
//        subview.semanticContentAttribute = kSharedLanguageManager.reversedSemanticAttribute;
        [self fixLayout:subview];
    }
}

- (void)fixLayoutReversed{
    [self fixLayoutReversed:self];
}

@end

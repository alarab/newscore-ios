//
//  NavigationTitleView.h
//  MEO
//
//  Created by Shashank Patel on 05/04/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

#define kNavigationTitleViewTag 300

@interface NavigationTitleView : UIView

+ (NavigationTitleView*)navigationTitleView;
- (void)shrink;
- (void)grow;
- (void)setLanguage:(LanguageName)language;
- (void)goToHome;

@end

NS_ASSUME_NONNULL_END

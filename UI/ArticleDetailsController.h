//
//  ArticleDetailsController.h
//  MEO
//
//  Created by Shashank Patel on 02/04/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import "ViewController.h"

@interface ArticleDetailsController : ViewController

@property (nonatomic, strong)   Article     *article;

- (void)resizeParagraphs;

@end

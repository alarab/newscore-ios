//
//  LoadingView.h
//  MEO
//
//  Created by Shashank Patel on 04/04/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingView : UIView

+ (LoadingView*)loadingView;

@end

//
//  IFramelyView.m
//  Ahval English
//
//  Created by Shashank Patel on 24/05/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import "IFramelyView.h"

@interface IFramelyView ()

@property (nonatomic, strong)   UIImageView                     *imageView;
@property (nonatomic, strong)   DTAttributedTextContentView     *titleSummaryLabel;

@property (nonatomic, strong)   IBOutlet    UIButton            *linkButton;

@end

@implementation IFramelyView

+ (IFramelyView*)iFramelyView{
    return [[UINib nibWithNibName:@"IFramelyView" bundle:nil] instantiateWithOwner:nil options:nil][0];
}


- (void)layoutSubviews{
    [super layoutSubviews];
    [self addBorder];
}

- (CGFloat)imageWidth{
//    if (_imageView.image) {
//        return _imageView.image.size.width;
//    }
    CGFloat imageWidth = [self.dataDict[@"image:width"] floatValue];
    if (imageWidth == 0) {
        imageWidth = self.frame.size.width;
    }
    return imageWidth;
}

- (CGFloat)imageHeight{
//    if (_imageView.image) {
//        return _imageView.image.size.height;
//    }
    if(!self.imageURL) return 0;
    
    CGFloat imageHeight = [self.dataDict[@"image:height"] floatValue];
    if (imageHeight == 0) {
        imageHeight = [self imageWidth] * 3 / 4;
    }
    return imageHeight;
}

- (NSDictionary*)dataDict{
    return self.data[@"data"];
}

- (NSURL*)imageURL{
    return [NSURL URLWithString:self.dataDict[@"image"]];
}

- (void)setData:(NSDictionary *)data{
    _data = data;
    self.URL = [NSURL URLWithString:data[@"url"]];
    
    CGFloat actualImageHeight = self.imageHeight * self.frame.size.width / self.imageWidth;
    
    if (!_imageView) {
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, actualImageHeight)];
        _imageView.contentMode = UIViewContentModeScaleAspectFill;
        _imageView.clipsToBounds = YES;
        [self addSubview:_imageView];
    }
    
    
    [self.imageView sd_setImageWithURL:self.imageURL
                      placeholderImage:nil
                               options:SDWebImageProgressiveLoad
                             completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                                 
                             }];
    
    if (!_titleSummaryLabel) {
        _titleSummaryLabel = [DTAttributedTextContentView new];
        _titleSummaryLabel.edgeInsets = UIEdgeInsetsMake(kHorizontalPadding, kHorizontalPadding, kHorizontalPadding, kHorizontalPadding);
        _titleSummaryLabel.shouldDrawLinks = YES;
        _titleSummaryLabel.shouldDrawImages = YES;
        _titleSummaryLabel.backgroundColor = UIColor.whiteColor;
        [self addSubview:_titleSummaryLabel];
    }
    _titleSummaryLabel.attributedString = self.attributedCompoundText;
    CGRect maxRect = CGRectMake(0, 0, self.frame.size.width, CGFLOAT_HEIGHT_UNKNOWN);
    NSRange entireString = NSMakeRange(0, [_titleSummaryLabel.attributedString length]);
    DTCoreTextLayoutFrame *layoutFrame = [_titleSummaryLabel.layouter layoutFrameWithRect:maxRect range:entireString];
    
    CGFloat textHeight = layoutFrame.frame.size.height;
    
    CGFloat totalHeight = actualImageHeight + textHeight;
    _titleSummaryLabel.frame = CGRectMake(0, actualImageHeight, self.imageWidth, textHeight);
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.imageWidth, totalHeight);
}

- (void)calculateFrame{
    [self bringSubviewToFront:self.linkButton];
    CGFloat actualImageHeight = self.imageHeight * self.frame.size.width / self.imageWidth;
    
    _imageView.frame = CGRectMake(0, 0, self.frame.size.width, actualImageHeight);
    
    CGRect maxRect = CGRectMake(0, 0, self.frame.size.width, CGFLOAT_HEIGHT_UNKNOWN);
    NSRange entireString = NSMakeRange(0, [_titleSummaryLabel.attributedString length]);
    DTCoreTextLayoutFrame *layoutFrame = [_titleSummaryLabel.layouter layoutFrameWithRect:maxRect range:entireString];
    
    CGFloat textHeight = layoutFrame.frame.size.height + kTotalHorizontalPadding;
    CGFloat totalHeight = actualImageHeight + textHeight;
    _titleSummaryLabel.frame = CGRectMake(0, actualImageHeight, self.imageWidth, textHeight);
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.imageWidth, totalHeight);
}

- (NSAttributedString *)attributedCompoundText{
    NSMutableAttributedString *text = [NSMutableAttributedString new];
    [text appendAttributedString:self.attributedTitle];
    [text appendString:@"\n"];
    [text appendAttributedString:self.attributedSummary];
    return text;
}

- (NSAttributedString*)attributedTitle{
    NSString *title = self.dataDict[@"title"]?:@"";
    return [[NSAttributedString alloc] initWithString:title
                                           attributes:self.titleAttributes];
}

- (NSDictionary*)titleAttributes{
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = kSharedLanguageManager.defaultTextAlignment;
    
    UIFont *font = [[kSharedNewsConfig articleTitleFont] fontWithSize:15];
    NSDictionary *attributes = @{NSFontAttributeName : font,
                                 NSForegroundColorAttributeName: [kSharedNewsConfig.dataSource articleTitleTextColor],
                                 NSParagraphStyleAttributeName : paragraphStyle
                                 };
    return attributes;
}

- (NSAttributedString*)attributedSummary{
    NSString *summary = self.dataDict[@"description"]?:@"";
    return [[NSAttributedString alloc] initWithString:summary
                                           attributes:self.summaryAttributes];
}

- (NSDictionary*)summaryAttributes{
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = kSharedLanguageManager.defaultTextAlignment;
    
    UIFont *font = [[kSharedNewsConfig articleSubTitleFont] fontWithSize:12];
    NSDictionary *attributes = @{NSFontAttributeName : font,
                                 NSForegroundColorAttributeName: [kSharedNewsConfig.dataSource articleBodyTextColor],
                                 NSParagraphStyleAttributeName : paragraphStyle
                                 };
    return attributes;
}

- (IBAction)tapped{
    SFSafariViewController *vc = [[SFSafariViewController alloc] initWithURL:self.URL];
    [[HomeViewController controller].navigationController presentViewController:vc animated:YES completion:nil];
}


@end

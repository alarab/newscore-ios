//
//  Label.m
//  NewsCore
//
//  Created by Shashank Patel on 16/03/19.
//  Copyright © 2019 Shashank Patel. All rights reserved.
//

#import "Label.h"

@implementation Label

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initUIElements];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUIElements];
    }
    return self;
}

- (void)initUIElements{
    CGFloat fontSize = self.font.pointSize;
    self.font = [[kSharedNewsConfig.dataSource fontForLanguage:kSharedLanguageManager.language] fontWithSize:fontSize];
    self.textColor = [kSharedNewsConfig.dataSource navigationBarColor];
    self.textAlignment = self.tag == 200 ? kSharedLanguageManager.reversedTextAlignment : kSharedLanguageManager.defaultTextAlignment;
}
    

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

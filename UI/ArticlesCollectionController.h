//
//  ArticlesCollectionController.h
//  MEO
//
//  Created by Shashank Patel on 17/04/19.
//  Copyright © 2019 Al Arab. All rights reserved.
//

#import "ViewController.h"

@interface ArticlesCollectionController : ViewController

@property (nonatomic)           NSInteger           index;
@property (nonatomic, strong)   NSArray<Article*>   *articles, *articlesSource;

@end
